$(document).ready(function() {
  $("#tablaPermisos").tablesorter({
      headers: {
        2: {sorter:false}
      }
  });
  $('#buscadorPermiso').val("");
});

$('#collapseFiltrosPermisos input').on("keypress" , function(e){
  if(e.which == 13) {
    e.preventDefault();
    $('#buscar-validado').click();
  }
})
//validación de filtro
$('#buscarPermiso').on('click', function(e){
  e.preventDefault();
  var permiso=$('#buscadorPermiso').val();

  if(permiso.length > 30){
    mostrarErrorValidacion($('#buscadorPermiso'), 'La cantidad máxima permitida de Carateres es de 30');
  }
  else{
    ocultarErrorValidacion($('#buscadorPermiso'));
    $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
       }
    })
    var formData={
      permiso: $('#buscadorPermiso').val(),
    }
     $.ajax({
         type: "POST",
         url: 'permisos/buscar',
         data: formData,
         dataType: 'json',
         success: function (data) {

          $('#cuerpoTablaPermisos tr').remove();
            var resultado='';

            if(data.permiso.length ==0){
              $('#tituloBusquedaPermisos').text('No se han encontrado Resultados');
              $('#cuerpoTablaPermisos').empty();
            }else{

              $('#tituloBusquedaPermisos').text('Se encontraron ' + data.permiso.length + ' Resultados');
              for (var i = 0; i < data.permiso.length; i++) {
                var fila=cargarLista(data.permiso[i]);
                $('#cuerpoTablaPermisos').append(fila);
              };
          }
        },
         error: function (data) {
             console.log('Error:', data);
         }
    });
  }
})


$(document).on('click','.detallePermiso',function(){
    var id_permiso = $(this).val();

    $('#modalDetallePermiso input').each(function(e){
      $(this).prop('disabled', true);

    })

    $.get('/permiso/' + id_permiso, function (data) {

      $('#nombrePermiso').text(data.permiso.name);
      $('#nombreSeccion').text( data.seccion.nombre_seccion);

      $('#id_permiso').val(data.permiso.id);


      $('#rolesP input:checkbox').prop('checked',false);
      $('#rolesP input:checkbox').prop('disabled' ,true);


      for (var i = 0; i < data.roles.length; i++) {
        $('#perm' + data.roles[i].id ).prop('checked' ,true);
      }

      $('#modalDetallePermiso').modal('show');
    })
})


// $(document).on('click','.modificarPermiso',function(){
//
//   var id_permiso = $(this).val();
//
//   $('#mensajeErrorPermisoModif').hide();
//   ocultarErrorValidacion($('#mod_permiso'));
//   $('#modif_secc_perm option').remove();
//
//   $.get('/permiso/' + id_permiso, function (data) {
//
//     $('#modalModificar input').each(function(e){
//       $(this).prop('checked', false);
//
//     })
//
//   //  $('#conteinerRolesModif input:checkbox').prop('checked',true);
//     $('#mod_permiso').val(data.permiso.name);
//     $('#btn-modificar-permiso').val(data.permiso.id);
//     var seccion=data.permiso.id_seccion
//
//     for (var i = 0; i < data.roles.length; i++) {
//        $('#pmod' + data.roles[i].id ).prop('checked' ,true);
//     }
//
//     $.get('permisos/getSecciones',function(data){
//       for (var i = 0; i < data.secciones.length; i++) {
//         if(data.secciones[i].id_seccion == seccion){
//           $('#modif_secc_perm').append($('<option>').val(data.secciones[i].id_seccion).attr('selected',true).text(data.secciones[i].nombre_seccion)).append($('</option>'));
//
//         }else{
//           $('#modif_secc_perm').append($('<option>').val(data.secciones[i].id_seccion).text(data.secciones[i].nombre_seccion)).append($('</option>'));
//         }
//       }
//     })
//       $('#modalModificar #btn-modificar-permiso').show();
//
//       $('#modalModificar').modal('show');
//
//   });
// });
//
// //guardar PERMISO nuevo o modificado
// $('#btn-modificar-permiso').click(function(e){
//   var roles=[];
//   $('#mensajeExito').hide();
//
//   $('#modalModificar input:checked').each(function(e){
//     roles.push($(this).val());
//   })
//
//   $.ajaxSetup({
//     headers: {
//       'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
//     }
//   })
//
//   var id= $(this).val();
//
//     var formData = {
//       id:id,
//       roles:  roles,
//       descripcion: $('#mod_permiso').val(),
//       id_seccion: $('#modif_secc_perm').val(),
//     } ;
//
//
//   $.ajax({
//     type: "POST",
//     url: 'permiso/modificar/' + id,
//     data: formData,
//     dataType: 'json',
//     success: function (data) {
//
//           $('#modalModificar').modal('hide');
//
//           $('#mensajeExito h3').text('EXITO');
//           $('#mensajeExito p').text('Se ha modificado correctamente el permiso ' + data.permiso.name +  '.');
//
//       $('#mensajeExito').show();
//       $('#buscarPermiso').trigger('click');
//     },
//     error: function (data) {
//
//       var response = JSON.parse(data.responseText).errors;
//
//       if(typeof response.existe !== 'undefined'){
//         $('#msjmodif').text('Este Permiso ya existe')
//       $('#mensajeErrorPermisoModif').show();
//       }
//
//       if(typeof response.descripcion !== 'undefined'){
//         mostrarErrorValidacion($('#mod_permiso'),response.descripcion[0],true);
//       }
//
//
//     }
//   });
// });

//NUEVO PERMISO
$('#btn-add-permiso').click(function(e){
  e.preventDefault();

  $('#mensajeExito').hide();
  $('#selectSecciones option').remove();
  $('#conteinerRoles').find('.rolesAsoc tbody > tr').remove();

  $('#frmPermisos').trigger('reset');
  // $('.modal-title').text('| NUEVO PERMISO');
  // $('#myModalPermisos .modal-header').attr('style','font-family: Roboto-Black; background-color: #6dc7be; color: #fff');
  $('#btn-save-permiso').show();


  $('#commentPermiso').val('');

    $('#commentPermiso').removeClass('alerta');
    $('#myModalPermisos #alertaDescripcion').hide();

  $('#myModalPermisos input').each(function(e){
    $(this).prop('checked', false);
    $(this).prop('disabled', false);
    $(this).prop('disabled', false);
  })
  $.get('permisos/getSecciones',function(data){
    for (var i = 0; i < data.secciones.length; i++) {
      $('#selectSecciones').append($('<option>').val(data.secciones[i].id_seccion).text(data.secciones[i].nombre_seccion)).append($('</select>'));
    }
  })
  $.get('rol/getAll',function(data){

      for (var i = 0; i < data.length; i++) {
        var fila = $(document.createElement('tr'));
        fila.append($('<td>').addClass('col-xs-3').append($('<input>').attr('id','permiso'+data[i].id).attr('type','checkbox').val(data[i].id)));
        fila.append($('<td>').addClass('col-xs-9').text(data[i].name).css('cssText','padding-left:-5px !important'));
        $('#conteinerRoles').find('.rolesAsoc').append(fila);
      }
  });


  $('#myModalPermisos').modal('show');


});

//guardar PERMISO nuevo o modificado
$('#btn-save-permiso').click(function(e){
  var roles=[];

  $('#mensajeExito').hide();

  $('#myModalPermisos input:checked').each(function(e){
    roles.push($(this).val());
  })

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
  })

  var formData={
    roles:  roles,
    descripcion: $('#commentPermiso').val(),
    id_seccion:$('#selectSecciones').val(),
  }

  $.ajax({
    type: "POST",
    url:'permiso/guardar',
    data: formData,
    dataType: 'json',
    success: function (data) {

          $('#frmResultado').trigger("reset");
          $('#myModalPermisos').modal('hide');
          $('#mensajeExito h3').text('Creación Exitosa');
          $('#mensajeExito p').text('Se ha creado correctamente el permiso ' + data.permiso.name +  '.');

          for (var i = 0; i < data.roles.length; i++) {
            var contenido =$('#cuerpoTablaRoles #' + data.roles[i].id_rol + ' a').data("content");
            roles= contenido + '· ' + data.permiso.name + '<br>';

            $('#cuerpoTablaRoles #' + data.roles[i].id + ' a').remove();
            $('#cuerpoTablaRoles #' + data.roles[i].id ).find('td:first')
                .append($('<a>')
                    .attr('id' , 'popoverData')
                    .addClass('pop').addClass('btn')
                    .attr('href' , "")
                    .attr("data-content", roles)
                    .attr("data-placement" , "bottom")
                    .attr("rel","popover")
                    .attr("data-original-title" , "Permisos asociados")
                    .attr("data-trigger" , "hover")
                    .append($('<span>')
                        .addClass("badge")
                        .append($('<i>')
                            .addClass("fa").addClass("fa-user"))))
          }

      // $("#tablaPermisos").trigger("update");
      // $("#tablaPermisos th").removeClass('headerSortDown').removeClass('headerSortUp').children('i').removeClass().addClass('fa').addClass('fa-sort');

      //Mostrar éxito
      $('#mensajeExito').show();
      $('#buscarPermiso').trigger('click');
    },
    error: function (data) {

      $('#myModalPermisos #alertaDescripcion').hide();
      var response = JSON.parse(data.responseText).errors;

      if(typeof response.existe !== 'undefined'){
      $('#mensajeErrorPermiso').show();
      }

      if(typeof response.descripcion !== 'undefined'){
        mostrarErrorValidacion($('#commentPermiso'),response.descripcion[0],true);
      }

    }
  });
});



$(document).on('click','.openEliminar',function(){

  $('#mensajeError').hide();
  $('#mensajeExito').hide();

    var id = $(this).val();

    $('#btn-eliminar-permiso').val(id);

    $.get('/permiso/' + id, function (data) {

        $('#lista2').empty();

          var renglon='';
          for (var i = 0; i < data.roles.length; i++) {

            renglon += '<li>'+data.roles[i].name+'</li>';
          }

          $('#lista2').append($('<h6>').append(renglon).css('cssText','font-size:16px !important'));
          $('#modalEliminar').modal('show');

    })

});

$('#btn-eliminar-permiso').click( function(e) {
  var id=$(this).val();

  $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
     }
 })
 $.ajax({
     type: "delete",
     url:  '/permiso/' + id,
     success: function (data) {

       $('#mensajeExito h3').text('EXITO');
       $('#mensajeExito p').text('Permiso eliminado');

       $('#modalEliminar').modal('hide');
       $('#mensajeExito').show();

     },
     error: function (data) {
       $('#mensajeError p').text('No es posible eliminar este Permiso');
       $('#modalEliminar').modal('hide');
       $('#mensajeError').show();     }
 });

});

function cargarLista(data){
  var fila=$('#moldePermisos').clone();
  fila.removeAttr('id');
  fila.attr('id',data.id);

  fila.find('.perm_name').text(data.name);
  fila.find('.perm_seccion').text(data.seccion);
  fila.find('.detallePermiso').val(data.id);
  fila.find('.openEliminar').val(data.id);

  fila.css('display','');
  $('#mostrarFPermiso').css('display','block');

  return fila;
}
