$(document).ready(function() {

  $('#barraUsuarios').attr('aria-expanded','true');
  $('#usuarios').removeClass();
  $('#usuarios').addClass('subMenu1 collapse in');

  $('.tituloSeccionPantalla').hide();
  $('#opcRolesPermisos').attr('style','border-left: 6px solid #185891; background-color: #131836;');
  $('#opcRolesPermisos').addClass('opcionesSeleccionado');

  $("#tablaRoles").tablesorter({
      headers: {
        1: {sorter:false}
      }
  });

  $('#buscadorRol').val("");

  //pestañas
    $(".tab_content").hide(); //Hide all content
    	$("ul.rolesPermisos li:first").addClass("active").show(); //Activate first tab
    	$(".tab_content:first").show(); //Show first tab content

      $('#rolesPermisos').show();
      $('#rolesPermisos').css('display','inline-block');

});

//PESTAÑAS
$("ul.rolesPermisos li").click(function() {

    $("ul.rolesPermisos li").removeClass("active"); //Remove any "active" class
    $(this).addClass("active"); //Add "active" class to selected tab
    $(".tab_content").hide(); //Hide all tab content

    var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to
                //identify the active tab + content
                console.log(activeTab);
    if(activeTab == '#pant_permisos'){
      $('#buscadorPermiso').val('');
      $('#buscadorPermiso').trigger('click');
    }
    if(activeTab == '#pant_rol'){
      $('#buscadorRol').val('');
      $('#buscadorRol').trigger('click');
    }
    $(activeTab).fadeIn(); //Fade in the active ID content
    return false;
});

$('#myModalRol input').on("keypress" , function(e){
  if(e.which == 13) {
    e.preventDefault();
    $('#btn-save-rol').click();
  }
})

$('#collapseFiltrosRoles input').on("keypress" , function(e){
  if(e.which == 13) {
    e.preventDefault();
    $('#buscarRol').click();
  }
})
//validación de filtro
$('#buscarRol').on('click', function(e){
  e.preventDefault();
  var rol=$('#buscadorRol').val();

  if(rol.length > 30){
    mostrarErrorValidacion($('#buscadorRol'), 'La cantidad máxima permitida de Carateres es de 30');
  }
  else{
    ocultarErrorValidacion($('#buscadorRol'));
    $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
       }
   })
    var formData={
      rol: $('#buscadorRol').val(),
    }
     $.ajax({
         type: "POST",
         url: 'roles/buscar',
         data: formData,
         dataType: 'json',
         success: function (data) {

            $('#cuerpoTablaRoles tr').remove();
              var resultado='';
              if(data.roles.length ==0){

                $('#tituloBusquedaRoles').text('No se encontraron roles');
                $('#cuerpoTablaRoles').empty();

              }
              else{

                $('#tituloBusquedaRoles').text('Se encontraron ' + data.roles.length + ' roles');
                for (var i = 0; i < data.roles.length; i++) {
                  var fila=cargarTabla(data.roles[i].rol);

                  $('#cuerpoTablaRoles').append(fila);
                }
            }
         },
         error: function (data) {
             console.log('Error:', data);
         }
    });
  }
})


//Opacidad del modal al minimizar
$('#btn-minimizar').click(function(){
    if($(this).data("minimizar")==true){
    $('.modal-backdrop').css('opacity','0.1');
      $(this).data("minimizar",false);
  }else{
    $('.modal-backdrop').css('opacity','0.5');
    $(this).data("minimizar",true);
  }
});

$(document).on('click','.openEliminarRol',function(){

    $('#mensajeError').hide();
    $('#mensajeExito').hide();

    var id = $(this).val();

    $('#btn-eliminar').val(id);

    $.get('/rol/' + id, function (data) {

        if(data.usuarios.length > 0){
          $('#modalEliminarRol #msjeliminarRol').text('Existen ' + data.usuarios.length + ' usuario/s asociados a ese Rol. Debe desasociarlos y luego podrá eliminar el Rol.').css('cssText','color:black !important;');}
        else{
            $('#modalEliminarRol #msjeliminarRol').text('No tiene usuarios asociados');
        }
        $('#modalEliminarRol').modal('show');

    })

});

$('#btn-eliminar').click( function(e) {
  var id=$(this).val();

  $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
     }
 })
 $.ajax({
     type: "delete",
     url:  '/rol/' + id,
     success: function (data) {
       $('#mensajeExito h3').text('EXITO');
       $('#mensajeExito p').text('Rol eliminado');

       $('#modalEliminarRol').modal('hide');
       $('#mensajeExito').show();
       $('#buscarRol').trigger('click');

     },
     error: function (data) {
        $('#mensajeError p').text('No es posible eliminar este Rol');
        $('#modalEliminarRol').modal('hide');
        $('#mensajeError').show();
     }
 });

});

$(document).on('click','.detalleRol',function(){

  $('#listaPermisosAsoc li').remove();
  $('#users li').remove();

  $('#tablaPermisosAsoc tbody tr').remove();
  $('#modalDetalleRol').modal('show');

  var id_rol = $(this).val();

  $.get('/rol/' + id_rol, function (data) {

    $('.nombreRol').text(data.rol.name);
    $('#id_rol').val(data.rol.id);

    for (var i = 0; i < data.usuarios.length; i++) {
      var fila = $('<li>');
      fila.text(data.usuarios[i].name).css('padding-left','10px').css('padding-bottom','5px').css( 'font-size', '16px');

      $('#users').append(fila);
    }

    for (var i = 0; i < data.permisos.length; i++) {
        var fila = $('<li>');

        fila.text(data.permisos[i].name).css('padding-left','10px').css('padding-bottom','5px').css( 'font-size', '16px');


        //Agregar fila a la tabla
        $('#listaPermisosAsoc').append(fila);
    }

  })
})

$(document).on('click','.modificarRol',function(){

  $('#ggg').remove();
  $('#contenedorSeccionesModif').hide();

  var id_rol = $(this).val();
  $('#id_rol_modif').val(id_rol);
  $('#mensajeErrorRol').hide();

  $('#modalModificarRol #btn-modificar-rol').show();
  $('#modalModificarRol #btn-modificar-rol').prop('disabled',true);
  $('#modalModificarRol #boton-cancelar-mod').show();
  $('#modalModificarRol').find('.loading').hide();
    $('#desplegableRolMod').append($('<div>').addClass('row').attr('id','ggg'));
   $('#ggg').append($('<div>').addClass('col-xs-12 mostrarSeccionModif').css('cssText','height:550px !important;overflow-y:auto !important'));

  $('#conteinerPermisos input:checkbox').prop('checked',false);
  $.get('/rol/' + id_rol, function (data2) {

    $('#nombreRolModif').val(data2.rol.name);

    var permisosAsociados=[];
    for (var i = 0; i < data2.permisos.length; i++) {
      permisosAsociados.push(data2.permisos[i].id);
    }
    console.log('perm',permisosAsociados);

      $.get('permiso/getAllSecciones', function(data){
        for (var i = 0; i < data.length; i++) {

          var f=$('#contenedorSeccionesModif').clone();
          f.find('#headingPMod').css('cssText','background-color:#ccc !important');
          f.removeAttr('id');
          f.find('#nombreSeccionmodif').text(data[i].seccion);

          for (var j = 0; j < data[i].permisos.length; j++) {

            var fila= $(document.createElement('tr'));

            if(permisosAsociados.includes(data[i].permisos[j].id)){
              fila.append($('<td>').addClass('col-xs-3').append($('<input>').attr('type','checkbox').val(data[i].permisos[j].id).prop('checked',true)))
                .append($('<td>').addClass('col-xs-9').text(data[i].permisos[j].name));
            }
            else{
              fila.append($('<td>').addClass('col-xs-3').append($('<input>').attr('type','checkbox').val(data[i].permisos[j].id).prop('checked',false)))
                .append($('<td>').addClass('col-xs-9').text(data[i].permisos[j].name));
            }

            f.find('#bodyModificar').append(fila);
          }
          f.css('display','block');
          $('.mostrarSeccionModif').append(f);
        }
      })

    $('#nombreRolModif').prop('readonly',false).removeClass('alerta');
    $('#modalModificarRol #alertaDescripcion').hide();

    //$('#rol1').prop('disabled',false);

    $('#modalModificarRol').modal('show');
      $('#modalModificarRol #desplegableRolMod').show();

  })
})

$(document).on('change','#modalModificarRol',function(){
  $('#modalModificarRol #btn-modificar-rol').prop('disabled',false);
})

$(document).on('change','#nombreRolModif',function(){
  $('#modalModificarRol #btn-modificar-rol').prop('disabled',false);
})

$('#btn-modificar-rol').on('click', function(e){
  e.preventDefault();

  var permisos=[];
  var my_url='rol/guardar';
  //var state=$('#btn-save-rol').val();

  $('#mensajeExito').hide();

    $('#modalModificarRol input:checked').each(function(e){
      permisos.push($(this).val());
    })
    var id=$('#id_rol_modif').val();

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
  })

  var formData = {
      id: id,
      permisos:  permisos,
      descripcion: $('#nombreRolModif').val(),
    };
    my_url='rol/modificar/' + id ;

  $.ajax({
    type: "POST",
    url: my_url,
    data: formData,
    dataType: 'json',
     beforeSend: function(data){
        $('#modalModificarRol #desplegableRolMod').hide();
        $('#modalModificarRol #btn-modificar-rol').hide();
        $('#modalModificarRol #boton-cancelar-mod').hide();


        $('#modalModificarRol').find('.iconoCargando').html('<div class="loading"><img src="/img/ajax-loader(1).gif" alt="loading" /><br>Un momento, por favor...</div>').css('text-align','center');

     },
    success: function (data) {

        $('#modalModificarRol').modal('hide');
        $('#frmRolModif').trigger("reset");

        $('#mensajeExito h3').text('EXITO');
        $('#mensajeExito p').text('Se ha modificado el rol ' + data.rol.name +  '.');

        $('#buscarRol').trigger('click');
      //Mostrar éxito
      $('#mensajeExito').show();

    },
    error: function (data) {

      var response = JSON.parse(data.responseText).errors;

      if(typeof response.permisos !== 'undefined'){
      $('#mensajeErrorRol').show();
      }

      if(typeof response.descripcion !== 'undefined'){
        mostrarErrorValidacion($('#commentModif'),data.descripcion,true);
      }
    }

  });
});

//NUEVO ROL
$('#btn-add-rol').click(function(e){
  e.preventDefault();

  $('#aeliminar').remove();
  $('#contenedorSeccionesModif').hide();
  $('#mensajeExito').hide();
  $('#btn-save-rol').show();

  $('#myModalRol input').each(function(e){
    $(this).prop('checked', false);
    $(this).prop('readonly', false);
    $(this).prop('disabled', false);
  })

  $('#desplegableCarga').append($('<div>').addClass('row').attr('id','aeliminar'));
 $('#aeliminar').append($('<div>').addClass('col-xs-12 mostrarSeccion').css('cssText','height:550px !important;overflow-y:auto !important'));


  $.get('permiso/getAllSecciones', function(data){
    for (var i = 0; i < data.length; i++) {

      var f=$('#contenedorSecciones').clone();
      f.find('#headingP').css('cssText','background-color:#ccc !important');
      f.removeAttr('id');
      f.find('#nombreSeccion2').text(data[i].seccion);

      for (var j = 0; j < data[i].permisos.length; j++) {

        var fila= $(document.createElement('tr'));
        fila.append($('<td>').addClass('col-xs-3').append($('<input>').attr('type','checkbox').val(data[i].permisos[j].id)))
          .append($('<td>').addClass('col-xs-9').text(data[i].permisos[j].name));

        f.find('#bodyNew').append(fila);
      }
      f.css('display','block');
      $('.mostrarSeccion').append(f);
    }

  })

  $('#comment').removeClass('alerta');

  $('#frmRol').trigger("reset");
  $('#myModalRol').modal('show');
});

//guardar ROL nuevo o modificado
$('#btn-save-rol').on('click', function(e){
  e.preventDefault();

  var permisos=[];
  var my_url='rol/guardar';

  $('#mensajeExito').hide();

    $('#myModalRol input:checked').each(function(e){
      permisos.push($(this).val());
    })

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
  })

  var formData={
      permisos:  permisos,
      descripcion: $('#comment').val(),
    }

  $.ajax({
    type: "POST",
    url: my_url,
    data: formData,
    dataType: 'json',
    success: function (data) {

        $('#myModalRol').modal('hide');
        $('#frmRol').trigger("reset");

        $('#mensajeExito h3').text('EXITO');
        $('#mensajeExito p').text('Se ha creado correctamente el rol ' + data.rol.name +  '.');
        $('#mensajeExito').show();

        $('#buscarRol').trigger('click');

    },
    error: function (data) {

      $('#myModalRol #alertaDescripcion').hide();
      var response = JSON.parse(data.responseText).errors;

      if(typeof response.permisos !== 'undefined'){
      $('#mensajeErrorRol').show();
      }

      if(typeof response.descripcion !== 'undefined'){
        mostrarErrorValidacion($('#comment'),response.descripcion[0],true);
      }
    }

  });
});




function crearSecciones(data){

  var array=[];
  for (var i = 0; i < data.length; i++) {
    if(!(array).includes(data[i].seccion)){
      array.push(data[i].seccion);
    }
  }
  return array;
}

function cargarTabla(data){
  var fila=$('#moldeRolFila').clone();
  fila.removeAttr('id');
  fila.attr('id',data.id);

  fila.find('.nombreRolTabla').text(data.name);
  fila.find('.detalleRol').val(data.id);
  fila.find('.modificarRol').val(data.id);
  fila.find('.openEliminarRol').val(data.id);

  fila.css('display','');
  $('#mostrarFRol').css('display','block');

  return fila;
}
