$(document).ready(function(e){

  $('.tituloSeccionPantalla').text('Configuración de Cuenta');

    var $imagenPerfil = $('#imagenPerfil').croppie({
      url: 'usuarios/imagen',
      enableExif: true,
      viewport: {
        width: 250,
        height:250,
        type: 'circle',
      },
      boundary: {
        width: 300,
        height: 300,
      },
    });

    $('#upload').on('change', function(e){
        e.preventDefault();

      	var reader = new FileReader();

        reader.onload = function (e) {
            $imagenPerfil.croppie('bind', {
                url: e.target.result
            }).then(function(){

          		console.log('jQuery bind complete');

          	});
        }

        reader.readAsDataURL(this.files[0]);
    });

    $('#password_actual').val("");

});


//Guardar solo la imagen de perfil
$('#btn-guardarImagen').click(function(e){
  $('#mensajeExito').hide();

  $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}});

  //Guardar imagen con la forma de recortar
  $('#imagenPerfil').croppie('result',{
    type: 'canvas',
    size: 'viewport',
    format: 'jpeg',
    quality: 0.5,
    circle: false,
    }).then(function(blob){
    console.log('blob',blob);

    var formData = new FormData();
    formData.append('id_usuario', $('#btn-guardarImagen').val());
    formData.append('imagen', blob);

    $.ajax({
        type: "POST",
        url: 'configCuenta/modificarImagen',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType:false,
        cache:false,

        success: function (data) {
            console.log(data);
            $('#img_perfilBarra').attr('src', 'data:image/jpeg;base64,' + data.imagen);

            $('#mensajeExito h3').text('EXITO');
            $('#mensajeExito p').text('SU IMAGEN DE PERFIL SE HA GUARDADO CORRECTAMENTE');
            $('#mensajeExito').show();
            //Mensaje de exito
        },
        error: function (data) {
            console.log(data);
        }
      });
  });

});

//Guardar solo los datos modificados de la cuenta (username, nombre completo y mail)
$('#btn-guardarDatos').click(function(e){
  $('#mensajeExito').hide();
  e.preventDefault();
  ocultarErrorValidacion($('#email'));
  ocultarErrorValidacion($('#user_name'));


  $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}});


  var formData = {
    id_usuario: $(this).val(),
    nombre:$('#nombre').val(),
    user_name: $('#user_name').val(),
    email: $('#email').val()
  }


  $.ajax({
      type: "POST",
      url: 'configCuenta/modificarDatos',
      data: formData,
      dataType: 'json',
      success: function (data) {
          console.log(data);
          //MOSTRAR MENSAJE ÉXTIO
          // $('#imgPerfil').attr('src', 'data:image/jpeg;base64,' + data.imagen);
          $('#mensajeExito h3').text('EXITO');
          $('#mensajeExito p').text('LOS CAMBIOS FUERON GUARDADOS CORRECTAMENTE');
          $('#mensajeExito').show();
      },
      error: function (data) {
          console.log(data);
          var response = data.responseJSON.errors;

          if(typeof response.user_name !== 'undefined'){
            mostrarErrorValidacion($('#user_name'),response.user_name[0],false);
          }
          if(typeof response.email !== 'undefined'){
            mostrarErrorValidacion($('#email'),response.email[0],false);

          }
      }
    });

});

//Cambiar la contraseña
$('#btn-cambiarPass').click(function(e){
  $('#mensajeExito').hide();
  e.preventDefault();

  $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}});

  var formData={
    id_usuario: $(this).val(),
    password_actual: $('#password_actual').val(),
    password_nuevo: $('#password_nuevo').val(),
    password_nuevo_confirmation:$('#password_nuevo_confirmation').val()
  }

  $.ajax({
      type: "POST",
      url: 'configCuenta/modificarPassword',
      data: formData,
      dataType: 'json',

      success: function (data) {
          console.log(data);
          $('#mensajeExito h3').text('EXITO');
          $('#mensajeExito p').text('SU CONTRASEÑA SE HA GUARDADO CORRECTAMENTE');
          $('#mensajeExito').show();

          ocultarErrorValidacion( $('#password_nuevo'));
          ocultarErrorValidacion( $('#password_actual'));
          ocultarErrorValidacion( $('#password_nuevo_confirmation'));

      },
      error: function (data) {
          console.log(data);
          var response = data.responseJSON.errors;

          if(typeof response.password_nuevo !== 'undefined'){
            mostrarErrorValidacion($('#password_nuevo'),response.password_nuevo[0],false);
          }
          if(typeof response.password_incorrecta !== 'undefined'){
            mostrarErrorValidacion($('#password_actual'),response.password_incorrecta[0],false);

          }
          if(typeof response.password_nuevo_confirmation !== 'undefined'){
            mostrarErrorValidacion($('#password_nuevo_confirmation'),response.password_nuevo_confirmation[0],false);

          }
      }
    });
});
