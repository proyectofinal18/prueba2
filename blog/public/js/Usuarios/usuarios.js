var id_usuario; //variable global, se usa para saber el usuario que esta siendo modificado

//Resaltar la sección en el menú del costado
$(document).ready(function(e) {

  $('#barraUsuarios').attr('aria-expanded','true');
  $('#usuarios').removeClass();
  $('#usuarios').addClass('subMenu1 collapse in');

  $('.tituloSeccionPantalla').text('Gestionar usuarios');
  $('#opcGestionarUsuarios').attr('style','border-left: 6px solid #185891; background-color: #131836;');
  $('#opcGestionarUsuarios').addClass('opcionesSeleccionado');
  $('[data-toggle="popover"]').popover();
  //Opacidad del modal al minimizar
  $('#btn-minimizar').click(function(){
      if($(this).data("minimizar")==true){
      $('.modal-backdrop').css('opacity','0.1');
        $(this).data("minimizar",false);
    }else{
      $('.modal-backdrop').css('opacity','0.5');
      $(this).data("minimizar",true);
    }
  });

  $('#contenedorFiltros #buscadorUsuario').val("");
  $('#contenedorFiltros #buscadorNombre').val("");
  $('#contenedorFiltros #buscadorEmail').val("");
  $('#contenedorFiltros #buscadorCasino').val('0');


  $('#buscar-usuarios').trigger('click',[1,10,'users.user_name','asc']);
});

$('#btn-ayuda').click(function(e){
  e.preventDefault();
	$('#modalAyuda').modal('show');
});

$('#btn-nuevo').click(function(e){
  e.preventDefault();

  $('#mensajeExito').hide();

  $('#modalCrear input:checked').prop('checked' ,false);
  $('#usuario').val('');
  $('#nombre').val('');
  $('#dni').val('');
  $('#email').val('');
  ocultarErrorValidacion($('#usuario'));
  ocultarErrorValidacion($('#nombre'));
  ocultarErrorValidacion($('#dni'));
  ocultarErrorValidacion($('#email'));

  $('#btn-guardar').val("nuevo");

  $('#modalCrear').modal('show');
  $('#mensajeErrorUser').hide();

})

//permite el enter cuando busca
$("#contenedorFiltros input").on('keypress',function(e){
    if(e.which == 13) {
      e.preventDefault();
      $('#buscar').click();
    }
});

$(document).on('change','#modalCrear #contenedorRoles input:checkbox', function(e){
  var roles = [];

  //Buscar todos los roles seleccionados
  $('#modalCrear #contenedorRoles input:checked').each(function(){
    roles.push($(this).val());
  })

  //Se muestran los permisos correspondientes a los roles seleccionados
  mostrarPermisos(roles, $('#modalCrear #contenedorPermisos'));
});

$(document).on('change','#modalModificar #contenedorRoles input:checkbox', function(e){
  var roles = [];

  //Buscar todos los roles seleccionados
  $('#modalModificar #contenedorRoles input:checked').each(function(){
    roles.push($(this).val());
  })

  //Se muestran los permisos correspondientes a los roles seleccionados
  mostrarPermisos(roles,$('#modalModificar #contenedorPermisos'));
})

$(document).on('click', '.resetPassword', function(e){

  e.preventDefault();

  var id=$(this).val();

  $('#modalAlerta #btn-reset').val(id);
  $('#modalAlerta').modal('show');

})

//resetear la contraseña: pasa a ser el dni
$('#btn-reset').click(function(e){

  $('#modalAlerta').modal('hide');

  $('#mensajeExito').hide();
  $('mensajeError').hide();

  var id_usuario=$(this).val();

    $.get('usuarios/resetear-ctr/' + id_usuario, function(data){

      if(data==1){
        $('#mensajeExito h3').text('Modificación Exitosa');
        $('#mensajeExito p').text('La contraseña de este usuario fue reemplazada por su DNI.');
        $('#mensajeExito .cabeceraMensaje').css('background-color','#FFB74D');
        $('#mensajeExito').show();
      }
      else{
        $('#mensajeError h3').text('ERROR');
        $('#mensajeError p').text('No fue posible restablecer la contraseña. ');
        $('#mensajeError').show();
        }

    })

});

$('#buscar-usuarios').click(function(e,pagina,page_size,columna,orden){

  e.preventDefault();
  var user=$('#buscadorUsuario').val();
  var email=$('#buscadorEmail').val();
  var nombre=$('#buscadorNombre').val();
  var casino=$('#buscadorCasino').val();

  var rta= validacionCantidad(user,email,nombre);
  if(rta == true){
    $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
       }
     });
     //Fix error cuando librería saca los selectores
     if(isNaN($('#herramientasPaginacion').getPageSize())){
       var size = 10; // por defecto
     }
     else {
       var size = $('#herramientasPaginacion').getPageSize();
     }

     var page_size = (page_size == null || isNaN(page_size)) ?size : page_size;
     // var page_size = (page_size != null) ? page_size : $('#herramientasPaginacion').getPageSize();
     var page_number = (pagina != null) ? pagina : $('#herramientasPaginacion').getCurrentPage();
     var sort_by = (columna != null) ? {columna,orden} : {columna: $('#tablaUsuarios .activa').attr('value'),orden: $('#tablaUsuarios .activa').attr('estado')} ;

     if(typeof sort_by['columna'] == 'undefined'){ // limpio las columnas
       var sort_by =  {columna: 'users.users_name',orden: 'asc'} ;

       //$('#tablaInicial th i').removeClass().addClass('fas fa-sort').parent().removeClass('activa').attr('estado','');
     }
     var formData={
          nombre: nombre,
          usuario: user,
          email: email,
          id_casino:casino,
          page: page_number,
          sort_by: sort_by,
          page_size: page_size,
    }

    $.ajax({
       type: "POST",
       url: 'usuarios/buscar',
       data: formData,
       dataType: 'json',
       success: function (data) {
          $('#herramientasPaginacion').generarTitulo(page_number,page_size,data.usuarios.total,clickIndice);
          $('#tablaUsuarios tbody tr').remove();

          for (var i = 0; i < data.usuarios.data.length; i++) {
            var fila=crearFila(data.usuarios.data[i]);
            $('#tablaUsuarios').append(fila);
          }

          $('#herramientasPaginacion').generarIndices(page_number,page_size,data.usuarios.total,clickIndice);

        },

       error: function (data) {
           console.log('Error:', data);
       }


     });
  }
})


// $('#buscar-validado')
//   e.preventDefault();
//
//
// });


$('#btn-guardar').on('click',function (e) {

  var roles=[];
  var casinos=[];
  $('#modalCrear #contenedorRoles input:checked').each(function(e){
    roles.push({'id_rol': $(this).val()});
  })
  $('#modalCrear #contenedorCasino input:checked').each(function(e){
    casinos.push({'id_casino': $(this).val()});
  })

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
  })

  e.preventDefault();

  my_url='usuarios/guardarUsuario';

  var formData = {
      nombre: $('#nombre').val(),
      usuario: $('#usuario').val(),
      dni: $('#dni').val(),
      email: $('#email').val(),
      roles: roles,
      casinos: casinos,

  }

  $.ajax({
      type: "POST",
      url: my_url,
      data: formData,
      dataType: 'json',
      success: function (data) {

          $('#mensajeExito p').text('El Usuario fue CREADO correctamente.');
          $('#mensajeExito h3').text('Creación Exitosa.');
          $('#mensajeExito .cabeceraMensaje').css('background-color','#4DB6AC');
          $('#modalCrear').modal('hide');
          $('#frmUsuario').trigger('reset');
          $('#mensajeExito').show();

          $('#buscar-usuarios').trigger('click',[1,10,'users.user_name','asc']);

        },

      error: function (data) {

        var response = data.responseJSON.errors;

        if(typeof response.usuario !== 'undefined'){
            mostrarErrorValidacion($('#usuario'),response.usuario[0],false);
        }
        if(typeof response.email !== 'undefined'){
          mostrarErrorValidacion($('#email'),response.email[0],false);
        }
        if(typeof response.nombre !== 'undefined'){
          mostrarErrorValidacion($('#nombre'),response.nombre[0],false);
        }
        if(typeof response.dni !== 'undefined'){
          mostrarErrorValidacion($('#dni'),response.dni[0],false);
        }
        if(typeof response.casinos !== 'undefined'){
          $('#mensajeErrorUser .msjTextCarga').text('Debe seleccionar al menos un casino para este usuario.');
          $('#mensajeErrorUser').show();
        }
        if(typeof response.roles !== 'undefined'){
          $('#mensajeErrorUser .msjTextCarga').text('Debe asignarle por lo menos un rol para este usuario.');
          $('#mensajeErrorUser').show();
        }
      }
  });
});

/* ver mas muestra modal cargado */

$(document).on('click','.info',function(){
  $('#contenedorPermisosVer').empty();
  $('#contenedorCasinoVer').empty();
  $('#modalVer #contenedorRoles').empty();
  var id_usuario = $(this).val();
    $('#modalVer input:checked').prop('checked' ,false);

  $.get('/usuarios/buscar/' + id_usuario, function (data) {

    $('#infoNombre').text(data.usuario.name);
    $('#infoUsuario').text(data.usuario.user_name);
    $('#infoEmail').text(data.usuario.email);
    $('#btn-info').val(data.usuario.id);
    // $('#modalVer #contenedorPermisos ul').empty();

    //Setear los roles del usuario
    for (var i = 0; i < data.roles.length; i++) {
      $('#modalVer #contenedorRoles').append($('<h6>').addClass('list-group-item')
                                                      .css('cssText','margin-top: 0px !important')
                                                      .css('font-size','14px')
                                                      .text(data.roles[i].name));
    }
    //mostrarPermisosVer(data.roles);

    //Setear los casinos del usuario
    for (var i = 0; i < data.casinos.length; i++) {
      $('#contenedorCasinoVer').append($('<h6>').addClass('list-group-item')
                                                .css('cssText','margin-top: 0px !important')
                                                .css('font-size','14px')
                                                .text(data.casinos[i].nombre));
    }

    for (var i = 0; i < data.permisos.length; i++) {
      $('#contenedorPermisosVer').append($('<h6>').addClass('list-group-item')
                                                .css('cssText','margin-top: 0px !important')
                                                .css('font-size','14px')
                                                .text(data.permisos[i].name));
    }

    $('#modalVer').modal('show');
  });
});


$(document).on('click','.modificar',function(){
  ocultarErrorValidacion($('#modNombre'));
  ocultarErrorValidacion($('#modUsuario'));
  ocultarErrorValidacion($('#modEmail'));
  ocultarErrorValidacion($('#modPassword'));

  $('#modNombre').prop('readonly',true);

  $('#mensajeExito').hide();
    var id_usuario = $(this).val();

    $('#modalModificar input:checked').prop('checked' ,false);

  $.get('usuarios/buscar/' + id_usuario, function (data) {

    $('#modNombre').val(data.usuario.name);
    $('#modUsuario').val(data.usuario.user_name);
    $('#modEmail').val(data.usuario.email);
    $('#btn-modificar').val(data.usuario.id);
    // $('#modalModificar #contenedorPermisos').empty();

    //Setear los roles del usuario
    for (var i = 0; i < data.roles.length; i++) {
      $('#modalModificar #rol' + data.roles[i].id ).prop('checked' ,true);
    }

    //Setear los permisos del usuario
    mostrarPermisos(data.roles,$('#modalModificar #contenedorPermisos'))  ;

    //Setear los casinos del usuario
    for (var i = 0; i < data.casinos.length; i++) {
      $('#modalModificar #casino' + data.casinos[i].id_casino ).prop('checked' ,true);
    }

    $('#modalModificar').modal('show');
    $('#mensajeErrorUserModif').hide();
  });
});

/*
modificar usuario
*/
$("#btn-modificar").click(function (e) {
  var roles=[];
  var casinos=[];
  $('#modalModificar #contenedorRoles input:checked').each(function(e){
    roles.push($(this).val());
  })
  $('#modalModificar #contenedorCasino input:checked').each(function(e){
    casinos.push($(this).val());
  })

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
  })

  my_url='usuarios/modificarUsuario';

  var formData = {
      id_usuario: $(this).val(),
      user_name: $('#modUsuario').val(),
      email: $('#modEmail').val(),
      roles: roles,
      casinos: casinos,
      origen: 1,
  }

  $.ajax({
      type: "POST",
      url: my_url,
      data: formData,
      dataType: 'json',
      success: function (data) {

          $('#modalModificar').modal('hide');
          $('#mensajeExito h3').text('EXITO');
          $('#mensajeExito p').text('El Usuario fue MODIFICADO correctamente.');
          $('#mensajeExito').show();

          $('#buscar-usuarios').trigger('click',[1,10,'users.user_name','asc']);

        },

      error: function (data) {
        var response = data.responseJSON.errors;


        if(typeof response.user_name !== 'undefined'){
          mostrarErrorValidacion($('#modUsuario'), response.user_name[0],false);
        }

        if(typeof response.email !== 'undefined'){
          mostrarErrorValidacion($('#modEmail'), response.email[0],false);
        }

        if(typeof response.nombre !== 'undefined'){
          mostrarErrorValidacion($('#modNombre'), response.email[0],false);
        }

        if (typeof response.casinos !== 'undefined'){
          $('#mensajeErrorUserModif .msjText').text('El usuario debe tener al menos un casino asignado.');
          $('#mensajeErrorUserModif').show();
        }
        if (typeof response.roles !== 'undefined'){
          $('#mensajeErrorUserModif .msjText').text('El usuario debe tener al menos un rol asignado.');
          $('#mensajeErrorUserModif').show();
        }
      }
  });
});

/* modal Ver Mas*/
/*
abre modal eliminar
*/
$(document).on('click','.openEliminar',function(){

  var id=$(this).val();
  $('#mensajeError').hide();
  $('#mensajeExito').hide();

  $('#modalEliminar').modal('show');
  $('#modalEliminar #btn-eliminar').val(id);
});


$('#btn-eliminar').click( function(e) {
  e.preventDefault();
  var id=$(this).val();

  formData={
    id: id,
  }

  $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
     }
 })
 $.ajax({
     type: "DELETE",
     url:  'usuarios/eliminarUsuario',
     data: formData,
    dataType: 'json',

     success: function (data) {

         $('#modalEliminar').modal('hide');
         $('#mensajeExito h3').text('EXITO');
         $('#mensajeExito p').text('El Usuario fue ELIMINADO.');
         $('#mensajeExito').show();

         $('#buscar-usuarios').trigger('click',[1,10,'users.user_name','asc']);
     },
     error: function (data) {
       $('#modalEliminar').modal('hide');
       $('#mensajeError h3').text('ERROR');
       $('#mensajeError p').text('No es posible eliminar este usuario.');
       $('#mensajeError').show();
    }
 });

});

//Función que obtiene los permisos de acuerdo a los roles seleccionados
function mostrarPermisos(roles, modal) {
  modal.empty();
  // $('#modalModificar #contenedorPermisos').empty();

  $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}});

  var formData = {
      roles: roles,
  }

  $.ajax({
      type: "POST",
      url: 'permiso/buscarPermisosPorRoles',
      data: formData,
      dataType: 'json',
      success: function (data) {
        console.log(data);

        modal.parent().find('h5').text("PERMISOS (" + data.permisos.length + ")");

        for (var i = 0; i < data.permisos.length; i++) {
          modal.append($('<div>')
                  .addClass('tagPermiso')
                  .attr('id', data.permisos[i].id)
                  .text(data.permisos[i].name))
        }
      },
      error: function (error) {
        console.log(error);
      },
  });
}

function crearFila(data){
  var nuevaFila=$('#moldeFila').clone();

  nuevaFila.removeAttr('id');
  nuevaFila.attr('id', 'usuario' + data.id);

  nuevaFila.find('.NUsuario').text(data.user_name);
  nuevaFila.find('.nombre').text(data.name);
  nuevaFila.find('.emailUs').text(data.email);
  nuevaFila.find('.info').val(data.id);
  nuevaFila.find('.modificar').val(data.id);
  nuevaFila.find('.openEliminar').val(data.id);
  nuevaFila.find('.resetPassword').val(data.id);

  nuevaFila.css('display','');
  $('#dd').css('display','block');
  return nuevaFila;
}


$(document).on('click','#tablaUsuarios thead tr th[value]',function(e){

  $('#tablaUsuarios th').removeClass('activa');

  if($(e.currentTarget).children('i').hasClass('fa-sort')){
    $(e.currentTarget).children('i').removeClass().addClass('fas fa-sort-down').parent().addClass('activa').attr('estado','desc');
  }
  else{

    if($(e.currentTarget).children('i').hasClass('fa-sort-down')){
      $(e.currentTarget).children('i').removeClass().addClass('fas fa-sort-up').parent().addClass('activa').attr('estado','asc');
    }
    else{
        $(e.currentTarget).children('i').removeClass().addClass('fas fa-sort').parent().attr('estado','');
    }
  }
  $('#tablaUsuarios th:not(.activa) i').removeClass().addClass('fas fa-sort').parent().attr('estado','');
  clickIndice(e,$('#herramientasPaginacion').getCurrentPage(),$('#herramientasPaginacion').getPageSize());
});


function clickIndice(e,pageNumber,tam){

  if(e != null){
    e.preventDefault();
  }

  var tam = (tam != null) ? tam : $('#herramientasPaginacion').getPageSize();
  var columna = $('#tablaUsuarios .activa').attr('value');
  var orden = $('#tablaUsuarios .activa').attr('estado');
  $('#buscar-usuarios').trigger('click',[pageNumber,tam,columna,orden]);
}

function validacionCantidad(user,email,nombre){
  var rta1,rta2,rta3=false;
  if(user.length > 45){
    mostrarErrorValidacion($('#buscadorUsuario'), 'La cantidad máxima permitida de caracteres es de 45',false);
    rta1= false;

  }else{
    ocultarErrorValidacion($('#buscadorUsuario'));
    rta1= true;
  }
  if(nombre.length > 60){
    mostrarErrorValidacion($('#buscadorNombre'), 'La cantidad máxima permitida de caracteres es de 60',false);
    rta2= false;
  }else{
    ocultarErrorValidacion($('#buscadorNombre'));
    rta2= true;
  }
  if(email.length > 60){
    mostrarErrorValidacion($('#buscadorEmail'), 'La cantidad máxima permitida de caracteres es 60',false);
    rta3= false;

  }else{
    ocultarErrorValidacion($('#buscadorEmail'));
    rta3 = true;
  }
  if(rta1==true && rta2==true && rta3==true){
    return true;
  }else{
    return false;
  }
}
