$(document).ready(function() {

  $('#barraUsuarios').attr('aria-expanded','true');
  $('#usuarios').removeClass();
  $('#usuarios').addClass('subMenu1 collapse in');

  $('.tituloSeccionPantalla').text('Log de Actividades');
  $('#opcLogActividades').attr('style','border-left: 6px solid #185891; background-color: #131836;');
  $('#opcLogActividades').addClass('opcionesSeleccionado');

  $('#usuarios').show();

  $('#btn-buscar').trigger('click');

});

$('#btn-ayuda').click(function(e){
  e.preventDefault();

  $('.modal-title').text('| LOG DE ACTIVIDADES');
  $('.modal-header').attr('style','font-family: Roboto-Black; background-color: #aaa; color: #fff');

	$('#modalAyuda').modal('show');

});

//Opacidad del modal al minimizar
$('#btn-minimizar').click(function(){
  if($(this).data("minimizar")==true){
    $('.modal-backdrop').css('opacity','0.1');
    $(this).data("minimizar",false);
  }else{
    $('.modal-backdrop').css('opacity','0.5');
    $(this).data("minimizar",true);
  }
});

//Quitar eventos de la tecla Enter
$('#collapseFiltros input').on('keypress',function(e){
    if(e.which == 13) {
      e.preventDefault();
      $('#btn-buscar').click();
    }
});

//DATETIMEPICKER de la fecha
$(function(){
    $('#dtpFecha').datetimepicker({
      language:  'es',
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      format: 'dd / mm / yyyy',
      pickerPosition: "bottom-left",
      startView: 4,
      minView: 2
    });
});
//Busqueda
$('#btn-buscar').click(function(e,pagina,page_size,columna,orden){
  e.preventDefault();
  var usuario=$('#B_usuario').val();
  var tabla=$('#B_tabla').val();
  var accion=$('#B_accion').val();
  var casino=$('#B_casino').val();

  var rta=validacionFiltros(usuario,tabla,accion);
  if(rta==true){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    var fecha;
    var fechaB;

    if($('#B_fecha').val() == ''){
      fecha = '';
    }else {
      fechaB = $('#B_fecha').val().split(" / ");
      fecha = fechaB[2]+"-"+fechaB[1]+"-"+fechaB[0];
    }
    if(isNaN($('#herramientasPaginacion').getPageSize())){
      var size = 10; // por defecto
    }else {
      var size = $('#herramientasPaginacion').getPageSize();
    }
    var page_size = (page_size == null || isNaN(page_size)) ? size : page_size;
    // var page_size = (page_size != null) ? page_size : $('#herramientasPaginacion').getPageSize();
    var page_number = (pagina != null) ? pagina : $('#herramientasPaginacion').getCurrentPage();
    var sort_by = (columna != null) ? {columna,orden} : {columna: $('#tablaResultados .activa').attr('value'),orden: $('#tablaResultados .activa').attr('estado')} ;
    if(typeof sort_by['columna'] == 'undefined'){ // limpio las columnas
      var sort_by =  {columna: 'fecha',orden: 'desc'} ;

      //$('#tablaInicial th i').removeClass().addClass('fas fa-sort').parent().removeClass('activa').attr('estado','');
    }

    var formData = {
      usuario: usuario,
      tabla: tabla,
      accion:accion,
      id_casino: casino,
      fecha: fecha,
      page: page_number,
      sort_by: sort_by,
      page_size: page_size,
    }

    $.ajax({
        type: 'POST',
        url: 'logActividades/buscarLogActividades',
        data: formData,
        dataType: 'json',
        success: function(resultados){
          $('#herramientasPaginacion').generarTitulo(page_number,page_size,resultados.total,clickIndice);
          $('#cuerpoTabla tr').remove();
          for (var i = 0; i < resultados.data.length; i++){
            $('#cuerpoTabla').append(generarFilaTablaResultados(resultados.data[i]));
          }
          $('#herramientasPaginacion').generarIndices(page_number,page_size,resultados.total,clickIndice);
        },
        error: function(data){
          console.log('Error:', data);
        }
      });
    }
});

$(document).on('click','#tablaResultados thead tr th[value]',function(e){
  $('#tablaResultados th').removeClass('activa');
  if($(e.currentTarget).children('i').hasClass('fa-sort')){
    $(e.currentTarget).children('i').removeClass().addClass('fas fa-sort-down').parent().addClass('activa').attr('estado','desc');
  }
  else{
    if($(e.currentTarget).children('i').hasClass('fa-sort-down')){
      $(e.currentTarget).children('i').removeClass().addClass('fas fa-sort-up').parent().addClass('activa').attr('estado','asc');
    }
    else{
      $(e.currentTarget).children('i').removeClass().addClass('fas fa-sort').parent().attr('estado','');
    }
  }
  $('#tablaResultados th:not(.activa) i').removeClass().addClass('fas fa-sort').parent().attr('estado','');
  console.log('tamaño_pagina evento' , $('#herramientasPaginacion').getPageSize());
  clickIndice(e,$('#herramientasPaginacion').getCurrentPage(),$('#herramientasPaginacion').getPageSize());
});

function clickIndice(e,pageNumber,tam){
  if(e != null){
    e.preventDefault();
  }
  console.log('tamaño pagina', tam);
  var tam = (tam != null) ? tam : $('#herramientasPaginacion').getPageSize();
  var columna = $('#tablaResultados .activa').attr('value');
  var orden = $('#tablaResultados .activa').attr('estado');
  $('#btn-buscar').trigger('click',[pageNumber,tam,columna,orden]);
}

function generarFilaTablaResultados(log){
      var fila = $(document.createElement('tr'));
      fila.attr('id','log' + log.id_log)
          .append($('<td>')
              .addClass('col-xs-2')
              .text(log.name).css('text-align','center')
          )
          .append($('<td>')
              .addClass('col-xs-2')
              .text(log.fecha).css('text-align','center')
          )
          .append($('<td>')
              .addClass('col-xs-2 columnaOculta')
              .text(log.accion).css('text-align','center')
          )
          .append($('<td>')
              .addClass('col-xs-2')
              .text(log.tabla).css('text-align','center')
          )
          .append($('<td>')
              .addClass('col-xs-2 columnaOculta')
              .text(log.nombre).css('text-align','center')
          )
          .append($('<td>')
              .addClass('col-xs-2')
              .append($('<button>')
                  .append($('<i>')
                      .addClass('fa')
                      .addClass('fa-search-plus')
                  )
                  .append($('<span>').text(' VER MÁS'))
                  .addClass('btn').addClass('btn-info').addClass('detalle').css('text-align','center')
                  .attr('value',log.id_log)
              )
          )
      return fila;
}

//Mostrar modal con los datos del Log
$(document).on('click','.detalle',function(){
    //Resetear formulario para llevar los datos
      $('#frmLog').trigger('reset');
      var id_log = $(this).val();

      $.get("logActividades/obtenerLogActividad/" + id_log, function(data){

          $('#fecha').text(data.log.fecha);
          $('#usuario').text(data.usuario);
          $('#accion').text(data.log.accion);
          $('#tabla').text(data.log.tabla);
          $('#id_entidad').text(data.casino.nombre);

          $('#tablaDetalleLog tbody > tr').remove();

          for (var i = 0; i < data.detalles.length; i++) {
            if (data.detalles_anterior != null) {
              if(data.detalles[i].valor != null){
                var valorNuevo=data.detalles[i].valor;
              }else{
                var valorNuevo='-';
              }
              if(data.detalles_anterior[i].valor != null){
                var valorAnterior=data.detalles_anterior[i].valor;
              }else{
                var valorAnterior='-';
              }

              $('#tablaDetalleLog tbody')
                  .append($('<tr>')
                      .append($('<td>')
                          .addClass('col-xs-4')
                          .text(data.detalles[i].campo).css('color','#757575')
                      )
                      .append($('<td>')
                          .addClass('col-xs-4')
                          .text(valorNuevo).css('color','#757575')
                      )
                      .append($('<td>')
                          .addClass('col-xs-4')
                          .text(valorAnterior).css('color','#757575')
                      )
                  )
            }else{
              if(data.detalles[i].valor != null){
                var valorN=data.detalles[i].valor;
              }else{
                var valorN='-';
              }
              $('#tablaDetalleLog tbody')
                  .append($('<tr>')
                      .append($('<td>')
                          .addClass('col-xs-6')
                          .text(data.detalles[i].campo).css('color','#757575')
                      )
                      .append($('<td>')
                          .addClass('col-xs-6')
                          .text(valorN).css('color','#757575')
                      )
                      .append($('<td>')
                          .addClass('col-xs-6')
                          .text('-').css('color','#757575')
                      )
                  )
            }

          }
          if(data.detalles.length == 0){
            $('#tablaDetalleLog').hide();
          }
          else {
            $('#tablaDetalleLog').show();
          }
          $('#modalLogActividad').modal('show');
      });
});

function validacionFiltros(usuario,tabla,accion){
  var rta1,rta2,rta3=false;
  if(usuario.length > 45){
    mostrarErrorValidacion($('#B_usuario'), 'La cantidad máxima permitida de caracteres es de 45',false);
    rta1= false;

  }else{
    ocultarErrorValidacion($('#B_usuario'));
    rta1= true;
  }
  if(tabla.length > 25){
    mostrarErrorValidacion($('#B_tabla'), 'La cantidad máxima permitida de caracteres es de 25',false);
    rta2= false;
  }else{
    ocultarErrorValidacion($('#B_tabla'));
    rta2= true;
  }
  if(accion.length > 15){
    mostrarErrorValidacion($('#B_accion'), 'La cantidad máxima permitida de caracteres es 15',false);
    rta3= false;

  }else{
    ocultarErrorValidacion($('#B_accion'));
    rta3 = true;
  }

  if(rta1==true && rta2==true && rta3==true){
    return true;
  }else{
    return false;
  }
}
