@extends('layouts.barraNavegacion')

@section('contenidoVista')


<header>
  <img class="iconoSeccion" src="/img/logos/usuarios_blue.png" alt="">
  <h2>USUARIOS</h2>
</header>

        <div class="row">
            <div class="col-md-4">
              <a href="usuarios" class="tarjetaMenu">
                  <div class="contenedorTarjeta" align="center">
                        <!-- <img class="imagenTarjeta" src="/img/tarjetas/img_1.jpg" alt=""> -->
                        <img class="imagenTarjeta" src="/img/tarjetas/img_GestionUsuarios.jpg" alt="">
                        <h1 class="tituloTarjeta">GESTIONAR USUARIOS</h1>
                        <p class="detalleTarjeta">Esta sección permite crear, modificar y eliminar usuarios, además de poder filtrar los usuarios de la lista.</p>
                  </div>
              </a>
            </div>
              <div class="col-md-4">
                <a href="roles" class="tarjetaMenu">
                    <div class="contenedorTarjeta" align="center">
                          <!-- <img class="imagenTarjeta" src="/img/tarjetas/img_1.jpg" alt=""> -->
                          <img class="imagenTarjeta" src="/img/tarjetas/img_RolesPermisos.jpg" alt="">
                          <h1 class="tituloTarjeta">ROLES Y PERMISOS</h1>
                          <p class="detalleTarjeta">Ingrese aquí para ver la sección de manejo de permisos/roles a usuarios.</p>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
              <a href="logActividades" class="tarjetaMenu">
                  <div class="contenedorTarjeta" align="center">
                        <!-- <img class="imagenTarjeta" src="/img/tarjetas/img_1.jpg" alt=""> -->
                        <img class="imagenTarjeta" src="/img/tarjetas/img_LogActividad.jpg" alt="">
                        <h1 class="tituloTarjeta">LOG DE ACTIVIDADES</h1>
                        <p class="detalleTarjeta">Muestra movimientos internos de acciones en el sistema.</p><br>
                  </div>
              </a>
            </div>

        </div>



@endsection
@section('scripts')
<!-- JavaScript personalizado -->
<script src="js/menu_usuarios.js" charset="utf-8"></script>

@endsection
