<?php

namespace App\Http\Controllers\Pruebas;

use Auth;
use Session;
use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Casino;
use App\Relevamiento;
use App\SecRecientes;
use App\Http\Controllers\RolesPermissions\RoleFinderController;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Mesas\Apertura;
use App\Mesas\ApuestaMinimaJuego;
use App\Mesas\Canon;
use App\Mesas\Cierre;
use App\Mesas\CierreApertura;
use App\Mesas\Mesa;
use App\MesCasino;
use App\Mesas\Ficha;
use App\Mesas\FichaTieneCasino;
use App\Mesas\JuegoMesa;
use App\Mesas\SectorMesas;
use App\Mesas\TipoMesa;
use App\Mesas\ImportacionDiariaMesas;
use App\Mesas\DetalleImportacionDiariaMesas;
use App\Mesas\ImportacionMensualMesas;
use App\Mesas\DetalleImportacionMensualMesas;
use App\Mesas\DetalleCierre;
use App\Mesas\DetalleApertura;
use App\Mesas\Moneda;

use App\Mesas\ComandoEnEspera;

use \DateTime;
use \DateInterval;
use Carbon\Carbon;


//controllersss
use App\Http\Controllers\Casino\CasinoController;
use App\Http\Controllers\Turnos\TurnosController;

class TodoController extends Controller
{
  private static $atributos = [
    'id_mesa_de_panio' => 'Identificacion de la mesa',
    'nro_mesa' => 'Número de Mesa',
    'nombre' => 'Nombre de Mesa',
    'descripcion' => 'Descripción',
    'id_tipo_mesa' => 'Tipo de Mesa',
    'id_juego_mesa' => 'Juego de Mesa',
    'id_casino' => 'Casino',
    'id_moneda' => 'Moneda',
    'id_sector_mesas' => 'Sector',
  ];

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware(['auth']);
  }

  //crea lo basico del casino:
  //mesas, juegos y sectores.
  public function quesehagalamagia()
  {
    //alta Casino
    // $casino = new Casino;
    // $casino->nombre = 'Master';
    // $casino->codigo = 'MTR';
    // $casino->fecha_inicio = '2018-12-17';
    // $casino->porcentaje_sorteo_mesas = '20';
    // $casino->save();
    // $this->actualizarMeses($casino->id_casino);
    // $tcontroller = new TurnosController;
    // $turnos = [[
    //             "nro" => "1",
    //             "desde" => "1",
    //             "hasta" => "7",
    //             "entrada" => "06:30",
    //             "salida" => "13:30",
    //           ],[
    //             "nro" => "2",
    //             "desde" => "1",
    //             "hasta" => "7",
    //             "entrada" => "12:30",
    //             "salida" => "19:30",
    //           ],[
    //             "nro" => "3",
    //             "desde" => "1",
    //             "hasta" => "7",
    //             "entrada" => "18:45",
    //             "salida" => "01:15",
    //           ],[
    //             "nro" => "4",
    //             "desde" => "1",
    //             "hasta" => "7",
    //             "entrada" => "00:45",
    //             "salida" => "07:00",
    //           ]
    //         ];
    //
    // foreach ($turnos as $tt) {
    //   $tcontroller->guardar($tt,$casino->id_casino);
    // }
    // $user = Auth::user();
    // $user->casinos()->attach($casino->id_casino);
    // //alta fichas
    // $fichas = Ficha::whereIn('id_ficha',[4,5,6,18,20,21])->get();
    //
    // foreach ($fichas as $ficha) {
    //   $f = new FichaTieneCasino;
    //   $f->ficha()->associate($ficha['id_ficha']);
    //   $f->casino()->associate($casino->id_casino);
    //   $f->save();
    // }
    $casino = Casino::find(6);
    //alta juegos
    $poker = JuegoMesa::create([
      'nombre_juego' => 'Póker Texas Bonus',
      'siglas' => 'PTB',
      'id_tipo_mesa' => 2,
      'id_casino' => $casino->id_casino,
      'posiciones' => 5
    ]);
    $ruleta = JuegoMesa::create([
      'nombre_juego' => 'Ruleta Americana',
      'siglas' => 'RA',
      'id_tipo_mesa' => 1,
      'id_casino' => $casino->id_casino,
      'posiciones' => 5
    ]);
    $blackjack = JuegoMesa::create([
      'nombre_juego' => 'Black Jack',
      'siglas' => 'BJ',
      'id_tipo_mesa' => 2,
      'id_casino' => $casino->id_casino,
      'posiciones' => 5
    ]);
    $craps = JuegoMesa::create([
      'nombre_juego' => 'Craps',
      'siglas' => 'CR',
      'id_tipo_mesa' => 3,
      'id_casino' => $casino->id_casino,
      'posiciones' => 5
    ]);
    //alta SectorMesas
    $sgeneral = SectorMesas::create([
                                    'descripcion' => 'GENERAL',
                                    'id_casino' => $casino->id_casino
                                  ]);

    //alta alta mesas
    $mesa1 = Mesa::create([
                          'nro_mesa' => 1,
                          'nombre' => 'RA01P',
                          'descripcion' => 'ruleta',
                          'nro_admin' => 1,
                          'id_juego_mesa' => $ruleta->id_juego_mesa,
                          'id_casino' => $casino->id_casino,
                          'id_moneda' => 1,
                          'id_sector_mesas' => $sgeneral->id_sector_mesas,
                          'multimoneda' => 0
                        ]);
    $mesa1->id_tipo_mesa = 1;
    $mesa1->save();
    //
    $mesa2 = Mesa::create([
                          'nro_mesa' => 2,
                          'nombre' => 'RA02D',
                          'descripcion' => 'ruletonga',
                          'nro_admin' => 2,
                          'id_juego_mesa' =>  $ruleta->id_juego_mesa,
                          'id_casino' => $casino->id_casino,
                          'id_moneda' => 2,
                          'id_sector_mesas' => $sgeneral->id_sector_mesas,
                          'multimoneda' => 0
                        ]);
    //
    $mesa2->id_tipo_mesa = 1;
    $mesa2->save();

    $mesa3 = Mesa::create([
                          'nro_mesa' => 3,
                          'nombre' => 'RA03',
                          'descripcion' => 'ruletita',
                          'nro_admin' => 3,
                          'id_juego_mesa' =>  $ruleta->id_juego_mesa,
                          'id_casino' => $casino->id_casino,
                          'id_moneda' => 1,
                          'id_sector_mesas' => $sgeneral->id_sector_mesas,
                          'multimoneda' => 0
                        ]);
    //
    $mesa3->id_tipo_mesa = 1;
    $mesa3->save();
    $mesa4 = Mesa::create([
                          'nro_mesa' => 1,
                          'nombre' => 'PTB01M',
                          'descripcion' => 'poker mm',
                          'nro_admin' => 1,
                          'id_juego_mesa' => $poker->id_juego_mesa,
                          'id_casino' => $casino->id_casino,
                          //'id_moneda' => ,
                          'id_sector_mesas' => $sgeneral->id_sector_mesas,
                          'multimoneda' => 1
                        ]);
    //
    $mesa4->id_tipo_mesa = 2;
    $mesa4->save();
    $mesa5 = Mesa::create([
                          'nro_mesa' => 2,
                          'nombre' => 'PTB02P',
                          'descripcion' => 'poker pesos 2',
                          'nro_admin' => 2,
                          'id_juego_mesa' => $poker->id_juego_mesa,
                          'id_casino' => $casino->id_casino,
                          'id_moneda' => 1,
                          'id_sector_mesas' => $sgeneral->id_sector_mesas,
                          'multimoneda' => 0
                        ]);
    //
    $mesa5->id_tipo_mesa = 2;
    $mesa5->save();
    $mesa6 = Mesa::create([
                          'nro_mesa' => 1,
                          'nombre' => 'BJ01P',
                          'descripcion' => 'blackk pesos 1',
                          'nro_admin' => 1,
                          'id_juego_mesa' => $blackjack->id_juego_mesa,
                          'id_casino' => $casino->id_casino,
                          'id_moneda' => 1,
                          'id_sector_mesas' => $sgeneral->id_sector_mesas,
                          'multimoneda' => 0
                        ]);
    //
    $mesa6->id_tipo_mesa = 2;
    $mesa6->save();
    $mesa7 = Mesa::create([
                          'nro_mesa' => 10,
                          'nombre' => 'CR10',
                          'descripcion' => 'craps 10',
                          'nro_admin' => 1,
                          'id_juego_mesa' => $craps->id_juego_mesa,
                          'id_casino' => $casino->id_casino,
                          'id_moneda' => 1,
                          'id_sector_mesas' => $sgeneral->id_sector_mesas,
                          'multimoneda' => 0
                        ]);
    //
    $mesa7->id_tipo_mesa = 3;
    $mesa7->save();
  }

  public function crearDatosMesAnterior(){
    $start = new Carbon('2019-04-01');
    $end = new Carbon('2019-04-30');
    // $end = new Carbon('twenty-eight day of February 2019');
    $anio = $start->format("Y");
    $mes = $start->format("m");
    $casino = Casino::find(6);

    //dd($anio,$mes,$casino);
    $importacionMensualPesos = new ImportacionMensualMesas;
    $importacionMensualPesos->fecha_mes = $anio.'-'.$mes.'-'.'01';
    $importacionMensualPesos->nombre_csv = 'autogenerated';
    $importacionMensualPesos->id_casino = $casino->id_casino;
    $importacionMensualPesos->id_moneda = 1;
    $importacionMensualPesos->total_drop_mensual += 0;
    $importacionMensualPesos->cotizacion_dolar += 0;
    $importacionMensualPesos->diferencias += 0;
    $importacionMensualPesos->utilidad_calculada += 0;
    $importacionMensualPesos->retiros_mes += 0;
    $importacionMensualPesos->reposiciones_mes += 0;
    $importacionMensualPesos->saldo_fichas_mes += 0;
    $importacionMensualPesos->total_utilidad_mensual += 0;
    $importacionMensualPesos->save();

    $importacionMensualDolares = new ImportacionMensualMesas;
    $importacionMensualDolares->fecha_mes = $anio.'-'.$mes.'-'.'01';
    $importacionMensualDolares->nombre_csv = 'autogenerated';
    $importacionMensualDolares->id_casino = $casino->id_casino;
    $importacionMensualDolares->id_moneda = 2;
    $importacionMensualDolares->total_drop_mensual += 0;
    $importacionMensualDolares->cotizacion_dolar += 0;
    $importacionMensualDolares->diferencias += 0;
    $importacionMensualDolares->validado = 1;
    $importacionMensualDolares->utilidad_calculada += 0;
    $importacionMensualDolares->retiros_mes += 0;
    $importacionMensualDolares->reposiciones_mes += 0;
    $importacionMensualDolares->saldo_fichas_mes += 0;
    $importacionMensualDolares->total_utilidad_mensual += 0;
    $importacionMensualDolares->save();

    for ($i=$start->format('d'); $i < $end->format('d'); $i++) {
      //alta cierres
      $dia = $i ;
      $importaciones = $this->crearCierresImportacionesDiarias($anio,$mes,$dia,$casino);
      foreach ($importaciones as $importacion) {
        $detalle = new DetalleImportacionMensualMesas;
        $detalle->fecha_dia = Carbon::parse($importacion->fecha)->format('d');
        $detalle->total_diario = $importacion->total_diario;
        $detalle->utilidad  = $importacion->utilidad_diaria_total;
        $detalle->cotizacion = $importacion->cotizacion;
        $detalle->retiros_dia = $importacion->total_diario_retiros;
        $detalle->reposiciones_dia = $importacion->total_diario_reposiciones;
        $detalle->saldo_fichas_dia = $importacion->saldo_diario_fichas;
        $detalle->diferencias = $importacion->diferencias;
        $detalle->utilidad_calculada_dia  = $importacion->utilidad_diaria_calculada;
        switch ($importacion->id_moneda) {
          case 1:
            $detalle->importacion_mensual_mesas()->associate($importacionMensualPesos);
            $importacionMensualPesos->total_drop_mensual += $detalle->total_diario;
            $importacionMensualPesos->cotizacion_dolar += $detalle->cotizacion;
            $importacionMensualPesos->diferencias += $detalle->diferencias;
            $importacionMensualPesos->validado = 1;
            $importacionMensualPesos->utilidad_calculada += $detalle->utilidad_calculada_dia;
            $importacionMensualPesos->retiros_mes += $detalle->retiros_dia;
            $importacionMensualPesos->reposiciones_mes += $detalle->reposiciones_dia;
            $importacionMensualPesos->saldo_fichas_mes += $detalle->saldo_fichas_dia;
            $importacionMensualPesos->total_utilidad_mensual += $detalle->utilidad;
            $importacionMensualPesos->save();

            break;
          case 2:
            $detalle->importacion_mensual_mesas()->associate($importacionMensualDolares);
            $importacionMensualDolares->total_drop_mensual += $detalle->total_diario;
            $importacionMensualDolares->cotizacion_dolar += $detalle->cotizacion;
            $importacionMensualDolares->diferencias += $detalle->diferencias;
            $importacionMensualDolares->validado = 1;
            $importacionMensualDolares->utilidad_calculada += $detalle->utilidad_calculada_dia;
            $importacionMensualDolares->retiros_mes += $detalle->retiros_dia;
            $importacionMensualDolares->reposiciones_mes += $detalle->reposiciones_dia;
            $importacionMensualDolares->saldo_fichas_mes += $detalle->saldo_fichas_dia;
            $importacionMensualDolares->total_utilidad_mensual += $detalle->utilidad;
            $importacionMensualDolares->save();

            break;
        }


        $detalle->save();
      }
      //alta aperturas
      //join de CierreApertura
    }

  }

  public function crearCierresImportacionesDiarias($anio,$mes,$dia,$casino)
  {
    $importaciones = array();
    $fecha = $anio.'-'.$mes.'-'.$dia;
    $monedas = Moneda::all();
    //dd($fecha);
    foreach ($monedas as $moneda) {
      $importacion = new ImportacionDiariaMesas;
      $importacion->casino()->associate($casino->id_casino);
      $importacion->moneda()->associate($moneda->id_moneda);
      $importacion->fecha = $fecha;
      $importacion->nombre_csv = $fecha.$casino->codigo.$moneda->siglas;
      $importacion->total_diario = 0;
      $importacion->diferencias = 0;
      $importacion->observacion = 0;
      $importacion->validado = 1;
      if($moneda->id_moneda == 2){
        $importacion->cotizacion = rand(30,50);
      }
      else {
        $importacion->cotizacion = 0;
      }
      $importacion->utilidad_diaria_calculada = 0;
      $importacion->saldo_diario_fichas = 0;
      $importacion->total_diario_retiros = 0;
      $importacion->total_diario_reposiciones = 0;
      $importacion->utilidad_diaria_total = 0;
      $importacion->save();
      $mesas = $casino->mesas()
      ->where('id_moneda','=', $moneda->id_moneda)
      ->inRandomOrder()
      ->take(3)
      ->get();
      //dd($mesas);
      foreach ($mesas as $mesa) {
        $cierre = $this->crearCierre($mesa, $fecha);
        $detalle_importacion = $this->crearDetalleImportacion($cierre, $mesa,
                                                      $casino, $importacion);
        $importacion->total_diario += $detalle_importacion->droop;
        $importacion->diferencias += $detalle_importacion->diferencia_cierre;
        $importacion->saldo_diario_fichas += $detalle_importacion->saldo_fichas;
        $importacion->total_diario_retiros += $detalle_importacion->retiros;
        $importacion->total_diario_reposiciones += $detalle_importacion->reposiciones;
        $importacion->utilidad_diaria_total = $detalle_importacion->utilidad;
        $importacion->utilidad_diaria_calculada = $detalle_importacion->utilidad_calculada;
        $importacion->save();
      }
      $importaciones[] = $importacion;
    }
    return $importaciones;
  }

  public function crearDetalleImportacion($cierre, $mesa,$casino, $importacion)
  {
    $detalle_importacion = new DetalleImportacionDiariaMesas;
    $detalle_importacion->importacion_diaria_mesas()->associate($importacion->id_importacion_diaria_mesas);
    $detalle_importacion->mesa()->associate($mesa->id_mesa_de_panio);
    $detalle_importacion->moneda()->associate($mesa->id_moneda);
    $detalle_importacion->fecha = $cierre->fecha;



    $last_cierre = Cierre::where('fecha','<',$cierre->fecha)
                          ->where('id_mesa_de_panio','=',$cierre->id_mesa_de_panio)
                          ->orderBy('fecha','desc')
                          ->get()->first();

    $random_drop = rand(1500,5000);
    $random_retiros = rand(0,4);
    $random_reposiciones = rand(0,4);
    $retiros = [0,0,0,200,200];
    $reposiciones = [0,0,0,0,5];

    if($last_cierre != null){
      $check_usado = DetalleImportacionDiariaMesas::where('id_ultimo_cierre',
                                            '=',$last_cierre->id_cierre_mesa)
                                                  ->get();
      if(count($check_usado) == 0 ||
        $check_usado->first()->id_detalle_importacion_diaria_mesas ==
        $detalle_importacion->id_detalle_importacion_diaria_mesas) {
          // calculo la diferencia entre ambos cierres
          $dif_cierres = $cierre->total_pesos_fichas_c-
                         $last_cierre->total_pesos_fichas_c ;



          //$detalle_importacion->utilidad = $calculado;
          $detalle_importacion->droop = $random_drop;
          $detalle_importacion->id_juego_mesa = $mesa->id_juego_mesa;
          $detalle_importacion->nro_mesa = $mesa->nro_admin;
          $detalle_importacion->nombre_juego = $mesa->juego->nombre_juego;
          $detalle_importacion->codigo_moneda = $cierre->moneda->siglas;
          $detalle_importacion->reposiciones = $reposiciones[$random_reposiciones];
          $detalle_importacion->retiros = $retiros[$random_retiros];
          $detalle_importacion->tipo_mesa = $mesa->juego->tipo_mesa->descripcion;


           //formula = (Cx+1 - Cx ) +DROP -FILL+CREDIT = UTILIDAD CALCULADA
           $calculado = $dif_cierres + $random_drop - $reposiciones[$random_reposiciones]
                       + $retiros[$random_retiros];
          $detalle_importacion->utilidad = $calculado;
           if ( $calculado == $detalle_importacion->utilidad) {
             $detalle_importacion->diferencia_cierre = 0;
           }
           else{
             $detalle_importacion->diferencia_cierre = abs($calculado - $detalle_importacion->utilidad);
           }
           $detalle_importacion->saldo_fichas = $dif_cierres;
           $detalle_importacion->utilidad_calculada = $calculado;
           $detalle_importacion->cierre()->associate($cierre->id_cierre_mesa);
           $detalle_importacion->cierre_anterior()->associate($last_cierre->id_cierre_mesa);
           $detalle_importacion->save();
      }else{
        dd('error, se creo importacion sin ultimo cierre sin usar.-');
      }
    }
    else {
      //no hay un cierre cierre_anterior
      $detalle_importacion->droop = $random_drop;
      $detalle_importacion->id_juego_mesa = $mesa->id_juego_mesa;
      $detalle_importacion->nro_mesa = $mesa->nro_admin;
      $detalle_importacion->nombre_juego = $mesa->juego->nombre_juego;
      $detalle_importacion->codigo_moneda = $cierre->moneda->siglas;
      $detalle_importacion->reposiciones = $reposiciones[$random_reposiciones];
      $detalle_importacion->retiros = $retiros[$random_retiros];
      $detalle_importacion->tipo_mesa = $mesa->juego->tipo_mesa->descripcion;
      $dif_cierres = $cierre->total_pesos_fichas_c;
      $calculado = $dif_cierres + $random_drop - $reposiciones[$random_reposiciones]
                  + $retiros[$random_retiros];
      $detalle_importacion->utilidad = $calculado;
      $detalle_importacion->diferencia_cierre = 0;
      $detalle_importacion->saldo_fichas = $dif_cierres;
      $detalle_importacion->utilidad_calculada = $calculado;
      $detalle_importacion->cierre()->associate($cierre->id_cierre_mesa);
      //$detalle_importacion->cierre_anterior()->associate($cierre->id_cierre_mesa);
      $detalle_importacion->save();

    }
    $detalle_importacion->save();
    return $detalle_importacion;
  }

  private function crearCierre($mesa, $fecha)
  {
    $hora_inicio = rand(0,2).rand(0,3).':'.rand(0,2).rand(0,9);
    $hora_fin = rand(0,2).rand(0,3).':'.rand(0,2).rand(0,9);

    if($mesa->multimoneda){
      $id_moneda = rand(1,2);
    }

    $cierre = new Cierre;
    $cierre->fecha =$fecha;
    $cierre->hora_inicio = $hora_inicio;
    $cierre->hora_fin = $hora_fin;

    $cierre->fiscalizador()->associate(Auth::user()->id);
    $cierre->mesa()->associate($mesa->id_mesa_de_panio);
    $cierre->moneda()->associate($mesa->id_moneda);
    $cierre->tipo_mesa()->associate($mesa->juego->tipo_mesa->id_tipo_mesa);
    $cierre->casino()->associate($mesa->id_casino);
    $cierre->estado_cierre()->associate(3);
    $cierre->save();
    $detalles = array();
    $total_pesos_fichas_c = 0;

    $fichas = FichaTieneCasino::join('ficha','ficha.id_ficha','=','ficha_tiene_casino.id_ficha')
                                ->where('id_casino','=',$mesa->id_casino)
                                ->where('id_moneda','=',$cierre->id_moneda)
                                ->get();

    foreach ($fichas as $f) {
      $randomBoolean = rand(0,1);
      if($randomBoolean) {
        $ficha = new DetalleCierre;
        $ficha->ficha()->associate($f->id_ficha);
        $ficha->monto_ficha = $f->valor_ficha * rand(1,20);
        $ficha->cierre()->associate($cierre->id_cierre_mesa);
        $ficha->save();
        $detalles[] = $ficha;
        $total_pesos_fichas_c+=  $ficha->monto_ficha;
      }
    }
    if($total_pesos_fichas_c != 0){
      $cierre->total_pesos_fichas_c = $total_pesos_fichas_c;
      $cierre->save();
    }
    else {
      $ficha = new DetalleCierre;
      $f = $fichas->first();
      $ficha->ficha()->associate($f->id_ficha);
      $ficha->monto_ficha = $f->valor_ficha * rand(1,20);
      $ficha->cierre()->associate($cierre->id_cierre_mesa);
      $ficha->save();

      $cierre->total_pesos_fichas_c = $ficha->monto_ficha;
      $cierre->save();
    }

    return $cierre;
  }

  public function actualizarMeses($id_casino){
    $casino = Casino::find($id_casino);
    $meses = $casino->meses;
    foreach($meses as $mes){
      $mes->casino()->dissociate();
      $mes->delete();
    }
    $ff = explode('-',$casino->fecha_inicio);
    $nombres = ['Enero','Febrero','Marzo','Abril','Mayo',
                'Junio','Julio','Agosto','Septiembre',
                'Octubre','Noviembre','Diciembre'
                ];

    $nro_cuota = 1;
    for ($i=0; $i <= 11 ; $i++) {
      $fecha = Carbon::createFromDate($ff[0],$ff[1],$ff[2])->addMonths($i);
      if($ff[1] == $fecha->month){
        if($ff[2] == '01'){
          $mes = new MesCasino;
          $mes->nombre_mes = $nombres[$fecha->format('n')-1];
          $mes->nro_cuota = $nro_cuota;
          $mes->dia_inicio = 1;
          $mes->dia_fin = $fecha->daysInMonth;
          $mes->nro_mes = $fecha->format('n');
          $mes->casino()->associate($casino->id_casino);
          $mes->save();
          $nro_cuota++;
        }else{
          $mes1 = new MesCasino;
          $mes1->nombre_mes = $nombres[$fecha->format('n')-1].' 01 al '.($fecha->day-1);
          $mes1->nro_cuota = 13;
          $mes1->dia_inicio = 1;
          $mes1->dia_fin = $fecha->day-1;
          $mes1->nro_mes = $fecha->format('n');
          $mes1->casino()->associate($casino->id_casino);
          $mes1->save();
          $mes2 = new MesCasino;
          $mes2->nombre_mes = $nombres[$fecha->format('n')-1].($fecha->day).' al '.$fecha->daysInMonth;
          $mes2->nro_cuota = $nro_cuota;
          $mes2->dia_inicio = $ff[2];
          $mes2->dia_fin = $fecha->daysInMonth;
          $mes2->nro_mes = $fecha->format('n');
          $mes2->casino()->associate($casino->id_casino);
          $mes2->save();
          $nro_cuota++;
        }
      }else{
        $mes2 = new MesCasino;
        $mes2->nombre_mes = $nombres[$fecha->format('n')-1];
        $mes2->nro_cuota = $nro_cuota;
        $mes2->dia_inicio = 1;
        $mes2->dia_fin = $fecha->daysInMonth;
        $mes2->nro_mes = $fecha->format('n');
        $mes2->casino()->associate($casino->id_casino);
        $mes2->save();
        $nro_cuota++;
      }
    }
  }

}
