<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use App\User;
use Auth;
use Illuminate\Http\Request;
use Response;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\View\View;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function resetPassword(Request $request){
      $validator = Validator::make($request->all(), [
        'user_name' => ['required' , 'exists:users,user_name'] ,
        'dni' => ['required'],
       ])->after(function ($validator){
         $user =  User::where('user_name','=',$validator->getData()['user_name'])->firstOrFail();

         $texto = 'El n° de DNI ingresado no corresponde al nombre de usuario '.$user->name.'.';
         if($validator->getData()['dni'] != $user->dni){
            $validator->errors()->add('dni', $texto);
          }

        });

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }

      $user = User::where('user_name','=',$request->user_name )->firstOrFail();

      $ps = bcrypt($user->dni);
      $user->password = $ps;
      $user->save();

      return redirect('login');
    }
}
