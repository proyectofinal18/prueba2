<?php

namespace App\Http\Controllers\RolesPermissions;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;

use App\Seccion;

class PermissionController extends Controller
{
  /**
   * Create instances for messages.
   *
   * @return coso
   */
    private static $atributos=[
      'descripcion' => 'Nombre del Permiso',
    ];
    public function __construct()
    {
        $this->middleware(['auth', 'permission:ABM Roles y Permisos|ABMC Usuarios']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();

        return view('permissions.index')->with('permissions', $permissions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();

        return view('permissions.create')->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $val = Validator::make($request->all(),[
            'descripcion'=>'required|max:40|unique:permissions,name',
            'id_seccion' => 'required|exists:secciones,id_seccion',
            ],array(),self::$atributos)->after(function($validator){

                //validar que descripcion no exista
                $descripcion =strtolower($validator->getData()['descripcion']);
                $resultado=Permission::where('name' , '=' , $descripcion )
                                      ->get();
                if($resultado->count() >= 1){
                    $validator->errors()
                        ->add('existe', 'Ya existe rol con misma descripción.');
                }
              });
        $val->validate();

        $name = $request['descripcion'];
        $permission = new Permission();
        $permission->name = strtolower($name);
        $permission->id_seccion = $request->id_seccion;

        $roles = $request['roles'];

        $permission->save();

        if (!empty($request['roles'])) {
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                $permission = Permission::where('name', '=', $name)->first();
                $r->givePermissionTo($permission);
            }
        }

        return ['permiso' =>$permission ,
                'roles' => $permission->roles()->get()];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('permissions');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::find($id);

        return view('permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $permission = Permission::findOrFail($id);
    //
    //     $val = Validator::make($request->all(),[
    //           'descripcion'=>'required|max:40',
    //           'id' => 'required',
    //           'id_seccion' => 'required|exists:secciones,id_seccion',
    //       ],array(),self::$atributos)->after(function($validator){
    //       //validar que descripcion no exista
    //       $descripcion =strtolower($validator->getData()['descripcion']);
    //       $resultados=Permission::where('name' , '=' , $descripcion )->get();
    //       $permission = Permission::findOrFail($validator->getData()['id']);
    //       if($resultados->count() >= 1){
    //         foreach ($resultados as $resultado) {
    //           if($permission->id != $resultado->id &&
    //              $descripcion == $resultado->name){
    //             $validator->errors()
    //                 ->add('existe', 'Ya existe permiso con misma descripción.');
    //           }
    //         }
    //
    //       }
    //     });
    //     $val->validate();
    //
    //     $permission->name=strtolower($request->descripcion);
    //     $permission->id_seccion = $request->id_seccion;
    //     $permission->save();
    //
    //
    //
    //     if(!empty($request->roles)){
    //       $permission->roles()->sync($request->roles);
    //     }else{
    //       $permission->roles()->detach();
    //     }
    //
    //     return ['permiso' => $permission,
    //             'roles' => $permission->roles()->get()];
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     $permission = Permission::findOrFail($id);
    //     $roles = $permission->roles()->get();
    //     // if ($permission->name == "SUPERUSUARIO") {
    //     //     return redirect()->route('roles')
    //     //     ->with('flash_message',
    //     //      'No puede eliminar este permiso!');
    //     // }
    //
    //     $permission->delete();
    //
    //     return ['permiso' => $permission , 'roles' => $roles];
    // }

    public function getPermiso($id){
      $permiso=Permission::findorfail($id);
      $seccion = Seccion::find($permiso->id_seccion);
      return ['permiso' => $permiso,'seccion' =>$seccion , 'roles' => $permiso->roles()->get() ];

    }

    public function getAll(){
        $permisos=Permission::all();
        return  $permisos;
    }

    public function buscarPermisos(Request $request){
      if(empty($request->permiso) ){
      $permiso='%';
      }
      else{
        $permiso= '%' . $request->permiso . '%';
      }
      $resultado = DB::table('permissions')
                      ->select('permissions.*','secciones.nombre_seccion as seccion')
                      ->leftJoin('secciones','permissions.id_seccion','=','secciones.id_seccion')
                      ->where([
                             ['name' ,'like' , $permiso],
                            ])
                      ->orderBy('name','asc')
                      ->get();
      return ['permiso' => $resultado];
    }

    public function buscarPermisosPorRoles(Request $request) {
      $roles = array();

      if ($request->roles != null) {
        foreach ($request->roles as $rol) {
          $roles[] = $rol;
        }
      }

      if($roles != null) {
        $permisos = DB::table("permissions")
                        ->select("permissions.*",'secciones.*')
                        ->join("role_has_permissions","permissions.id","=",
                                            "role_has_permissions.permission_id")
                        ->join('secciones','secciones.id_seccion','=','permissions.id_seccion')
                        ->whereIn("role_has_permissions.role_id",$roles)
                        ->orderBy('name','asc')
                        ->distinct()
                        ->get();

        return ["permisos" => $permisos];

      }
    }
    public function getSecciones(){
      return ['secciones' => Seccion::all()];
    }

    public function obtenerPermisosPorSeccion(){
      $permisos = DB::table("permissions")
                      ->select("permissions.*",'secciones.*')
                      ->join('secciones','secciones.id_seccion','=','permissions.id_seccion')
                      ->orderBy('name','asc')
                      ->distinct()
                      ->get()
                      ->sortBy('nombre_seccion')
                      ->groupBy('nombre_seccion');
      $respuesta = array();
      foreach ($permisos as $key => $value) {
        $respuesta[] = [
          'seccion' => $key,
          'permisos' =>$value
        ];
      }
      // $permisos = DB::table("permissions")
      //                 ->select("permissions.name",'secciones.nombre_seccion'
      //                          "permissions.id"
      //                         )
      //                 ->join('secciones','secciones.id_seccion','=','permissions.id_seccion')
      //                 ->orderBy('name','asc')
      //                 ->groupBy('secciones.nombre_seccion',"permissions.name"
      //                           "permissions.id")
      //                 ->orderBy('nombre_seccion','asc')
                      // ->get();
      return $respuesta;
    }

}
