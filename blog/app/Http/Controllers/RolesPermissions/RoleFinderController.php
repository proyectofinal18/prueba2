<?php

namespace App\Http\Controllers\RolesPermissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;
use App\User;

use Illuminate\Support\Facades\DB;
class RoleFinderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getRol($id){
      $rol=Role::findorfail($id);
      $permisos = DB::table('permissions')->select('permissions.*','secciones.*')
                      ->join('role_has_permissions','permissions.id','=','role_has_permissions.permission_id')
                      ->leftJoin('secciones','permissions.id_seccion','=','secciones.id_seccion')
                      ->where('role_id','=',$id)
                      ->get();
      return ['rol' => $rol ,
       'permisos' =>  $permisos,
        'usuarios' => $users = User::role($rol->name)->get()];
    }

    public function getAll(){
      $todos=Role::all();
      return $todos;
    }

    public function getMisRoles(){
      $user = Auth::user();
      $misRoles = $user->roles;
      $arraymisroles = array();
      //dd($misRoles);
      foreach ($misRoles as $mirol) {
        $arraymisroles[] = $mirol->id;
      }
      $arraymiscasinos = array();
      foreach ($user->casinos as $cas) {
        array_push($arraymiscasinos,''.$cas->id_casino);
      }
      $autorizacion = DB::table('rol_crea_rol')
                          ->whereIn('id_casino',$arraymiscasinos)
                          ->whereIn('id_rol_creador',$arraymisroles)
                          ->get();
      $idsRoles = $arraymisroles;
      foreach ($autorizacion as $aut) {
        if(!in_array()){
          
        }
      }
    }

    public function buscarRoles(Request $request){
      $reglas=array();
      if(empty($request->rol) ){
        $reglas[]= ['name' ,'like' , '%'];
      }
      else{
        $reglas[] = ['name' ,'like' , '%' . $request->rol . '%'];
      }

      $roles = Role::where($reglas)->get();

      $resultado=array();
      foreach ($roles as $rol) {
          $listaPermisos=array();
          $permisos =$rol->permissions()->get();
          foreach ($permisos as $permiso) {
            $listaPermisos[]=$permiso->descripcion;
          }
          $unRol=['rol' => $rol, 'permisos' => $listaPermisos];
          $resultado[]=$unRol;
      }

      return ['roles' => $resultado];
    }

}
