<?php

namespace App\Http\Controllers\RolesPermissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
USE Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;
use App\User;
use Validator;

class RoleController extends Controller
{
  /**
   * Create instances for messages.
   *
   * @return coso
   */
    private static $atributos=[
      'descripcion' => 'Nombre del Rol',
      'permisos' => 'Permiso/s del Rol',
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware(['auth', 'permission:ABM Roles y Permisos']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        //return view('roles.index')->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();

      //  return view('roles.create', ['permissions'=>$permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'descripcion'=>'required|max:45',
            'permisos' =>'required',//lista de permisos
            ],array(),self::$atributos)->after(function ($validator){
              //validar que descripcion no exista
              $descripcion =strtoupper($validator->getData()['descripcion']);
              $resultado=Role::where('name' , '=' , $descripcion )->get();
              if($resultado->count() >= 1){
                $validator->errors()->add('existe', 'Ya existe rol con misma descripción.');
              }
            })->validate();

        $name = $request['descripcion'];
        $role = new Role();
        $role->name = $name;
        $permissions = $request['permisos'];

        $role->save();

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            $role = Role::where('name', '=', $name)->first();
            $role->givePermissionTo($p);
        }

        return ['rol' => $role , 'permisos' => $role->permissions()->get()];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('roles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();

        return view('roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //hay que pasar a mayusculas la descripcion en la pantalla
        $role = Role::findOrFail($id);
        $validator=Validator::make($request->all(),[
            'descripcion'=>'required|max:45',
            'permisos' =>'required',
            'id' => 'required',
        ],array(),self::$atributos)->after(function ($validator){
          //validar que descripcion no exista
          $descripcion =strtoupper($validator->getData()['descripcion']);
          $resultados=Role::where('name' , '=' , $descripcion )->get();
          $role = Role::findOrFail($validator->getData()['id']);

          if($resultados->count() >= 1){
            foreach ($resultados as $resultado) {
              if($role->id != $resultado->id && $descripcion == $resultado->name){
                $validator->errors()->add('existe', 'Ya existe rol con misma descripción.');
              }
          }
        }
      });
        $validator->validate();

        $role->name = $request['descripcion'];
        $role->save();

        $input = $request->except(['permisos']);
        $permissions = $request['permisos'];
        $role->save();
        $p_all = Permission::all();

        foreach ($p_all as $p) {
            $role->revokePermissionTo($p);
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form permission in db
            $role->givePermissionTo($p);
        }

        $users=User::join('model_has_roles as model',
                    'model.model_id','=','users.id')
                    ->where('model.role_id', '=', $role->id)
                    ->get();

        if(!empty($users)){
              foreach ($users as $user) {
                $permisos = $role->permissions()->get();
                $user->syncPermissions($permisos);
              }
            }

        return ['rol' => $role, 'permisos' => $role->permissions()->get()];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
       $users = User::role($role->name)->get();
        if(count($users)== 0){

          $role->delete();

          return ['rol' => $role];
        }
        else {
          return response()->json(['error' => 'No es posible eliminar el rol.'], 405);
        }

    }

    public function buscarTodo(){
        $roles= Role::orderBy('name' , 'asc')->get();
        $permisos = DB::table('permissions')->select('permissions.*','secciones.*')
                        ->leftJoin('secciones','permissions.id_seccion','=','secciones.id_seccion')
                        ->orderBy('name','asc')
                        ->get();
        $secciones = DB::table('secciones')->select('nombre_seccion as seccion','id_seccion')->get();
        // dd($secciones);
        return view('RolesPermissions/seccionRolesPermisos' , ['roles' => $roles,
                                                             'permisos' => $permisos,
                                                             'secciones' => $secciones
                                                           ]);
      }



}
