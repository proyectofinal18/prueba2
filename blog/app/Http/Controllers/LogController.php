<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Log;
use Illuminate\Support\Facades\DB;
use Response;

use Carbon\Carbon;

class LogController extends Controller
{
  private static $instance;

  public static function getInstancia(){
    if (!isset(self::$instance)) {
      self::$instance = new LogController();
    }
    return self::$instance;
  }

  public function buscarTodo(){
    $user = Auth::user();
    $casinos = $user->casinos->all();
    return view('LogActividades/seccionLogActividades', ['casinos' => $casinos]);//->with('logActividades',$logs);
  }

  public function guardarLog($accion,$tabla,$id_entidad,$casino){
      $log = new Log;
      $log->accion = $accion;
      $log->tabla = $tabla;
      if(Auth::user()){
        $id_usuario = Auth::user()->id;
        $log->usuario()->associate($id_usuario);
      }else{
        if($tabla != 'users'){
          //es testing
          $log->usuario()->associate(1);
        }else{
          //se reseteó la contraseña del usuario $id_entidad
          $log->usuario()->associate($id_entidad);
        }

      }
      if($casino == null){
        $log->casino()->associate(Auth::user()->casinos()->first());
      }else{
        $log->casino()->associate($casino->id_casino);
      }

      $log->id_entidad = (int) $id_entidad;

      $log->fecha = date_create();
      $log->save();
      return $log;
  }

  public function getAll(){
    $todos=Log::all();
    return $todos;
  }

  public function obtenerLogActividad($id){
    $log = Log::find($id);
    $fecha = Carbon::parse($log->fecha);
    //dd($fecha->year);
    $anterior = Log::where('id_entidad','=',$log->id_entidad)
                    ->where('tabla','=',$log->tabla)
                    ->whereYear('fecha','<=',$fecha->year)
                    ->whereMonth('fecha','<=',$fecha->month)
                    ->whereDay('fecha','<=',$fecha->day)
                    ->where('id_casino','=',$log->id_casino)
                    ->get()->first();
    return ['log' => $log,
            'usuario' => $log->usuario->name,
            'detalles' => $log->detalles,
            'casino' => ['nombre' =>$log->casino->nombre],
            'anterior' => $anterior,
            'detalles_anterior' => $anterior->detalles
          ];
  }

  public function buscarLogActividades(Request $request){
    $reglas = Array();
    if(!empty($request->usuario))
      $reglas[]=['users.name', 'like', '%'.$request->usuario.'%'];

    if(!empty($request->id_casino) && $request->id_casino != 0){
      $reglas[]= ['log.id_casino','=', $request->id_casino];
    }
    if(!empty($request->tabla))
      $reglas[]=['log.tabla', 'like', '%'.$request->tabla.'%'];
    if(!empty($request->accion))
      $reglas[]=['log.accion', 'like', '%'.$request->accion.'%'];
    if(!empty($request->fecha)){
      $fechaMax = date("Y-m-d", strtotime("+1 day", strtotime($request->fecha)));
      $reglas[]=['log.fecha', '>=', $request->fecha];
      $reglas[]=['log.fecha', '<=', $fechaMax];
    }

    $sort_by = $request->sort_by;
     $resultados=DB::table('log')
    ->select('log.*','users.name','casino.nombre')
    ->join('users','users.id','=','log.id_usuario')
    ->join('casino','casino.id_casino','=', 'log.id_casino')
    ->when($sort_by,function($query) use ($sort_by){
                    return $query->orderBy($sort_by['columna'],$sort_by['orden']);
                })
    ->where($reglas)->paginate($request->page_size);

    return $resultados;
  }

  //tipo_archivo,casino,fecha
  public function obtenerUltimasImportaciones(){
    $usuario = Auth::user();
    $casinos = array();
    foreach($usuario->casinos as $casino){
      $casinos [] = $casino->id_casino;
    }
    $importaciones = DB::table('log')->whereIn('log.tabla',['contador_horario','producido','beneficio'])
                                     ->join('usuario_tiene_casino','log.id_usuario','=','usuario_tiene_casino.id_usuario')
                                     ->join('casino','usuario_tiene_casino.id_casino','=','casino.id_casino')
                                     ->whereIn('casino.id_casino',$casinos)
                                     ->select('log.tabla as tipo_archivo','casino.nombre as casino','log.fecha as fecha')
                                     ->orderBy('log.fecha','desc')->take(10)->get();

    return $importaciones;
  }

}
