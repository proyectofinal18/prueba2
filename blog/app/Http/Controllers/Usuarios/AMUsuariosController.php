<?php

namespace App\Http\Controllers\Usuarios;

use Auth;
use Session;
use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Hash;

use App\User;

use App\Seccion;
use App\Casino;
use App\SecRecientes;
use App\Http\Controllers\RolesPermissions\RoleFinderController;
use App\Http\Controllers\Usuarios\SeccionesRecientesController;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AMUsuariosController extends Controller
{
  private static $atributos = [
    'user_name' => 'Nombre Usuario',
    'password' => 'Contraseña',
    'usuario' => 'Nombre de Usuario' ,
    'email' => 'Email',
    'password' => 'Contraseña',
    'name' => 'Nombre y Apellido',
    'imagen' => 'Imagen',
    'dni' => 'DNI',
    'casinos' => 'Casinos',
    'roles' => 'Roles',
    'password_nuevo_confirmation' => 'Repita Nueva Contraseña',
    'password_nuevo' => 'Nueva Contraseña',
  ];

  private static $instance;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware(['auth','permission:ABMC Usuarios']);
  }

  public function guardarUsuario(Request $request){

    /*
    validacion
    */
    $validator=Validator::make($request->all(), [
      'usuario' => ['required' , 'max:45', "regex:/^([a-zA-Z]*)$/" , 'unique:users,user_name'] ,
      'email' => ['required' , 'max:45' , 'email','unique:users,email'],
      'nombre' => ['required', 'max:150',"regex:/^((([Á,É,Ó,Ú,Í])?([a-zA-Z])*)+(([á,é,í,ú,ó])*)+(([Á,É,Ó,Ú,Í])*)+(([',. -][a-zA-Z])?([',. -][Á,É,Ó,Ú,Í])?([á,é,í,ú,ó])?[a-zA-Z]*)*)*$/"],
      'imagen' => ['nullable', 'image'],
      'dni' => 'required|integer|min:10000000|unique:users,dni',
      'casinos' => 'required',
      'casinos.*.id_casino' => 'required|exists:casino,id_casino',
      'roles' => 'required',
      'roles.*.id_rol' => 'required|exists:roles,id',
     ],array(),self::$atributos)->after(function ($validator){
       $validator = $this->validarPermisosCreacion($validator);
     })->validate();
      //dejar este
    if(isset($validator)){
      if ($validator->fails()){
          return ['errors' => $validator->messages()->toJson()];
          }
        }
      $nombre=$request->nombre;
      $pass=$request->dni;
      $username=$request->usuario;
      $email=$request->email;
      $roles=$request->roles;
      //falta validar que sea de al menos un casino
      $casinos = $request->casinos;
      /*
      crea modelo
      */
      $usuario= new User;
      $usuario->name=$nombre;
      $usuario->dni = $request->dni;
      $usuario->user_name=$username;
      $usuario->password=bcrypt($pass);
      $usuario->email=$email;
      if($request->imagen != null){
        $usuario->imagen = base64_encode(file_get_contents($request->imagen->getRealPath()));
      }
      $usuario->save();
      $permisos = collect();
      $array_permisos = array();
      if(!empty($roles)){
        foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();
                $usuario->assignRole($role_r); //Assigning role to user
                $permisos = $permisos->concat($role_r->permissions()->get());

        }
        foreach ($permisos as $p) {
          if(!in_array($p->id,$array_permisos)){
            $array_permisos[] = $p->id;
          }
        }
        $usuario->syncPermissions($array_permisos);
      }
      if(!empty($casinos)){
        $usuario->casinos()->sync($casinos);
      }
      $usuario->save();

      $secRecCont = new SeccionesRecientesController;
      $secRecCont->initSeccionReciente($usuario);
      return ['usuario' => $usuario];
  }

  public function modificarUsuario(Request $request){

    $user = Auth::user();

    $validator=Validator::make($request->all(),[
        'id_usuario' =>'required|exists:users,id',
        'email' => 'required|max:45|email',
        'casinos' => 'required',
        'roles' => 'required',
    ], array(), self::$atributos)->after(function($validator){

    })->validate();
      if(isset($validator)){
        if ($validator->fails()){
            return ['errors' => $validator->messages()->toJson()];
            }
       }

    $usuario=User::find($request->id_usuario);
    $usuario->email = $request->email;
    // $usuario->password = $request->password;
    $usuario->save();

    $roles=$request->roles;
    $casinos=$request->casinos;
    $permisos = collect();
    $array_permisos = array();
        if(!empty($roles)){
          $usuario->roles()->detach();
          foreach ($roles as $role) {
                  $role_r = Role::where('id', '=', $role)->firstOrFail();
                  $usuario->assignRole($role_r); //Assigning role to user
                  $permisos = $permisos->concat($role_r->permissions()->get());

          }
          foreach ($permisos as $p) {
            if(!in_array($p->id,$array_permisos)){
              $array_permisos[] = $p->id;
            }
          }
          $usuario->syncPermissions($array_permisos);
        }else{
          $usuario->roles()->detach();
          $usuario->permissions()->detach();
        }
        if(!empty($casinos)){
          $usuario->casinos()->sync($casinos);
        }else {
          $usuario->casinos()->detach();
        }

    return ['usuario' => $usuario];
  }

  public function eliminarUsuario(Request $request){
    $usuario= User::find($request->id);
    //supongo que no se deben hacer detachs porque usa SoftDeletes..
    $usuario->delete();
    return ['usuario' => $usuario];
  }

  private function validarPermisosCreacion($validator){
    if(!empty($validator->getData()['casinos']) && !empty($validator->getData()['roles'])){
      $user = Auth::user();
      $misRoles = $user->roles;
      $arraymisroles = array();
      //dd($misRoles);
      foreach ($misRoles as $mirol) {
        $arraymisroles[] = $mirol->id;
      }
      $arraymiscasinos = array();
      foreach ($user->casinos as $cas) {
        array_push($arraymiscasinos,''.$cas->id_casino);
      }
      $arraysuscasinos = array();
      foreach ($validator->getData()['casinos'] as $cas) {
        array_push($arraysuscasinos, $cas['id_casino']);
        if(!in_array($cas['id_casino'],$arraymiscasinos)) {
          $validator->errors()->add('casinos', 'No esta autorizado a crear usuarios con algún casino.');
        }
      }

      if(!in_array(1,$arraymisroles)){//si no es SUPERUSUARIO
        foreach ($validator->getData()['roles'] as $idrol) {
          $autorizacion = DB::table('rol_crea_rol')
                              ->whereIn('id_casino',$arraysuscasinos)
                              ->whereIn('id_rol_creador',$arraymisroles)
                              ->where('id_rol_a_crear',$idrol['id_rol'])
                              ->get()->first();

          if($autorizacion == null && !in_array($idrol['id_rol'],$arraymisroles)){
            $validator->errors()->add('roles', 'No esta autorizado a crear usuarios con algún rol.');
          }
        }
      }

    }
    return $validator;
  }

}
