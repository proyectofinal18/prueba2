<?php

namespace App\Http\Controllers\Usuarios;

use Auth;
use Session;
use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Hash;

use App\User;

use App\Seccion;
use App\Casino;
use App\SecRecientes;
use App\Http\Controllers\RolesPermissions\RoleFinderController;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class BuscarUsuariosController extends Controller
{
  private static $atributos = [
    'user_name' => 'Nombre Usuario',
    'password' => 'Contraseña',
    'usuario' => 'Nombre de Usuario' ,
    'email' => 'Email',
    'password' => 'Contraseña',
    'name' => 'Nombre y Apellido',
    'imagen' => 'Imagen',
    'dni' => 'DNI',
    'casinos' => 'Casinos',
    'roles' => 'Roles',
    'password_nuevo_confirmation' => 'Repita Nueva Contraseña',
    'password_nuevo' => 'Nueva Contraseña',
  ];

  private static $instance;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware(['auth']);
  }

  public static function getInstancia() {
      if (!isset(self::$instance)) {
          self::$instance = new BuscarUsuariosController();
      }
      return self::$instance;
  }

    public function buscarUsuariosPorNombre($nombre){

      if(empty($nombre) ){
      $nombre = '%';
      }
      else{
        $nombre = '%'.$nombre.'%';
      }
      $resultado = User::where([['name','like',$nombre]])->get();
      return ['usuarios' => $resultado];
    }

    public function buscarUsuarios(Request $request){
      $user = Auth::user();
      $misRoles = $user->roles;
      $arraymisroles = array();
      //dd($misRoles);
      foreach ($misRoles as $mirol) {
        $arraymisroles[] = $mirol->id;
      }
      $casinos = array();
      foreach($user->casinos as $casino){
        $casinos[]=$casino->id_casino;
      }
      if (!in_array(1,$arraymisroles)) {
        $puedegestionar = DB::table('rol_crea_rol')
                            ->select('rol_crea_rol.id_rol_a_crear')
                            ->whereIn('id_casino',$casinos)
                            ->whereNotIn('id_rol_a_crear',$arraymisroles)
                            ->whereIn('id_rol_creador',$arraymisroles)
                            ->get()->all();
        foreach ($puedegestionar as $rol) {
          array_push($arraymisroles,$rol->id_rol_a_crear);
        }
      }else{
        $roles = Role::all();
        $arraymisroles = array();
        foreach ($roles as $r) {
          array_push($arraymisroles,$r->id);
        }
      }

      $reglas=array();
      if(!empty($request->nombre)){
        $reglas[]=['users.name' , 'like' , '%' . $request->nombre . '%'];
      }
      if(!empty($request->id_casino) && $request->id_casino != 0){
        $casino= $request->id_casino;
        $reglas[]=['usuario_tiene_casino.id_casino','=',$request->id_casino];
      }
      if(!empty($request->usuario)){
        $reglas[]=['users.user_name' , 'like' , '%' . $request->usuario . '%'];
      }
      if(!empty($request->email)){
        $reglas[]=['users.email' , '=' , $request->email];
      }

      $sort_by = $request->sort_by;
      //dd($arraymisroles);
      $usuarios = DB::table('users')
                    ->select('users.*')
                    ->join('usuario_tiene_casino',
                          'users.id','=','usuario_tiene_casino.id_usuario')
                    ->join('model_has_roles', function ($join) use($arraymisroles) {
                          $join->on('users.id', '=', 'model_has_roles.model_id')
                               ->whereIn('model_has_roles.role_id', $arraymisroles);
                      })
                    ->where($reglas)
                    ->whereIn('usuario_tiene_casino.id_casino',$casinos)
                    ->whereNull('users.deleted_at')
                    ->distinct('users.id')
                    ->when($sort_by,function($query) use ($sort_by){
                                    return $query->orderBy($sort_by['columna'],$sort_by['orden']);
                                })
                    ->paginate($request->page_size);
      return ['usuarios'=>$usuarios];
    }

    public function buscarTodo(){
      $req = new Request;
      //$resultados= $this->buscarUsuarios($req);
      $rolFController= new RoleFinderController;
      $user = Auth::user();
      $casinos = $user->casinos->all();
      $roles_user = $user->roles;

      $arraymisroles = array();
      //dd($misRoles);
      foreach ($roles_user as $mirol) {
        $arraymisroles[] = $mirol->id;
      }
      $arraymiscasinos = array();
      foreach ($user->casinos as $cas) {
        array_push($arraymiscasinos,''.$cas->id_casino);
      }

      if(!in_array(1,$arraymisroles)){
        $roles_a_crear= Role::join('rol_crea_rol','rol_crea_rol.id_rol_a_crear','=','roles.id')
                            ->whereIn('rol_crea_rol.id_rol_creador',$arraymisroles)
                            ->whereIn('rol_crea_rol.id_casino',$arraymiscasinos)
                            ->get();
        $roles = $roles_user->union($roles_a_crear);
      }else{
        $roles = Role::all();
      }



      return view('Usuarios.seccionUsuarios',  ['usuarios' => [] , 'roles' => $roles , 'casinos' => $casinos]);
    }

    public function buscarUsuario($id){
      $usuario=User::findOrFail($id);

      $roles = DB::table("roles")
                      ->select("roles.id","roles.name")
                      ->join("model_has_roles","roles.id","=",
                                          "model_has_roles.role_id")
                      ->where("model_has_roles.model_id",'=',$id)
                      ->distinct()
                      ->get();
      $permisos = User::findOrFail($id)->getPermissionsViaRoles()->sortBy('name')->values();
      return ['usuario' => $usuario,
              'roles' => $roles ,
              'casinos' => $usuario->casinos,
              'permisos' => $permisos
              ];
    }

    public function buscarFiscaNombreCasino($id_casino, $nombre){
      $nombre = (empty($nombre)) ? '%' : '%'.$nombre.'%';

      $resultado = User::role('FISCALIZADOR')
                          ->join('usuario_tiene_casino',
                                'usuario_tiene_casino.id_usuario','=','users.id')
                          ->where([['name','like',$nombre],
                              ['usuario_tiene_casino.id_casino','=',$id_casino]])
                          ->get();

      return ['usuarios' => $resultado];
    }

    public function quienSoy(){
      return Auth::user();
    }

    public function tieneImagen(){
      $file = Auth::user();
      $data = $file->imagen;

      return $data != null;
    }


  //-probablemente este solo se use en auditoria_casinos
  public function obtenerControladores($id_casino, $id_usuario){
     $users = User::role('CONTROLADOR')
                    ->join('usuario_tiene_casino',
                    'users.id','=','usuario_tiene_casino.id_usuario')
                    ->where('usuario_tiene_casino.id_casino','=',$id_casino)
                    ->whereNotIn('users.id',[$id_usuario])
                    ->get();
     $admins = User::role('ADMINISTRADOR')
                     ->join('usuario_tiene_casino',
                     'users.id','=','usuario_tiene_casino.id_usuario')
                     ->where('usuario_tiene_casino.id_casino','=',$id_casino)
                     ->whereNotIn('users.id',[$id_usuario])
                     ->get();
     $users->prepend($admins);
     return $users->unique();
  }
  public function obtenerFiscalizadores($id_casino, $id_usuario){
    $fiscalizadores = User::role('FISCALIZADOR')
                            ->join('usuario_tiene_casino',
                            'users.id','=','usuario_tiene_casino.id_usuario')
                            ->where('usuario_tiene_casino.id_casino','=',$id_casino)
                            ->whereNotIn('users.id',[$id_usuario])
                            ->get();
    return $fiscalizadores;
  }
  public function buscarUsuariosPorNombreYRelevamiento($nombre, $id_relevamiento){
    $relevamiento = Relevamiento::find($id_relevamiento);
    $id_casino = $relevamiento->sector->id_casino;

    if(empty($request->nombre) ){
    $nombre = '%';
    }
    else{
      $nombre = '%'.$request->nombre.'%';
    }

    $resultado = User::join('usuario_tiene_casino' ,
                  'usuario_tiene_casino.id_usuario' ,'=' , 'users.id')
                  ->where([['name','like',$nombre],
                  ['usuario_tiene_casino.id_casino' , '=' , $id_casino]])
                  ->get();

    return ['usuarios' => $resultado];
  }
  public function buscarUsuariosPorNombreYCasino($id_casino,$nombre){
    $nombre = (empty($nombre)) ? '%' : '%'.$nombre.'%';

    $resultado = User::join('usuario_tiene_casino',
                              'usuario_tiene_casino.id_usuario','=','users.id')
                        ->where([['name','like',$nombre],
                            ['usuario_tiene_casino.id_casino','=',$id_casino]])
                        ->get();

    return ['usuarios' => $resultado];
  }
  public function buscarCasinoDelUsuario($id_usuario){

    $casinos = array();
    $auxiliar=DB::table('casino')
                  ->join('usuario_tiene_casino' , 'casino.id_casino', '=', 'usuario_tiene_casino.id_casino')
                  ->join('users', 'users.id','=', 'usuario_tiene_casino.id_usuario')
                  ->where( 'users.id' ,'=', $id_usuario)
                  ->get()
                  ->toArray();
    $casinos=array_merge($casinos,$auxiliar);
    $casino= reset($casinos);

    return $casino->id_casino;
  }
  public function buscarCasinosDelUsuario($id_usuario){

    $casinos =DB::table('casino')
                  ->select('usuario.*','casino.id_casino','casino.nombre as nombre_casino')
                  ->join('usuario_tiene_casino' , 'casino.id_casino', '=', 'usuario_tiene_casino.id_casino')
                  ->join('users', 'users.id','=', 'usuario_tiene_casino.id_usuario')
                  ->where( 'users.id' ,'=', $id_usuario)
                  ->get();
    return $casinos;
  }
  public function buscarControladoresCasino($id_casino){
    $users = User::role('CONTROLADOR')
                   ->join('usuario_tiene_casino',
                   'users.id','=','usuario_tiene_casino.id_usuario')
                   ->where('usuario_tiene_casino.id_casino','=',$id_casino)
                   ->get();
    $admins = User::role('ADMINISTRADOR')
                    ->join('usuario_tiene_casino',
                    'users.id','=','usuario_tiene_casino.id_usuario')
                    ->where('usuario_tiene_casino.id_casino','=',$id_casino)
                    ->get();
    $users->prepend($admins);
    return $users->unique();
  }
  public function usuarioEsControlador($usuario){
    $esControlador = 0;
    foreach ($usuario->roles as $rol) {
      if($rol->id_rol == 1 || $rol->id_rol == 2 || $rol->id_rol == 4){
        $esControlador=1;
        break;
      }
    }
    return ($usuario->hasRole('CONTROLADOR') ||
            $usuario->hasRole('SUPERUSUARIO') ||
            $usuario->hasRole('ADMINISTRADOR'));

  }
  public function obtenerUsuariosRol($id_casino, $id_rol){
    $role  = Role::where('id','=',$id_rol)->getFirst();
    $rta = User::role($role->name)
                ->join('usuario_tiene_casino','usuario_tiene_casino.id_usuario','=','id')
                ->where('usuario_tiene_casino.id_casino','=',$id_casino)
                ->get();
    return $rta;
  }


}
