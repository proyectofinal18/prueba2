<?php

namespace App\Http\Controllers\Usuarios;

use Auth;
use Session;
use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Hash;

use App\User;

use App\Seccion;
use App\Casino;
use App\SecRecientes;
use App\Http\Controllers\RolesPermissions\RoleFinderController;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ConfiguracionCuentaController extends Controller
{
  private static $atributos = [
    'user_name' => 'Nombre Usuario',
    'password' => 'Contraseña',
    'usuario' => 'Nombre de Usuario' ,
    'email' => 'Email',
    'password' => 'Contraseña',
    'name' => 'Nombre y Apellido',
    'imagen' => 'Imagen',
    'dni' => 'DNI',
    'casinos' => 'Casinos',
    'roles' => 'Roles',
    'password_nuevo_confirmation' => 'Repita Nueva Contraseña',
    'password_nuevo' => 'Nueva Contraseña',
  ];

  private static $instance;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware(['auth']);
  }

  public function modificarImagen(Request $request){
    $user = Auth::user();
    $validator=Validator::make($request->all(),[
        'id_usuario' => 'required|exists:users,id',
        'imagen' => 'required'
    ], array(), self::$atributos)->after(function($validator){
      $user = Auth::user();
      if($validator->getData()['id_usuario'] != $user->id){
        $validator->errors()->add('usuario_incorrecto',
                        'El usuario enviado no coincide con el de la sesion.');
      }
      if(!empty($validator->getData()['imagen'])){
        $tipo = explode(';', $validator->getData()['imagen']);
        if($tipo[0] != "data:image/jpeg"){
          $validator->errors()->add('imagen',
                          'Formato de imagen incorrecto.');
        }
      }
    })->validate();
    if(isset($validator)){
      if ($validator->fails()){
          return ['errors' => $validator->messages()->toJson()];
          }
     }

    $usuario = User::findOrFail($request->id_usuario);

    $imagen = $request->imagen;
    if(!empty($imagen)){

      list(, $imagen) = explode(';', $imagen);

      list(, $imagen) = explode(',', $imagen);
      //dd($imagen);
        $usuario->imagen = $imagen;
    }
    $usuario->save();

    return ['imagen' => $usuario->imagen];
  }

  public function modificarPassword(Request $request){
    $user = Auth::user();
    $validator=  Validator::make($request->all(),[
        'id_usuario' => 'required|exists:users,id',
        'password_actual' => 'required|max:45',
        'password_nuevo' => 'required|max:45|confirmed',
        'password_nuevo_confirmation' => 'required|max:45|same:password_nuevo'
    ], array(), self::$atributos)->after(function($validator){

      $user = Auth::user();
      if($validator->getData()['id_usuario'] != $user->id){
        $validator->errors()->add('usuario_incorrecto',
                        'El usuario enviado no coincide con el de la sesion.');
      }
      //dd([$user->user_name,$user->password,bcrypt($validator->getData()['password_actual'])]);
      if(!(Hash::check($validator->getData()['password_actual'] ,$user->password ))){
        $validator->errors()
                  ->add('password_incorrecta',
                        'La contraseña actual no coincide con la del usuario.');
      }
    })->validate();
    if(isset($validator)){
      if ($validator->fails()){
          return ['errors' => $validator->messages()->toJson()];
          }
     }
    $usuario = User::findOrFail($request->id_usuario);
    $usuario->password = bcrypt($request->password_nuevo);
    $usuario->save();

    return $usuario;
  }

  public function modificarDatos(Request $request){
    $user = Auth::user();
      $validator=Validator::make($request->all(),[
      'id_usuario' => 'required|exists:users,id',
      'nombre' => 'required|max:45',
      'user_name' => ['required', 'max:45' , Rule::unique('users')->ignore( $request->id_usuario,'id')],
      'email' =>  ['required','email', 'max:45' , Rule::unique('users')->ignore( $request->id_usuario,'id')]
    ], array(), self::$atributos)->after(function($validator){
      $user = Auth::user();
      if($validator->getData()['id_usuario'] != $user->id){
        $validator->errors()->add('usuario_incorrecto','El usuario enviado no coincide con el de la sesion.');
      }
    })->validate();
    if(isset($validator)){
      if ($validator->fails()){
          return ['errors' => $validator->messages()->toJson()];
          }
     }

    $usuario = User::find($request->id_usuario);
    $usuario->name = $request->nombre;
    $usuario->user_name = $request->user_name;
    $usuario->email = $request->email;
    $usuario->save();

    return $usuario;
  }

  public function configUsuario(){
    $usuario = Auth::user();
    return view('Usuarios.seccionConfigCuenta')->with('usuario',$usuario);
  }

  public function restablecerPassword($id_usuario){

    $user = User::findOrFail($id_usuario);
    $ps = bcrypt($user->dni);
    $user->password = $ps;
    $user->save();
    //dd($user->dni,$ps);
    return 1;
  }

  public function leerImagenUsuario(){
    $file = Auth::user();
    $data = $file->imagen;

    return Response::make(base64_decode($data), 200, [ 'Content-Type' => 'image/jpeg',
                                                      'Content-Disposition' => 'inline; filename="lalaaa.jpeg"']);
  }

}
