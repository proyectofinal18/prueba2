<?php

namespace App\Http\Controllers\Usuarios;

use Auth;
use Session;
use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Hash;

use App\User;

use App\Seccion;
use App\Casino;
use App\SecRecientes;
use App\Http\Controllers\RolesPermissions\RoleFinderController;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class SeccionesRecientesController extends Controller
{
  private static $atributos = [
    'user_name' => 'Nombre Usuario',
    'password' => 'Contraseña',
    'usuario' => 'Nombre de Usuario' ,
    'email' => 'Email',
    'password' => 'Contraseña',
    'name' => 'Nombre y Apellido',
    'imagen' => 'Imagen',
    'dni' => 'DNI',
    'casinos' => 'Casinos',
    'roles' => 'Roles',
    'password_nuevo_confirmation' => 'Repita Nueva Contraseña',
    'password_nuevo' => 'Nueva Contraseña',
  ];

  private static $instance;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware(['auth']);
  }


    //creo que no se usa
    public function getSecRecientes(){
      $user = Auth::user();
      //si no tiene creadas las secciones_recientes
      if(!isset($user->secciones_recientes)){
        $array = array();
        return $array;
      }else{
        $sec1=null;
        $sec2=null;
        $sec3=null;
        foreach ($user->secciones_recientes as $sec) {
          switch ($sec->orden) {
            case 1:
              $sec1=$sec->seccion;
              break;
            case 2:
              $sec2=$sec->seccion;
              break;
            case 3:
              $sec3=$sec->seccion;
              break;
            default:
              # code...
              break;
          }
        }
        return ['ultima'=> $sec1, 'anteultima'=> $sec2, 'penultima' => $sec3];
      }
    }

    public function agregarSeccionReciente($seccion){
      $seccion = Seccion::where('nombre_seccion','like',$seccion)->first();
      if($seccion == null) $seccion = Seccion::find(14);//es el de config cuenta
      $user = Auth::user();

      //si no tiene creadas las secciones_recientes
      if($user->secciones_recientes->count() == 0){
        $sec1 = new SecRecientes;
        $sec1->orden = 1;
        $sec1->seccion()->associate($seccion->id_seccion);
        $sec1->usuario()->associate($user->id);
        $sec1->save();

        $sec2 = new SecRecientes;
        $sec2->orden = 2;
        $sec2->id_seccion = 14;
        $sec2->usuario()->associate($user->id);
        $sec2->save();

        $sec3 = new SecRecientes;
        $sec3->orden = 3;
        $sec3->id_seccion = 14;
        $sec3->usuario()->associate($user->id);
        $sec3->save();

        $sec4 = new SecRecientes;
        $sec4->orden = 4;
        $sec4->id_seccion = 14;
        $sec4->usuario()->associate($user->id);
        $sec4->save();


      }else{
        $secciones = $user->secciones_recientes;
        if($seccion->id_seccion != $secciones[1]->id_seccion &&
          $seccion->id_seccion != $secciones[0]->id_seccion &&
          $seccion->id_seccion != $secciones[2]->id_seccion &&
          $seccion->id_seccion != $secciones[3]->id_seccion){//evito repetidos

          $secciones[3]->seccion()->associate($secciones[2]->id_seccion);
          $secciones[3]->save();

          $secciones[2]->seccion()->associate($secciones[1]->id_seccion);
          $secciones[2]->save();

          $secciones[1]->seccion()->associate($secciones[0]->id_seccion);
          $secciones[1]->save();

          $secciones[0]->seccion()->associate($seccion->id_seccion);
          $secciones[0]->save();
        }
      }
    }

    public function initSeccionReciente(User $user){
      //si no tiene creadas las secciones_recientes
        $sec1 = new SecRecientes;
        $sec1->orden = 1;
        $sec1->usuario()->associate($user->id);
        $sec1->id_seccion = 14;
        $sec1->save();

        $sec2 = new SecRecientes;
        $sec2->orden = 2;
        $sec2->id_seccion = 14;
        $sec2->usuario()->associate($user->id);
        $sec2->save();

        $sec3 = new SecRecientes;
        $sec3->orden = 3;
        $sec3->id_seccion = 14;
        $sec3->usuario()->associate($user->id);
        $sec3->save();

        $sec4 = new SecRecientes;
        $sec4->orden = 4;
        $sec4->id_seccion = 14;
        $sec4->usuario()->associate($user->id);
        $sec4->save();
    }
}
