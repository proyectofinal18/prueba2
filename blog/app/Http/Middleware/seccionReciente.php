<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Exceptions\UnauthorizedException;
use App\Http\Controllers\Usuarios\SeccionesRecientesController;

class seccionReciente
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$seccion)
    {
        if (app('auth')->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $secRecController = new SeccionesRecientesController;

        $secRecController->agregarSeccionReciente($seccion);
        //dd($next,$seccion,$request);
        return $next($request);
    }
}


/*
public function handle($request, Closure $next, $permission)
{
    if (app('auth')->guest()) {
        throw UnauthorizedException::notLoggedIn();
    }

    $permissions = is_array($permission)
        ? $permission
        : explode('|', $permission);

    foreach ($permissions as $permission) {
        if (app('auth')->user()->can($permission)) {
            return $next($request);
        }
    }

    throw UnauthorizedException::forPermissions($permissions);
}
*/
