<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mesas\ComandoEnEspera;
use App\Http\Controllers\Importaciones\Mesas\ImportadorController;
use App\Http\Controllers\Aperturas\ABMCRelevamientosAperturaController;
use App\Http\Controllers\Apuestas\GenerarPlanillasController;
use Carbon\Carbon;


class ConsultarComandos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CAE:ejecutar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se ejecurtan los comandos en lista de espera';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $comando = ComandoEnEspera::where('fecha_a_ejecutar','>',Carbon::now()->format('Y-m-d H:i:s'))
           ->get();
       $impController = new ImportadorController;
       $relevamientoController = new ABMCRelevamientosAperturaController;
       $generarPlanillasController = new GenerarPlanillasController;
        foreach ($comando as $c) {
          switch ($c->nombre_comando) {
            case 'IDM:calcularDiff':
              $impController->calcularDiffIDM();
              break;
            case 'RAM:sortear':
              $relevamientoController->sortearMesasCommand();
              break;
            case 'RelevamientoApuestas:generar':
              $generarPlanillasController->generarRelevamientosApuestas();
              break;
            default:
              break;
          }
        }
    }
}
