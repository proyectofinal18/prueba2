<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Mesas\ApuestaMinimaJuego;
use App\Observers\Mesas\ApuestaMinimaJuegoObserver;

class ApuestaMinimaJuegoModelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        ApuestaMinimaJuego::observe(ApuestaMinimaJuegoObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
