<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Mesas\ImportacionMensualMesas;
use App\Observers\Mesas\ImportacionMensualObserver;

class ImportacionMensualModelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        ImportacionMensualMesas::observe(ImportacionMensualObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
