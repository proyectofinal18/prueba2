<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Mesas\ImagenesBunker;
use App\Observers\Mesas\BunkerObserver;

class BunkerModelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        ImagenesBunker::observe(BunkerObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
