<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Laravel\Dusk\DuskServiceProvider;
use App\Mesas\TipoMesa;
use App\Mesas\Mesa;
use App\Mesas\Moneda;
use App\Mesas\SectorMesas;
use App\Mesas\JuegoMesa;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
     public function boot()
     {
        Schema::defaultStringLength(191);
     }

    /**
     * Register any application services.
     *
     * @return void
     */
     public function register()
     {
     if ($this->app->environment('local', 'testing')) {
         $this->app->register(DuskServiceProvider::class);
     }
   }
}
