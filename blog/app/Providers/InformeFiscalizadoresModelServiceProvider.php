<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Mesas\InformeFiscalizadores;
use App\Observers\Mesas\InformeFiscalizadoresObserver;

class InformeFiscalizadoresModelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        InformeFiscalizadores::observe(InformeFiscalizadoresObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
