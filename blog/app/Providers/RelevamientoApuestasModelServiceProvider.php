<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Mesas\RelevamientoApuestas;
use App\Observers\Mesas\RelevamientoApuestasObserver;

class RelevamientoApuestasModelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        RelevamientoApuestas::observe(RelevamientoApuestasObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
