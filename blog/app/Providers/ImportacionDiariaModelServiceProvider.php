<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Mesas\ImportacionDiariaMesas;
use App\Observers\Mesas\ImportacionDiariaObserver;

class ImportacionDiariaModelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        ImportacionDiariaMesas::observe(ImportacionDiariaObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
