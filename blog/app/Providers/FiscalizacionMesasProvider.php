<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Mesas\Cierre;
use App\Mesas\Apertura;
use App\Observers\Mesas\CierreObserver;
use App\Observers\Mesas\AperturaObserver;

class FiscalizacionMesasServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Cierre::observe(CierreObserver::class);
        //Apertura::observe(AperturaObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
