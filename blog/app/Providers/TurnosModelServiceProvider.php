<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Turno;
use App\Observers\Mesas\TurnosObserver;

class TurnosModelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Turno::observe(TurnosObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
