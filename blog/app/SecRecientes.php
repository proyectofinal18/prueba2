<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecRecientes extends Model
{
  protected $connection = 'mysql';
  protected $table = 'sec_recientes';
  protected $primaryKey = 'id_sec_reciente';
  protected $visible = array('id_sec_reciente','orden','id_seccion', 'id_usuario','url_seccion','nombre_seccion','url_imagen');
  public $timestamps = false;
  protected $appends = array('url_seccion','nombre_seccion','url_imagen');

  public function getUrlSeccionAttribute(){
    if($this->id_seccion != null){
      return $this->seccion->url_seccion;
    }else{
      return 'home';
    }
  }

  public function getUrlImagenAttribute(){
    if($this->id_seccion != null){
      return $this->seccion->url_imagen;
    }else{
      return 'logos/tarjetas/casino_degrade';
    }
  }

  public function getNombreSeccionAttribute(){
    if($this->id_seccion != null){
      return $this->seccion->nombre_seccion;
    }else{
      return 'INICIO';
    }
  }

  public function usuario(){
    return $this->belongsTo('App\User','id_usuario','id');
  }

  public function seccion(){
    return $this->belongsTo('App\Seccion','id_seccion','id_seccion');
  }

  public function getTableName(){
    return $this->table;
  }

  public function getId(){
    return [$this->id_usuario, $this->orden];
  }

}
