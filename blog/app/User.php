<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Observers\UsuarioObserver;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_name','dni',
    ];

    protected $visible = array('id','user_name','name','email', 'dni' ,'ultimos_visitados');

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','imagen',
    ];

    protected $dates = ['deleted_at'];

    protected $appends = array('es_superusuario');

    protected $guard_name = 'web';

    public function getEsSuperusuarioAttribute(){
      return $this->hasRole('SUPERUSUARIO');
    }

    public function relevamientos_apuestas(){
      return $this->belongsToMany('App\Mesas\RelevamientoApuestas','fiscalizador_relevo_apuesta','id_usuario','id_relevamiento_apuestas_mesas');
    }

    public function casinos(){
      return $this->belongsToMany('App\Casino','usuario_tiene_casino','id_usuario','id_casino');
    }
    public function logs(){
      return $this->hasMany('App\Log','id_usuario','id');
    }

    public function secciones_recientes(){
      return $this->hasMany('App\SecRecientes','id_usuario','id')->orderBy('orden','asc');
    }

    public function cierres_cargados(){
      return $this->hasMany('App\Mesas\Cierre','id_fiscalizador','id');
    }
    public function aperturas_cargadas(){
      return $this->hasMany('App\Mesas\Apertura','id_cargador','id');
    }

    public static function boot(){
          parent::boot();
          User::observe(new UsuarioObserver());
    }

    // public function agregarRuta($string){
    //   $visitados = $this->ultimos_visitados;
    //   $vistas = explode(";" , $visitados);
    //   $this->ultimos_visitados = $string . ";" . $vistas[0] . ";" . $vistas[1] . ";" . $vistas[2] ;
    // }

    //si el usuario forma parte del casino $id_casino devuelve verdadero
    public function usuarioTieneCasino($id_casino){
      $bandera=0;
      foreach($this->casinos as $casino){
          if($casino->id_casino == $id_casino){
              return 1;
          }
      }
      return $bandera;
    }

    //notificaciones
    public function routeNotificationForMail()
    {
        return $this->email;
    }

    public function lastNotifications()
    {
      return $this->notifications()
                      ->orderBy('id', 'desc')->take(10)->get();
    }

    public function getTableName(){
      return 'users';
    }

    public function getId(){
      return $this->id;
    }

}
