<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seccion extends Model
{
  protected $connection = 'mysql';
  protected $table = 'secciones';
  protected $primaryKey = 'id_seccion';
  protected $visible = array('id_seccion','nombre_seccion',
                              'url_imagen','url_seccion');
  public $timestamps = false;

  public function getTableName(){
    return $this->table;
  }

  public function getId(){
    return $this->id_seccion;
  }
}
