<?php

namespace App\Observers\Mesas;
use App\Mesas\ImportacionDiariaMesas;
use App\Observers\EntityObserver;

class ImportacionDiariaObserver extends EntityObserver
{

   public function getDetalles($entidad){
     $detalles = array(//para cada modelo poner los atributos más importantes
       array('fecha', $entidad->fecha),
       array('total_diario', $entidad->total_diario),
       array('utilidad_diaria_calculada', $entidad->utilidad_diaria_calculada),
       array('utilidad_diaria_total', $entidad->utilidad_diaria_total),
       array('cotizacion_dolar', $entidad->cotizacion),
       array('id_casino', $entidad->id_casino),
       array('id_moneda', $entidad->id_moneda),


     );
     return $detalles;
   }
}
