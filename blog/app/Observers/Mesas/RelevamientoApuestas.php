<?php

namespace App\Observers\Mesas;
use App\Mesas\RelevamientoApuestas;
use App\Observers\EntityObserver;

class RelevamientoApuestasObserver extends EntityObserver
{

   public function getDetalles($entidad){
     $detalles = array(//para cada modelo poner los atributos más importantes
       array('fecha', $entidad->fecha),
       array('id_relevamiento_apuestas', $entidad->id_relevamiento_apuestas),
       array('hora_ejecucion', $entidad->hora_ejecucion),
       array('id_turno', $entidad->id_turno),
       array('id_casino', $entidad->id_casino)
     );
     return $detalles;
   }
}
