<?php

namespace App\Observers\Mesas;
use App\Mesas\InformeFiscalizadores;
use App\Observers\EntityObserver;

class InformeFiscalizadoresObserver extends EntityObserver
{

   public function getDetalles($entidad){
     $detalles = array(//para cada modelo poner los atributos más importantes
       array('fecha', $entidad->fecha),
       array('id_casino', $entidad->id_casino),
     );
     return $detalles;
   }
}
