<?php

namespace App\Observers\Mesas;
use App\Mesas\ImportacionMensualMesas;
use App\Observers\EntityObserver;

class ImportacionMensualObserver extends EntityObserver
{

   public function getDetalles($entidad){
     $detalles = array(//para cada modelo poner los atributos más importantes
       array('mes', $entidad->mes),
       array('total_utilidad_mes', $entidad->total_utilidad_mensual),
       array('cotizacion_euro', $entidad->cotizacion_euro),
       array('cotizacion_dolar', $entidad->cotizacion_dolar),
       array('id_casino', $entidad->id_casino),
       array('id_moneda', $entidad->id_moneda)

     );
     return $detalles;
   }
}
