<?php

namespace App\Observers\Mesas;
use App\Mesas\ImagenesBunker;
use App\Observers\EntityObserver;

class BunkerObserver extends EntityObserver
{

   public function getDetalles($entidad){
     $detalles = array(//para cada modelo poner los atributos más importantes
       array('fechas', $entidad->fechas),
       array('id_casino', $entidad->id_casino),
       array('mes_anio', $entidad->mes_anio),
     );
     return $detalles;
   }
}
