<?php

namespace App\Observers\Mesas;
use App\Turno;
use App\Observers\EntityObserver;

class TurnosObserver extends EntityObserver
{

   public function getDetalles($entidad){
     $detalles = array(//para cada modelo poner los atributos más importantes
       array('id_casino', $entidad->id_casino),
       array('nro_turno', $entidad->nro_turno),
     );

     return $detalles;
   }

}
