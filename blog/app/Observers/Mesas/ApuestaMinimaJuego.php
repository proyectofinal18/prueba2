<?php

namespace App\Observers\Mesas;
use App\Mesas\ApuestaMinimaJuego;
use App\Observers\EntityObserver;

class ApuestaMinimaJuegoObserver extends EntityObserver
{

   public function getDetalles($entidad){
     $detalles = array(//para cada modelo poner los atributos más importantes
       array('id_apuesta_minima', $entidad->id_apuesta_minima),
       array('id_casino', $entidad->id_casino),
       array('cantidad_requerida', $entidad->cantidad_requerida),
       array('apuesta_minima', $entidad->apuesta_minima),
       array('id_juego_mesa', $entidad->id_juego_mesa),
     );
     return $detalles;
   }
}
