<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Observers\CasinoObserver;
use Illuminate\Database\Eloquent\SoftDeletes;

class Casino extends Model
{
  use SoftDeletes;
  protected $connection = 'mysql';
  protected $table = 'casino';
  protected $primaryKey = 'id_casino';
  protected $visible = array('id_casino','nombre','codigo','fecha_inicio','porcentaje_sorteo_mesas');
  public $timestamps = false;


  public function usuarios(){
    return $this->hasMany('App\User','usuario_tiene_casino','id_casino','id_usuario');
  }

  public function turnos(){
    return $this->hasMany('App\Turno','id_casino','id_casino');
  }

  public function mesas(){
    return $this->hasMany('App\Mesas\Mesa','id_casino','id_casino');
  }

  public function meses(){
    return $this->hasMany('App\MesCasino','id_casino','id_casino');
  }

  public function detalles_informe_final_mesas(){
    return $this->hasMany('App\Mesas\DetalleInformeFinalMesas','id_casino','id_casino');
  }

  public function fichas(){
    return $this->hasMany('App\Mesas\FichaTieneCasino','id_casino','id_casino');
  }

  public static function boot(){
        parent::boot();
        Casino::observe(new CasinoObserver());
  }

  public function getTableName(){
    return $this->table;
  }

  public function getId(){
    return $this->id_casino;
  }

}
