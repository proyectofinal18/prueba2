ALTER TABLE `comando_a_ejecutar` CHANGE `nombre_comando` `nombre_comando` VARCHAR(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
--  nuevo permiso de roles y permisos. asignado a superusuario y controlador
INSERT INTO `permissions` (`id`, `id_seccion`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES (NULL, '13', 'm_abmc_roles_permisos', 'web', NULL, NULL);
INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES ('104', '1'), ('104', '4');
ALTER TABLE `detalle_informe_final_mesas` DROP `mes_detalle` ;
ALTER TABLE `detalle_informe_final_mesas` DROP `variacion` ;


ALTER TABLE `log` ADD `id_casino` INT NOT NULL AFTER `id_entidad`, ADD INDEX `casino_log_act_idx` (`id_casino`);
ALTER TABLE `cierre_mesa` ADD CONSTRAINT `fk_state_cierre` FOREIGN KEY (`id_estado_cierre`) REFERENCES `estado_cierre`(`id_estado_cierre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

INSERT INTO `estado_cierre` (`id_estado_cierre`, `descripcion`) VALUES ('4', 'VALIDADO CON APERTURA'); --jueves a la tarde


ALTER TABLE `importacion_diaria_mesas` ADD `nombre_csv` VARCHAR(100) NULL DEFAULT NULL AFTER `id_importacion_diaria_mesas`;
ALTER TABLE `importacion_mensual_mesas` ADD `nombre_csv` VARCHAR(100) NULL AFTER `id_importacion_mensual_mesas`;
ALTER TABLE `informe_fiscalizadores` ADD `turnos_sin_minimo` VARCHAR(15) NOT NULL DEFAULT '-' AFTER `id_apuesta_minima_juego`, ADD `mesas_relevadas_abiertas` INT NOT NULL DEFAULT '0' AFTER `turnos_sin_minimo`, ADD `mesas_importadas_abiertas` INT NOT NULL DEFAULT '0' AFTER `mesas_relevadas_abiertas`, ADD `mesas_con_diferencias` INT NOT NULL DEFAULT '100' AFTER `mesas_importadas_abiertas`, ADD `porcent_ap_sin_validar` INT NOT NULL DEFAULT '100' AFTER `mesas_con_diferencias`;
ALTER TABLE `informe_fiscalizadores` ADD `porcent_cie_sin_validar` INT NOT NULL DEFAULT '0' AFTER `porcent_ap_sin_validar`;
ALTER TABLE `informe_fiscalizadores` CHANGE `mesas_relevadas_abiertas` `mesas_relevadas_abiertas` MEDIUMTEXT NOT NULL;
ALTER TABLE `informe_fiscalizadores` CHANGE `mesas_importadas_abiertas` `mesas_importadas_abiertas` MEDIUMTEXT NOT NULL;
ALTER TABLE `informe_fiscalizadores` CHANGE `porcent_ap_sin_validar` `ap_sin_validar` INT(11) NOT NULL DEFAULT '100', CHANGE `porcent_cie_sin_validar` `cie_sin_validar` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `informe_fiscalizadores` CHANGE `mesas_con_diferencias` `mesas_con_diferencias` MEDIUMTEXT NOT NULL;

ALTER TABLE `importacion_mensual_mesas` ADD `saldo_fichas_mes` DOUBLE(15,2) NULL AFTER `total_utilidad_mensual`;
ALTER TABLE `importacion_mensual_mesas` CHANGE `diferencias` `diferencias` DOUBLE(15,2) NOT NULL DEFAULT '0';

ALTER TABLE `detalle_importacion_mensual_mesas` CHANGE `utilidad_calculada` `utilidad_calculada_dia` DOUBLE(15,2) NULL DEFAULT NULL;
ALTER TABLE `detalle_importacion_mensual_mesas` ADD `created_at` TIMESTAMP NULL AFTER `reposiciones_dia`, ADD `updated_at` TIMESTAMP NULL AFTER `created_at`, ADD `deleted_at` TIMESTAMP NULL AFTER `updated_at`;
ALTER TABLE `cierre_mesa` ADD `id_estado_cierre` INT NOT NULL DEFAULT '1' AFTER `id_cierre_mesa`, ADD INDEX `estado_cierre_cierremesa_idx` (`id_estado_cierre`);

ALTER TABLE `cierre_mesa` ADD `observacion` VARCHAR(200) NULL AFTER `id_moneda`;

ALTER TABLE `detalle_importacion_mensual_mesas` DROP `droop`, DROP `hold`;


CREATE TABLE IF NOT EXISTS `campo_modificado_mesas` (
  `id_campo_modificado` int(11) NOT NULL AUTO_INCREMENT,
  `id_importacion_diaria_mesas` int(11) NOT NULL,
  `id_entidad` int(11) NOT NULL,
  `nombre_entidad` varchar(30) NOT NULL,
  `nombre_del_campo` varchar(30) NOT NULL,
  `valor_anterior` varchar(50) NOT NULL,
  `id_entidad_extra` int(11) DEFAULT NULL,
  `nombre_entidad_extra` varchar(30) DEFAULT NULL,
  `accion` varchar(15) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_campo_modificado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `permissions` CHANGE `seccion` `id_seccion` INT(11) NULL DEFAULT NULL;
ALTER TABLE `secciones` CHANGE `url_seccion` `url_seccion` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
--
-- CREATE TABLE `ficha_tiene_casino` ( `id_ficha_tiene_casino` INT NOT NULL AUTO_INCREMENT ,
-- `id_ficha` INT NOT NULL ,
-- `id_casino` INT NOT NULL ,
-- `created_at` TIMESTAMP NULL ,
-- `updated_at` TIMESTAMP NULL ,
-- `deleted_at` TIMESTAMP NULL ,
-- PRIMARY KEY (`id_ficha_tiene_casino`),
-- INDEX `idx_ficha_tiene_casino` (`id_ficha`),
-- INDEX `idx_casino_con_ficha_tiene_casino` (`id_casino`)) ENGINE = InnoDB;
--
-- -- ALTER TABLE `secciones` CHANGE `id_seccion` `id_seccion` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `nombre_seccion` `nombre_seccion` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `url_seccion` `url_seccion` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
-- CREATE TABLE `secciones`
-- ( `id_seccion` INT NOT NULL AUTO_INCREMENT ,
--   `nombre_seccion` INT NOT NULL ,
--   `url_seccion` INT NOT NULL ,
--   `url_imagen` INT NOT NULL ,
--   PRIMARY KEY (`id_seccion`)
-- ) ENGINE = InnoDB;
--
-- ALTER TABLE `secciones` CHANGE `id_seccion` `id_seccion` INT(11) NOT NULL AUTO_INCREMENT,
--  CHANGE `nombre_seccion` `nombre_seccion` VARCHAR(20) NOT NULL,
--   CHANGE `url_seccion` `url_seccion` INT(20) NOT NULL,
--    CHANGE `url_imagen` `url_imagen` INT(100) NOT NULL;
-- ALTER TABLE `secciones` CHANGE `url_seccion` `url_seccion` VARCHAR(20) NOT NULL, CHANGE `url_imagen` `url_imagen` VARCHAR(100) NOT NULL;
-- ALTER TABLE `sec_recientes` ADD `id_seccion` INT NOT NULL AFTER `id_usuario`,
--  ADD INDEX `idx_seccion_sec_recientes` (`id_seccion`);
--
--  ALTER TABLE `sec_recientes`
--   DROP `seccion`,
--   DROP `ruta`;
--
--
--   INSERT INTO `secciones`(`id_seccion`, `nombre_seccion`, `url_seccion`, `url_imagen`) VALUES
--   (1,"Cierres y Aperturas","aperturas","img//tarjetas//cierresyaperturas"),
--   (2,"Apuestas Mínimas","apuestas","img//tarjetas//apuestas"),
--   (3,"Imágenes Bunker","apuestas","img//tarjetas//apuestas"),
--   (4,"Canon","canon","img//tarjetas//apuestas"),
--   (5,"Casinos","casinos","img//tarjetas//"),
--   (6,"Importaciones Diarias","importacionDiaria","img//tarjetas//"),
--   (7,"Importaciones Mensuales","importacionMensual","img//tarjetas//"),
--   (8,"Informes Diarios Fiscalizaciones","informeDiarioBasico","img//tarjetas//"),
--   (9,"Informes Diarios","informeDiario","img//tarjetas//"),
--   (10,"Informes Mensuales","informeMensual","img//tarjetas//"),
--   (11,"Juegos y Sectores","juegos","img//tarjetas//"),
--   (12,"Mesas","mesas-de-panio","img//tarjetas//"),
--   (13,"Roles y Permisos","roles","img//tarjetas//"),
--   (14,"Configuración de Cuenta","configCuenta","img//tarjetas//"),
--   (15,"Gestionar Usuarios","usuarios","img//tarjetas//");
--
--
-- UPDATE `secciones` SET `url_imagen` = 'img/pngs/fichas.png'
-- WHERE `secciones`.`id_seccion` = 1; UPDATE `secciones`
-- SET `url_imagen` = 'img/pngs/ficha.png' WHERE `secciones`.`id_seccion` = 2;
-- UPDATE `secciones` SET `url_seccion` = 'solicitudImagenes',
-- `url_imagen` = 'img/pngs/camara_lines.png' WHERE `secciones`.`id_seccion` = 3;
-- UPDATE `secciones` SET `url_imagen` = 'img/pngs/bolsa_lines.png'
-- WHERE `secciones`.`id_seccion` = 4; UPDATE `secciones`
-- SET `url_imagen` = 'img/pngs/casino.png' WHERE `secciones`.`id_seccion` = 5;
-- UPDATE `secciones` SET `url_imagen` = 'img/pngs/csv_lines.png'
-- WHERE `secciones`.`id_seccion` = 6; UPDATE `secciones`
-- SET `url_imagen` = 'img/pngs/csv_lines.png' WHERE `secciones`.`id_seccion` = 7;
-- UPDATE `secciones` SET `url_imagen` = 'img/pngs/informe_lines.png'
-- WHERE `secciones`.`id_seccion` = 8; UPDATE `secciones`
-- SET `url_imagen` = 'img/pngs/informe_lines.png' WHERE `secciones`.`id_seccion` = 9;
-- UPDATE `secciones` SET `url_imagen` = 'img/pngs/informe_lines.png'
-- WHERE `secciones`.`id_seccion` = 10; UPDATE `secciones`
-- SET `url_imagen` = 'img/pngs/ruleta.png' WHERE `secciones`.`id_seccion` = 11;
-- UPDATE `secciones` SET `url_imagen` = 'img/pngs/mesa_line.png'
-- WHERE `secciones`.`id_seccion` = 12; UPDATE `secciones`
-- SET `url_imagen` = 'img/pngs/configuracion.png' WHERE `secciones`.`id_seccion` = 13;
-- UPDATE `secciones` SET `url_imagen` = 'img/pngs/usuario_configuracion.png'
-- WHERE `secciones`.`id_seccion` = 14; UPDATE `secciones`
-- SET `url_imagen` = 'img/pngs/usuario.png' WHERE `secciones`.`id_seccion` = 15;
