<?php

use Faker\Generator as Faker;
use Faker\Provider\es_PE\Person as Pepe;
use App\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
  $faker2 = new Faker();
  $faker2->addProvider(new Pepe($faker2));
    return [
        'name' => $faker->name,
        'user_name' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'dni' =>$faker2->unique()->dni,
        'remember_token' => str_random(10),
    ];
});

// $factory->defineAs(User::class, 'SUPERUSUARIO', function ($faker) use ($factory) {
//     $user = $factory->raw(User::class);
//     //$user->assignRole('SUPERUSUARIO');
//     return array_merge($user);
// });
//
// $factory->defineAs(User::class, 'ADMINISTRADOR', function ($faker) use ($factory) {
//     $user = $factory->raw(User::class);
//     $user->assignRole('ADMINISTRADOR');
//     return array_merge($user, ['ADMINISTRADOR' => true]);
// });
//
// $factory->defineAs(User::class, 'CONTROLADOR', function ($faker) use ($factory) {
//     $user = $factory->raw(User::class);
//     $user->assignRole('CONTROLADOR');
//     return array_merge($user, ['CONTROLADOR' => true]);
// });
//
// $factory->defineAs(User::class, 'AUDITOR', function ($faker) use ($factory) {
//     $user = $factory->raw(User::class);
//     $user->assignRole('AUDITOR');
//     return array_merge($user, ['AUDITOR' => true]);
// });


///USAGE
//$user = factory(App\User::class, 'admin')->create();
