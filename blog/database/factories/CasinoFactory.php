<?php

use Faker\Generator as Faker;
//use faker.Faker();
use App\Casino;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Casino::class, function (Faker $faker) {

  $codigo=array("BNH","IUD", "POL", "CAN");
    $nro=rand(1,10);
    return [
        'nombre' => $faker->unique()->name,
        'codigo' => array_rand($codigo,1),
        'porcentaje_sorteo_mesas' =>$nro,
        'fecha_inicio'=>$faker.date_time_between(  "2016-01-01" ,"now",''),

    ];
});
