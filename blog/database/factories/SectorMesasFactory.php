<?php

use Faker\Generator as Faker;
use App\SectorMesas;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(SectorMesas::class, function (Faker $faker) {


    return [
        'descripcion' => $faker->text,
        'id_casino' => $this->random('CasinoFactory')->id

    ];
});
