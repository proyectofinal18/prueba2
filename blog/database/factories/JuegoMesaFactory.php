<?php

use Faker\Generator as Faker;
use App\Mesas\JuegoMesa;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(JuegoMesa::class, function (Faker $faker) {

  //$siglas=rand(ord(A),ord(Z),3);
  $pos=rand(1,18);
  $tipo=array("1","2", "3");
  $siglas=array("BNH","IUD", "POL", "CAN","POI","ERT","RAE");


    return [
        'nombre_juego'=>$faker->firstname,
        'siglas'=>array_rand($siglas,1),
        'posiciones' => $pos,
        'id_tipo_mesa' =>rand(1,3),
        'id_casino' => 1,
    ];
});
