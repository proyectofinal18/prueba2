<?php

use Faker\Generator as Faker;
use App\Mesas\Mesa;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Mesa::class, function (Faker $faker) {

  $id_moneda=rand(1,2);
  $nro=rand(1,200);
    return [
        'nro_mesa' => $nro,
        'nombre' => $faker->unique()->name,
        'descripcion' => $faker->userName,
        'id_juego_mesa' =>6,
        'id_casino' => 1,
        'id_moneda' => $id_moneda,
        'id_sector_mesas' => 10,
    ];
});
