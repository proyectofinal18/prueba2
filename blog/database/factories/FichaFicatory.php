<?php

use Faker\Generator as Faker;
use App\Ficha;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Ficha::class, function (Faker $faker) {

  $valor=array("50.00","100.00", "1.00", "1.25","5.00","10.00","1000.00","5000.00","10000.00","20000.00");
  $id_moneda=rand(1,2);
    return [
        'valor_ficha' => array_rand($valor,1),
        'id_moneda' => $id_moneda

    ];
});
