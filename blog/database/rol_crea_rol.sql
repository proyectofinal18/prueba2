
CREATE TABLE IF NOT EXISTS `rol_crea_rol` (
  `id_rol_crea_rol` int(11) NOT NULL AUTO_INCREMENT,
  `id_rol_creador` int(11) NOT NULL,
  `id_rol_a_crear` int(11) NOT NULL,
  `id_casino` int(11) NOT NULL,
  PRIMARY KEY (`id_rol_crea_rol`),
  KEY `id_rol_creador` (`id_rol_creador`,`id_rol_a_crear`,`id_casino`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `rol_crea_rol`
--

INSERT INTO `rol_crea_rol` (`id_rol_crea_rol`, `id_rol_creador`, `id_rol_a_crear`, `id_casino`) VALUES
(5, 2, 3, 1),
(2, 2, 3, 2),
(6, 2, 3, 3),
(1, 2, 4, 1),
(7, 2, 4, 2),
(8, 2, 4, 3),
(3, 4, 3, 1),
(9, 4, 3, 2),
(10, 4, 3, 3);
