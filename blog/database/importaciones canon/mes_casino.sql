-- phpMyAdmin SQL Dump
-- version 4.0.10deb1ubuntu0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 04, 2019 at 02:21 AM
-- Server version: 5.5.62-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bdmmb`
--

--
-- Dumping data for table `mes_casino`
--

INSERT INTO `mes_casino` (`id_mes_casino`, `nombre_mes`, `nro_mes`, `nro_cuota`, `dia_inicio`, `dia_fin`, `id_casino`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Septiembre 01 al 27', 9, 13, 1, 27, 1, NULL, NULL, NULL),
(2, 'Septiembre28 al 30', 9, 1, 28, 30, 1, NULL, NULL, NULL),
(3, 'Octubre', 10, 2, 1, 31, 1, NULL, NULL, NULL),
(4, 'Noviembre', 11, 3, 1, 30, 1, NULL, NULL, NULL),
(5, 'Diciembre', 12, 4, 1, 31, 1, NULL, NULL, NULL),
(6, 'Enero', 1, 5, 1, 31, 1, NULL, NULL, NULL),
(7, 'Febrero', 2, 6, 1, 28, 1, NULL, NULL, NULL),
(8, 'Marzo', 3, 7, 1, 31, 1, NULL, NULL, NULL),
(9, 'Abril', 4, 8, 1, 30, 1, NULL, NULL, NULL),
(10, 'Mayo', 5, 9, 1, 31, 1, NULL, NULL, NULL),
(11, 'Junio', 6, 10, 1, 30, 1, NULL, NULL, NULL),
(12, 'Julio', 7, 11, 1, 31, 1, NULL, NULL, NULL),
(13, 'Agosto', 8, 12, 1, 31, 1, NULL, NULL, NULL),
(14, 'Agosto 01 al 10', 8, 13, 1, 10, 2, NULL, NULL, NULL),
(15, 'Agosto11 al 31', 8, 1, 11, 31, 2, NULL, NULL, NULL),
(16, 'Septiembre', 9, 2, 1, 30, 2, NULL, NULL, NULL),
(17, 'Octubre', 10, 3, 1, 31, 2, NULL, NULL, NULL),
(18, 'Noviembre', 11, 4, 1, 30, 2, NULL, NULL, NULL),
(19, 'Diciembre', 12, 5, 1, 31, 2, NULL, NULL, NULL),
(20, 'Enero', 1, 6, 1, 31, 2, NULL, NULL, NULL),
(21, 'Febrero', 2, 7, 1, 28, 2, NULL, NULL, NULL),
(22, 'Marzo', 3, 8, 1, 31, 2, NULL, NULL, NULL),
(23, 'Abril', 4, 9, 1, 30, 2, NULL, NULL, NULL),
(24, 'Mayo', 5, 10, 1, 31, 2, NULL, NULL, NULL),
(25, 'Junio', 6, 11, 1, 30, 2, NULL, NULL, NULL),
(26, 'Julio', 7, 12, 1, 31, 2, NULL, NULL, NULL),
(27, 'Octubre 01 al 13', 10, 13, 1, 13, 3, NULL, NULL, NULL),
(28, 'Octubre14 al 31', 10, 1, 14, 31, 3, NULL, NULL, NULL),
(29, 'Noviembre', 11, 2, 1, 30, 3, NULL, NULL, NULL),
(30, 'Diciembre', 12, 3, 1, 31, 3, NULL, NULL, NULL),
(31, 'Enero', 1, 4, 1, 31, 3, NULL, NULL, NULL),
(32, 'Febrero', 2, 5, 1, 28, 3, NULL, NULL, NULL),
(33, 'Marzo', 3, 6, 1, 31, 3, NULL, NULL, NULL),
(34, 'Abril', 4, 7, 1, 30, 3, NULL, NULL, NULL),
(35, 'Mayo', 5, 8, 1, 31, 3, NULL, NULL, NULL),
(36, 'Junio', 6, 9, 1, 30, 3, NULL, NULL, NULL),
(37, 'Julio', 7, 10, 1, 31, 3, NULL, NULL, NULL),
(38, 'Agosto', 8, 11, 1, 31, 3, NULL, NULL, NULL),
(39, 'Septiembre', 9, 12, 1, 30, 3, NULL, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
