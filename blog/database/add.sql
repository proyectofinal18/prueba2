INSERT INTO `permissions`( `name`, `guard_name`, `seccion` )VALUES
('m_abm_aperturas' , 'web','aperturas'),
('m_abmc_relevamientos_aperturas', 'web','aperturas'),
('m_bc_aperturas' , 'web','aperturas'),
('m_v_aperturas' , 'web','aperturas'),
('m_abm_apuestas' , 'web','apuestas'),
('m_abmc_apuesta_minima', 'web','apuestas'),
('m_bc_apuestas', 'web','apuestas'),
('m_bv_apuestas', 'web','apuestas'),
('m_relevamiento_apuestas', 'web','apuestas'),
('m_abmc_img_bunker', 'web','solicitud_imagenes'),
('m_abmc_canon', 'web', 'canon'),
('m_actualizar_canon', 'web', 'canon'),
('m_a_pagos', 'web', 'canon'),
('abmc_casinos', 'web', 'casinos'),
('m_abm_cierres', 'web','cierres'),
('m_bc_cierres', 'web','cierres'),
('m_v_cierres', 'web','cierres'),
('m_importar', 'web', 'importacion'),
('m_abm_informes_fiscalizadores', 'web', 'informes_fiscalizadores'),
('m_bc_informes_fiscalizadores', 'web', 'informes_fiscalizadores'),
('m_generar_informes_fiscalizadores', 'web', 'informes_fiscalizadores'),
('m_bc_informes_fiscalizadores', 'web', 'informes_fiscalizadores'),
('m_generar_informes_fiscalizadores', 'web', 'informes_fiscalizadores'),
('m_bc_anuales', 'web','informe_anual'),
('m_bc_diario_mensual', 'web','informes'),
('m_abm_juegos', 'web','juegos'),
('m_b_juegos', 'web','juegos'),
('m_abm_mesas', 'web','mesas'),
('m_b_mesas', 'web','mesas'),
('m_abmc_sectores', 'web','sectores'),
('abmc_turnos', 'web','turnos');

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `seccion`, `created_at`, `updated_at`) VALUES (NULL, 'abmc_usuarios', 'web', 'Configuración', NULL, NULL);
(abmc_usuarios)

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `id_seccion`, `created_at`, `updated_at`) VALUES
(102, 'ver_seccion_imagenes_bunker', 'web', 3, NULL, NULL),
(101, 'ver_seccion_canon', 'web', 4, NULL, NULL),
(100, 'ver_seccion_informes_contables', 'web', 9, NULL, NULL),
(99, 'ver_seccion_informes_fiscalizadores', 'web', 8, NULL, NULL),
(98, 'ver_seccion_apuestas', 'web', 2, NULL, NULL),
(97, 'ver_seccion_cierres_aperturas', 'web', 1, NULL, NULL),
(96, 'ver_seccion_mesas', 'web', 12, NULL, NULL);

INSERT INTO `model_has_permissions`(`permission_id`, `model_id`, `model_type`) VALUES
(96,1,'App\User'),
(97,1,'App\User'),
(98,1,'App\User'),
(99,1,'App\User'),
(100,1,'App\User'),
(101,1,'App\User'),
(102,1,'App\User');
