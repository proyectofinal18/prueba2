ALTER TABLE `informe_fiscalizadores` ADD `aperturas_sorteadas` DOUBLE( 8, 2 ) NOT NULL AFTER `cie_sin_validar` ;

ALTER TABLE `detalle_informe_final_mesas` CHANGE `impuestos` `impuestos` DOUBLE(15,2) NULL DEFAULT '0.0';

ALTER TABLE `campo_modificado_mesas` ADD `valor_nuevo` VARCHAR( 50 ) NOT NULL AFTER `valor_anterior` ;
ALTER TABLE `detalle_informe_final_mesas` CHANGE `cotizacion_euro_anterior` `cotizacion_euro_anterior` DOUBLE( 8, 4 ) NULL DEFAULT NULL ,
CHANGE `cotizacion_dolar_actual` `cotizacion_dolar_actual` DOUBLE( 8, 4 ) NULL DEFAULT NULL ,
CHANGE `cotizacion_dolar_anterior` `cotizacion_dolar_anterior` DOUBLE( 8, 4 ) NULL DEFAULT NULL ,
CHANGE `cotizacion_euro_actual` `cotizacion_euro_actual` DOUBLE( 8, 4 ) NULL DEFAULT NULL ;

-- CREATE TABLE `informe_fiscalizacion_tiene_valor_minimo` (
-- `id_informe_fiscalizacion_tiene_valor_minimo` INT NOT NULL AUTO_INCREMENT ,
-- `id_apuesta_minima_juego` INT NOT NULL ,
-- `id_informe_fiscalizadores` INT NOT NULL ,
-- `cantidad_cumplieron` INT NOT NULL DEFAULT '0' ,
-- `created_at` TIMESTAMP NULL ,
-- `deleted_at` TIMESTAMP NULL ,
-- `updated_at` TIMESTAMP NULL ,
-- PRIMARY KEY (`id_informe_fiscalizacion_tiene_valor_minimo`),
-- INDEX `idx_minimo_apuestas_tiene_fiscc` (`id_apuesta_minima_juego`),
-- INDEX `idx_informe_tiene_valor_ap_minima` (`id_informe_fiscalizadores`))
-- ENGINE = InnoDB;
--
--
-- ALTER TABLE `detalle_relevamiento_apuestas` ADD `multimoneda` TINYINT NOT NULL AFTER `nombre_juego` ,
-- ADD `id_moneda` INT NULL AFTER `multimoneda` ,
-- ADD INDEX ( `id_moneda` ) ;
-- UPDATE `detalle_relevamiento_apuestas` SET `id_moneda` =1 WHERE `id_moneda` IS NULL
--
--
-- ALTER TABLE `informe_fiscalizadores` CHANGE `mesas_con_diferencias` `mesas_con_diferencia` MEDIUMTEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;
-- ALTER TABLE `informe_fiscalizadores` CHANGE `turnos_sin_minimo` `turnos_sin_minimo` VARCHAR( 200 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '-';
