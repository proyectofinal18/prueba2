12/02/2019
ALTER TABLE `detalle_importacion_mensual_mesas` ADD `saldo_fichas` DOUBLE(15,2) NULL AFTER `utilidad_calculada`;
ALTER TABLE `detalle_importacion_diaria_mesas` ADD `saldo_fichas` DOUBLE(15,2) NULL AFTER `diferencia_cierre`;
-------

ALTER TABLE `cierre_apertura` ADD `fecha_produccion` DATE NULL
AFTER `id_controlador`, ADD `diferencias` TINYINT NULL AFTER `fecha_produccion`;


ALTER TABLE `cierre_apertura` ADD `id_casino` INT NOT NULL AFTER `id_cierre_apertura`, ADD INDEX `casino_cierre_ap_conjunto_idx` (`id_casino`);
ALTER TABLE `cierre_apertura` ADD  CONSTRAINT `fk_casino_cierre_ap_conjunto` FOREIGN KEY (`id_casino`) REFERENCES `casino`(`id_casino`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `informe_fiscalizadores` ADD `pendiente` TINYINT NOT NULL DEFAULT '0' AFTER `id_casino`;
--hay que hacer join update antes de agregar la fk GG

----
--holis
-----

CREATE TABLE `informe_fiscalizadores` (
  `id_informe_fiscalizadores` int(11) NOT NULL,
  `id_casino` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `cumplio_minimo` tinyint(1) NOT NULL,
  `cant_cierres` int(11) DEFAULT NULL,
  `cant_aperturas` int(11) DEFAULT NULL,
  `cant_mesas_abiertas` int(11) DEFAULT NULL,
  `cant_mesas_totales` int(11) DEFAULT NULL,
  `cant_mesas_con_diferencia` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `informe_fiscalizadores`
  ADD PRIMARY KEY (`id_informe_fiscalizadores`),
  ADD KEY `id_casino_fk_fisca` (`id_casino`);

ALTER TABLE `informe_fiscalizadores`
  MODIFY `id_informe_fiscalizadores` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
--------

INSERT INTO `estado_relevamiento` (`id_estado_relevamiento`, `descripcion`) VALUES (7,'Validado con Observación');
ALTER TABLE `relevamiento_apuestas_mesas` ADD `nro_turno` INT NOT NULL AFTER `observaciones`;
---
ALTER TABLE `relevamiento_apuestas_mesas` ADD `observaciones_validacion` VARCHAR(200) NULL DEFAULT NULL AFTER `id_estado_relevamiento`;
ALTER TABLE `relevamiento_apuestas_mesas` ADD `es_backup` TINYINT NOT NULL DEFAULT '0' AFTER `id_relevamiento_apuestas`;
ALTER TABLE `relevamiento_apuestas_mesas` ADD `id_controlador` INT NULL AFTER `id_cargador`, ADD INDEX `id_controlador_rel_apuestas_idx` (`id_controlador`);

--30-01-2019
ALTER TABLE `relevamiento_apuestas_mesas` ADD `id_controlador` INT NOT NULL AFTER `id_estado_relevamiento`, ADD INDEX `controlador_relapuestas_idx` (`id_controlador`);
ALTER TABLE `relevamiento_apuestas_mesas` ADD `observaciones` VARCHAR(200) NULL DEFAULT NULL AFTER `id_estado_relevamiento`;
ALTER TABLE `relevamiento_apuestas_mesas` CHANGE `id_controlador` `id_controlador` INT(11) NULL;
-------

ALTER TABLE `detalle_relevamiento_apuestas` CHANGE `id_detalle_relevamiento_apuestas` `id_detalle_relevamiento_apuestas` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `minimo` `minimo` INT(11) NULL, CHANGE `maximo` `maximo` INT(11) NULL, CHANGE `id_estado_mesa` `id_estado_mesa` INT(11) NOT NULL DEFAULT '2';
UPDATE `turno` SET `hora_relevamientos_apuestas`='15:20';
--

ALTER TABLE `detalle_relevamiento_apuestas` CHANGE `id_tipo_mesa` `id_tipo_mesa` INT(11) NOT NULL;

---
ALTER TABLE `relevamiento_apuestas_mesas` DROP `validado`;
ALTER TABLE `detalle_relevamiento_apuestas` DROP `estado_cerrada`;
ALTER TABLE `detalle_relevamiento_apuestas` ADD `id_estado_mesa` INT NOT NULL AFTER `id_juego_mesa`, ADD INDEX `estado_mesa_det_apuesta` (`id_estado_mesa`);

---
--APUESTAS
CREATE TABLE IF NOT EXISTS `relevamiento_apuestas_mesas` (
  `id_relevamiento_apuestas` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `hora_ejecucion` time DEFAULT NULL,
  `hora_propuesta` time NOT NULL,
  `id_turno` int(11) NOT NULL,
  `id_fiscalizador` int(11) DEFAULT NULL,
  `id_cargador` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_relevamiento_apuestas`),
  KEY `id_Cargador_apuestas_idx` (`id_cargador`),
  KEY `fiscalizador_Apuestas_idx` (`id_fiscalizador`),
  KEY `turno_apuestas_idx` (`id_turno`)
);
CREATE TABLE IF NOT EXISTS `detalle_relevamiento_apuestas` (
  `id_detalle_relevamiento_apuestas` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_mesa` date NOT NULL,
  `nro_mesa` int(11) NOT NULL,
  `nombre_juego` varchar(45) NOT NULL,
  `posiciones` int(11) NOT NULL,
  `estado_cerrada` tinyint(4) NOT NULL,
  `minimo` int(11) NOT NULL,
  `maximo` int(11) NOT NULL,
  `codigo_mesa` varchar(15) NOT NULL,
  `id_relevamiento_apuestas` int(11) NOT NULL,
  `id_mesa_de_panio` int(11) NOT NULL,
  PRIMARY KEY (`id_detalle_relevamiento_apuestas`),
  KEY `tipo_rel_apuestas_idx` (`id_tipo_mesa`),
  KEY `mesa_detalle_apuesta_idx` (`id_mesa_de_panio`),
  KEY `relevamientos_apuwstsas_idx` (`id_relevamiento_apuestas`)
) ;

ALTER TABLE `relevamiento_apuestas_mesas` ADD `id_casino` INT NOT NULL AFTER `id_cargador`, ADD `validado` TINYINT NOT NULL DEFAULT '0' AFTER `id_casino`, ADD `created_at` TIMESTAMP NULL DEFAULT NULL AFTER `validado`, ADD `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER `created_at`, ADD `deleted_at` TIMESTAMP NULL DEFAULT NULL AFTER `updated_at`, ADD INDEX `casino_rel_apuestas_idx` (`id_casino`);

CREATE TABLE `turno` (
  `id_turno` int(11) NOT NULL,
  `id_casino` int(11) NOT NULL,
  `nro_turno` varchar(45) NOT NULL,
  `dia_desde` int(11) NOT NULL COMMENT '1 -> lunes ... 7 ->domingo',
  `dia_hasta` int(11) NOT NULL COMMENT '1 -> lunes ... 7 ->domingo',
  `entrada` time NOT NULL,
  `salida` time NOT NULL,
  `hora_relevamientos_apuestas` time
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `turno`
--

INSERT INTO `turno` (`id_turno`, `id_casino`, `nro_turno`, `dia_desde`, `dia_hasta`, `entrada`, `salida`) VALUES
(1, 1, '1', 0, 4, '12:00:00', '19:00:00'),
(2, 1, '2', 0, 4, '18:00:00', '00:30:00'),
(3, 1, '3', 0, 4, '23:15:00', '05:15:00'),
(4, 1, '1', 5, 5, '12:00:00', '19:00:00'),
(5, 1, '2', 5, 5, '18:30:00', '01:00:00'),
(6, 1, '3', 5, 5, '00:15:00', '06:15:00'),
(7, 1, '1', 6, 6, '12:00:00', '19:00:00'),
(8, 1, '2', 6, 6, '18:45:00', '01:15:00'),
(9, 1, '3', 6, 6, '01:00:00', '07:15:00'),
(10, 2, '1', 0, 6, '06:30:00', '13:30:00'),
(11, 2, '2', 0, 6, '12:30:00', '19:30:00'),
(12, 2, '3', 0, 6, '18:45:00', '01:15:00'),
(13, 2, '4', 0, 6, '00:45:00', '07:00:00'),
(14, 3, '1', 0, 6, '10:00:00', '17:00:00'),
(15, 3, '2', 0, 6, '16:45:00', '23:30:00'),
(16, 3, '3', 0, 6, '23:15:00', '05:30:00');

ALTER TABLE `detalle_relevamiento_apuestas` ADD `id_juego_mesa` INT NOT NULL AFTER `id_mesa_de_panio`, ADD INDEX `juego_mesa_apuestas_idx` (`id_juego_mesa`);
ALTER TABLE `relevamiento_apuestas_mesas` ADD `id_estado_relevamiento` INT NOT NULL AFTER `id_casino`, ADD INDEX `estado_rel_apuestas_idx` (`id_estado_relevamiento`);
--
-- Table structure for table `estado_relevamiento`
--

CREATE TABLE `estado_relevamiento` (
  `id_estado_relevamiento` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estado_relevamiento`
--

INSERT INTO `estado_relevamiento` (`id_estado_relevamiento`, `descripcion`) VALUES
(1, 'Generado'),
(2, 'Cargando'),
(3, 'Finalizado'),
(4, 'Validado'),
(5, 'Sin relevar'),
(6, 'Error');


CREATE TABLE `estado_mesa` (
  `id_estado_mesa` int(11) NOT NULL,
  `descripcion_mesa` varchar(45) NOT NULL,
  `siglas_mesa` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estado_relevamiento`
--

INSERT INTO `estado_mesa` (`id_estado_mesa`, `descripcion_mesa`,`siglas_mesa`) VALUES
(1, 'ABIERTA','A'),
(2, 'CERRADA','C'),
(3, 'EN TORNEO','T');

-------------------
ALTER TABLE `mesa_de_panio` ADD `multimoneda` TINYINT NOT NULL DEFAULT '0' AFTER `descripcion`, ADD INDEX `multimoneda_mesa_idx` (`multimoneda`);
--
ALTER TABLE `apertura_mesa` ADD `id_moneda` INT NULL DEFAULT NULL AFTER `id_estado_cierre`, ADD INDEX `moneda_apertura_mesa_idx` (`id_moneda`);
ALTER TABLE `apertura_mesa` ADD CONSTRAINT `fk_moneda_ap` FOREIGN KEY (`id_moneda`) REFERENCES `moneda`(`id_moneda`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `cierre_mesa` ADD `id_moneda` INT NULL DEFAULT NULL AFTER `id_casino`, ADD INDEX `moneda_cierre_mesas_idx` (`id_moneda`);
ALTER TABLE `cierre_mesa` ADD CONSTRAINT `fk_moneda_cierre` FOREIGN KEY (`id_moneda`) REFERENCES `moneda`(`id_moneda`) ON DELETE NO ACTION ON UPDATE NO ACTION;
---

ALTER TABLE `importacion_mensual_mesas` CHANGE `total_mensual` `total_utilidad_mensual` DOUBLE(15,2) NULL DEFAULT NULL;
ALTER TABLE `importacion_mensual_mesas` ADD `total_drop_mensual` DECIMAL(15,2) NOT NULL AFTER `total_utilidad_mensual`;
ALTER TABLE `importacion_mensual_mesas` CHANGE `total_drop_mensual` `total_drop_mensual` DOUBLE(15,2) NULL DEFAULT NULL;
ALTER TABLE `importacion_mensual_mesas` CHANGE `diferencias` `diferencias` TINYINT(1) NOT NULL DEFAULT '1';
ALTER TABLE `detalle_importacion_mensual_mesas` ADD `utilidad_calculada` DOUBLE(15,2) NULL DEFAULT NULL AFTER `diferencias`;

----------
ALTER TABLE `importacion_mensual_mesas` ADD `observaciones` VARCHAR(200) NULL DEFAULT NULL AFTER `diferencias`;

-----------

ALTER TABLE `importacion_diaria_mesas` ADD `cotizacion` DOUBLE(15,2) NULL DEFAULT NULL AFTER `observacion`;
ALTER TABLE `detalle_importacion_mensual_mesas` CHANGE `drop` `droop` DOUBLE(15,2) NULL DEFAULT NULL;
ALTER TABLE `detalle_importacion_mensual_mesas` ADD `cotizacion` DOUBLE(15,2) NULL DEFAULT NULL AFTER `hold`;
--.-
ALTER TABLE `filas_csv_mesas_bingos` CHANGE `row_7` `row_7` DATE NULL DEFAULT NULL;

---


CREATE TABLE IF NOT EXISTS `importacion_diaria_mesas` (
  `id_importacion_diaria_mesas` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATE NULL,
  `id_casino` INT NULL,
  `id_moneda` INT NULL,
  `total_diario` DOUBLE(15,2) NULL,
  PRIMARY KEY (`id_importacion_diaria_mesas`),
  INDEX `fk_casino_importacion_mesas_idx` (`id_casino` ASC) ,
  INDEX `fk_moneda_importacion_mesas_diaria_idx` (`id_moneda` ASC) ,
  UNIQUE INDEX `id_importacion_diaria_mesas_UNIQUE` (`id_importacion_diaria_mesas` ASC) ,
  CONSTRAINT `fk_casino_importacion_mesas_diaria`
    FOREIGN KEY (`id_casino`)
    REFERENCES `casino` (`id_casino`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_moneda_importacion_mesas_diaria`
    FOREIGN KEY (`id_moneda`)
    REFERENCES `moneda` (`id_moneda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `detalle_importacion_diaria_mesas` (
  `id_detalle_importacion_diaria_mesas` INT NOT NULL AUTO_INCREMENT,
  `id_importacion_diaria_mesas` INT NULL,
  `id_mesa_de_panio` INT NULL,
  `id_moneda` INT NULL,
  `fecha` DATE NULL,
  `utilidad` DOUBLE NULL,
  `drop` DOUBLE(15,2) NULL,
  `hold` DOUBLE(15,2) NULL,
  `id_juego_mesa` INT NULL,
  `nro_mesa` VARCHAR(45) NULL,
  `nombre_juego` VARCHAR(45) NULL,
  `codigo_moneda` VARCHAR(45) NULL,
  PRIMARY KEY (`id_detalle_importacion_diaria_mesas`),
  UNIQUE INDEX `id_detalle_importacion_diaria_mesas_UNIQUE` (`id_detalle_importacion_diaria_mesas` ASC) ,
  INDEX `fk_detalle_imp_mesas_diaria_mesa_idx` (`id_mesa_de_panio` ASC) ,
  INDEX `fk_detalle_imp_mesas_diaria_juego_idx` (`id_juego_mesa` ASC) ,
  INDEX `fk_detalle_imp_mesas_diaria_imp_mesas_idx` (`id_importacion_diaria_mesas` ASC) ,
  CONSTRAINT `fk_detalle_imp_mesas_diaria_mesa`
    FOREIGN KEY (`id_mesa_de_panio`)
    REFERENCES `mesa_de_panio` (`id_mesa_de_panio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_imp_mesas_diaria_juego`
    FOREIGN KEY (`id_juego_mesa`)
    REFERENCES `juego_mesa` (`id_juego_mesa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_detalle_imp_mesas_diaria_imp_mesas`
      FOREIGN KEY (`id_importacion_diaria_mesas`)
      REFERENCES `importacion_diaria_mesas` (`id_importacion_diaria_mesas`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `importacion_mensual_mesas` (
  `id_importacion_mensual_mesas` INT NOT NULL AUTO_INCREMENT,
  `fecha_mes` DATE NULL,
  `id_casino` INT NULL,
  `id_moneda` INT NULL,
  `total_mensual` DOUBLE(15,2) NULL,
  `cotizacion_dolar` DOUBLE(4,4) NULL,
  `cotizacion_euro` DOUBLE(4,4) NULL,
  PRIMARY KEY (`id_importacion_mensual_mesas`),
  INDEX `fk_imp_mens_mesas_casino_idx` (`id_casino` ASC) ,
  INDEX `fk_imp_mens_mesas_moneda_idx` (`id_moneda` ASC) ,
  CONSTRAINT `fk_imp_mens_mesas_casino`
    FOREIGN KEY (`id_casino`)
    REFERENCES `casino` (`id_casino`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_imp_mens_mesas_moneda`
    FOREIGN KEY (`id_moneda`)
    REFERENCES `moneda` (`id_moneda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `detalle_importacion_mensual_mesas` (
  `id_detalle_importacion_mensual_mesas` INT NOT NULL AUTO_INCREMENT,
  `id_importacion_mensual_mesas` INT NULL,
  `fecha_dia` DATE NULL,
  `total_diario` DOUBLE(15,2) NULL,
  `utilidad` DOUBLE(15,2) NULL,
  `drop` DOUBLE(15,2) NULL,
  `hold` DOUBLE(15,2) NULL,
  PRIMARY KEY (`id_detalle_importacion_mensual_mesas`),
  INDEX `fk_importacion_mensual_mesas_detalles_idx` (`id_importacion_mensual_mesas` ASC) ,
  CONSTRAINT `fk_importacion_mensual_mesas_detalles`
   FOREIGN KEY (`id_importacion_mensual_mesas`)
    REFERENCES `importacion_mensual_mesas` (`id_importacion_mensual_mesas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `canon_mesas` (
  `id_canon_mesas` INT NOT NULL AUTO_INCREMENT,
  `id_casino` INT NULL,
  `valor_canon` DOUBLE(15,2) NULL,
  `periodo_anio_inicio` INT NULL,
  `periodo_anio_final` INT NULL,
  PRIMARY KEY (`id_canon_mesas`),
  INDEX `fk_casino_canon_mesas_idx` (`id_casino` ASC) ,
  CONSTRAINT `fk_casino_canon_mesas`
    FOREIGN KEY (`id_casino`)
    REFERENCES `casino` (`id_casino`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `informe_final_mesas` (
  `id_informe_final_mesas` INT NOT NULL AUTO_INCREMENT,
  `id_casino` INT NULL,
  `anio_inicio` INT NULL,
  `anio_final` INT NULL,
  `base_anterior_dolar` DOUBLE(15,2) NULL,
  `base_anterior_euro` DOUBLE(15,2) NULL,
  `monto_anterior_dolar` DOUBLE(15,2) NULL,
  `monto_anterior_euro` DOUBLE(15,2) NULL,
  `porcentaje_variacion` DOUBLE(15,2) NULL,
  `base_actual_dolar` DOUBLE(15,2) NULL,
  `base_actual_euro` DOUBLE(15,2) NULL,
  `base_cobrado_dolar` DOUBLE(15,2) NULL,
  `base_cobrado_euro` DOUBLE(15,2) NULL,
  PRIMARY KEY (`id_informe_final_mesas`),
  INDEX `fk_final_mesas_casino_idx` (`id_casino` ASC) ,
  CONSTRAINT `fk_final_mesas_casino`
    FOREIGN KEY (`id_casino`)
    REFERENCES `casino` (`id_casino`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `detalle_informe_final_mesas` (
  `id_detalle_informe_final_mesas` INT NOT NULL AUTO_INCREMENT,
  `total_mes_anio_anterior` DOUBLE(15,2) NULL,
  `total_mes_actual` DOUBLE(15,2) NULL,
  `cotizacion_euro_anterior` DOUBLE(4,4) NULL,
  `cotizacion_dolar_actual` DOUBLE(4,4) NULL,
  `cotizacion_euro_actual` DOUBLE(4,4) NULL,
  `mes_detalle` INT NULL,
  `nombre_mes` VARCHAR(45) NULL,
  `id_informe_final_mesas` INT NULL,
  PRIMARY KEY (`id_detalle_informe_final_mesas`),
  INDEX `fk_informe_final_detalle_mes_idx` (`id_informe_final_mesas` ASC) ,
  CONSTRAINT `fk_informe_final_detalle_mes`
    FOREIGN KEY (`id_informe_final_mesas`)
    REFERENCES `informe_final_mesas` (`id_informe_final_mesas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


ALTER TABLE `importacion_diaria_mesas` ADD `diferencias` BOOLEAN NOT NULL DEFAULT FALSE AFTER `total_diario`;
ALTER TABLE `importacion_mensual_mesas` ADD `diferencias` BOOLEAN NOT NULL DEFAULT FALSE AFTER `cotizacion_euro`;

ALTER TABLE `juego_mesa` ADD `posiciones` INT(5) NOT NULL DEFAULT '0' AFTER `siglas`;

ALTER TABLE `detalle_importacion_diaria_mesas` ADD `reposiciones` DOUBLE(15,2) NULL DEFAULT NULL AFTER `hold`, ADD `retiros` DOUBLE(15,2) NULL DEFAULT NULL AFTER `reposiciones`;
ALTER TABLE `filas_csv_mesas_bingos` CHANGE `row_6` `row_6` DOUBLE(15,2) NULL DEFAULT NULL;
ALTER TABLE `detalle_importacion_diaria_mesas` ADD `tipo_mesa` VARCHAR(45) NULL DEFAULT NULL AFTER `codigo_moneda`;
---agregar indices de la tabla importacion_diaria_mesas y detalle_importacion_diaria_mesas

----------

ALTER TABLE `importacion_diaria_mesas` ADD `observacion` VARCHAR(200) NULL DEFAULT NULL AFTER `diferencias`;
ALTER TABLE `apertura_mesa` ADD `observacion` VARCHAR(200) NULL DEFAULT NULL AFTER `hora`;
ALTER TABLE `importacion_diaria_mesas` ADD `validado` TINYINT NOT NULL DEFAULT '0' AFTER `fecha`;

----------------------

ALTER TABLE `detalle_importacion_mensual_mesas` ADD `diferencias` TINYINT NOT NULL DEFAULT '0' AFTER `hold`;
ALTER TABLE `importacion_mensual_mesas` ADD `validado` TINYINT NOT NULL DEFAULT '0' AFTER `deleted_at`, ADD `observacion` VARCHAR(200) NULL DEFAULT NULL AFTER `validado`;
