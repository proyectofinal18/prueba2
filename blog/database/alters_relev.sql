---
ALTER TABLE `detalle_relevamiento_apuestas` ADD CONSTRAINT `relevamiento_apuest_fk`
FOREIGN KEY (`id_relevamiento_apuestas`) REFERENCES `relevamiento_apuestas_mesas`(`id_relevamiento_apuestas`)
ON DELETE NO ACTION ON UPDATE NO ACTION; ALTER TABLE `detalle_relevamiento_apuestas`
ADD CONSTRAINT `juego_mesa_relevamietos_ap_fk` FOREIGN KEY (`id_juego_mesa`)
REFERENCES `juego_mesa`(`id_juego_mesa`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `detalle_relevamiento_apuestas` ADD CONSTRAINT `estado_mesa_relevamietos_ap_fk`
FOREIGN KEY (`id_estado_mesa`) REFERENCES `estado_mesa`(`id_estado_mesa`) ON DELETE
NO ACTION ON UPDATE NO ACTION; ALTER TABLE `detalle_relevamiento_apuestas`
ADD CONSTRAINT `mesa_relevamietos_apuest_fk` FOREIGN KEY (`id_mesa_de_panio`)
REFERENCES `mesa_de_panio`(`id_mesa_de_panio`) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE `relevamiento_apuestas_mesas` ADD CONSTRAINT `turno_relev_apuesta`
FOREIGN KEY (`id_turno`) REFERENCES `turno`(`id_turno`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `relevamiento_apuestas_mesas` ADD CONSTRAINT `estado_relev_apuestas`
FOREIGN KEY (`id_estado_relevamiento`) REFERENCES `estado_relevamiento`(`id_estado_relevamiento`)
ON DELETE NO ACTION ON UPDATE NO ACTION; ALTER TABLE `relevamiento_apuestas_mesas`
ADD CONSTRAINT `controlador_relev_apuestas` FOREIGN KEY (`id_controlador`)
REFERENCES `usuario`(`id_usuario`) ON DELETE NO ACTION ON UPDATE RESTRICT;
ALTER TABLE `relevamiento_apuestas_mesas` ADD CONSTRAINT `fisca_relev_apuestas`
FOREIGN KEY (`id_fiscalizador`) REFERENCES `usuario`(`id_usuario`) ON DELETE
NO ACTION ON UPDATE NO ACTION; ALTER TABLE `relevamiento_apuestas_mesas`
ADD CONSTRAINT `cargo_relev_apuestas` FOREIGN KEY (`id_cargador`)
REFERENCES `usuario`(`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;


INSERT INTO `estado_mesa` (`id_estado_mesa`, `descripcion_mesa`, `siglas_mesa`) VALUES
(1, 'ABIERTA', 'A'),
(2, 'CERRADA', 'C'),
(3, 'EN TORNEO', 'T');


INSERT INTO `permiso` (`id_permiso`, `descripcion`) VALUES (NULL, 'm_abm_apuesta_minima'),
 (NULL, 'm_consultar_relevamientos_apuestas'), (NULL, 'm_validar_eliminar_relevamientos_apuestas'),
 (NULL, 'm_abm_relevamiento_apuestas');

 ALTER TABLE `turno` ADD `hora_propuesta` TIME NOT NULL DEFAULT '00:00:00' AFTER `salida`,
 ADD `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `hora_propuesta`,
 ADD `updated_at` TIMESTAMP NULL AFTER `created_at`, ADD `deleted_at` TIMESTAMP NULL AFTER `updated_at`;

 INSERT INTO `permiso` (`id_permiso`, `descripcion`) VALUES (NULL, 'm_ver_seccion_apuestas');


CREATE TABLE `apuesta_minima_juego` ( `id_apuesta_minima` INT NOT NULL AUTO_INCREMENT ,
   `descripcion` VARCHAR(200) NULL , `id_juego_mesa` INT NULL , `cantidad_requerida` INT NULL ,
   `apuesta_minima` INT NULL , `created_at` TIMESTAMP NOT NULL , `updated_at` TIMESTAMP NULL ,
    `deleted_at` TIMESTAMP NULL , PRIMARY KEY (`id_apuesta_minima`), INDEX `juego_mesa_apuesta_minma_idx`
    (`id_juego_mesa`)) ENGINE = InnoDB;

ALTER TABLE `apuesta_minima_juego` ADD `id_casino` INT NULL AFTER `id_juego_mesa`,
 ADD INDEX `idx_casino_apuesta_min` (`id_casino`);


 ------------

 -- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-02-2019 a las 21:28:01
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdmmb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fiscalizador_relevo_apuesta`
--

CREATE TABLE `fiscalizador_relevo_apuesta` (
  `id_fiscalizador_relevo_apuesta` int(11) NOT NULL,
  `id_usuario` int(11) UNSIGNED NOT NULL,
  `id_relevamiento_apuestas_mesas` int(11) NOT NULL,
  `id_sector_mesas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `fiscalizador_relevo_apuesta`
--
ALTER TABLE `fiscalizador_relevo_apuesta`
  ADD PRIMARY KEY (`id_fiscalizador_relevo_apuesta`),
  ADD KEY `fisca_apuesta_tiene_rel_idx` (`id_usuario`),
  ADD KEY `rel_apuestas_tiene_fisca_idx` (`id_relevamiento_apuestas_mesas`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `fiscalizador_relevo_apuesta`
--
ALTER TABLE `fiscalizador_relevo_apuesta`
  ADD CONSTRAINT `rel_apuestas_fisca_tiene_rel_apuestas_fk`
  FOREIGN KEY (`id_relevamiento_apuestas_mesas`)
  REFERENCES `relevamiento_apuestas_mesas` (`id_relevamiento_apuestas`)
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuario_fisca_tiene_rel_apuestas_fk`
  FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

ALTER TABLE `relevamiento_apuestas_mesas` ADD `cumplio_minimo` TINYINT NOT NULL AFTER `hora_ejecucion`;
ALTER TABLE `fiscalizador_relevo_apuesta` CHANGE `id_fiscalizador_relevo_apuesta` `id_fiscalizador_relevo_apuesta` INT(11) NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



--chequear antes de hacer

ALTER TABLE `turno` CHANGE `hora_relevamientos_apuestas` `hora_propuesta` TIME NULL DEFAULT NULL;




----------


ALTER TABLE `apuesta_minima_juego` ADD `id_moneda` INT NOT NULL AFTER `id_casino`, ADD INDEX `moneda_apuesta_minima_juego_idx` (`id_moneda`);

ALTER TABLE `apuesta_minima_juego` ADD  CONSTRAINT `apuesta_minima_juego_para_moneda_fk`
FOREIGN KEY (`id_moneda`) REFERENCES `moneda`(`id_moneda`) ON DELETE NO ACTION ON UPDATE NO ACTION;


---

ALTER TABLE `informe_fiscalizadores` ADD `cantidad_abiertas_con_minimo` INT NOT NULL AFTER `cant_mesas_con_diferencia`, ADD `id_apuesta_minima`
INT NOT NULL AFTER `cantidad_abiertas_con_minimo`, ADD INDEX `valor_ap_minima_juego` (`id_apuesta_minima`);


----
ALTER TABLE `informe_fiscalizadores` CHANGE `id_apuesta_minima` `id_apuesta_minima_juego` INT(11) NOT NULL;
