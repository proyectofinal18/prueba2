<?php

namespace Tests\Browser\Usuarios;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use database\factories\UserFactory;
use App\User;

class LoginTest extends DuskTestCase
{
      use DatabaseTransactions;
      protected $user;

      public function get_user()
      {
          if ($this->user) return;
          $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();
          $this->user = factory(User::class)->create();
          $this->user->assignRole('SUPERUSUARIO');
      }

      /** @test */
      public function a_user_receives_errors_for_wrong_login_credentials()
      {
        $this->get_user();
        $this->browse(function (Browser $browser) {
            $browser->visit('login')
                    ->type('user_name',$this->user->user_name)
                    ->type('password','helloooou')
                    ->press('Acceder')
                    ->assertSee('Estas credenciales no coinciden con nuestros registros.');
                    });
      }

      /** @test */
      public function a_user_can_successfully_log_in()
      {
          $this->get_user();
          $userr = $this->user;
          $this->browse(function (Browser $browser) use ($userr) {
              $browser->visit('login')
                      ->type('user_name','camix')
                      ->type('password','39576025')
                      ->press('Acceder')
                      ->assertPathIs('/home');
          });
      }



    // /**
    //  * A Dusk test example.
    //  *
    //  * @return void
    //  */
    // public function testExample()
    // {
    //     $this->browse(function (Browser $browser) {
    //         $browser->visit('/')
    //                 ->assertSee('Laravel');
    //     });
    // }
}
