<?php

namespace Tests\Unit\GestionarMesas;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Spatie\Permission\PermissionRegistrar;

use database\factories\UserFactory;
use App\User;
use database\factories\MesaFactory;
use App\Mesas\Mesa;

class ABMMesasTest extends TestCase
{
  use DatabaseTransactions;

    public function setUp(){
      // first include all the normal setUp operations
      parent::setUp();

      // now re-register all the roles and permissions
      $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function crear_exitosamente_una_mesa()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $user->casinos()->sync([1,2,3]);

        $mesa = factory(Mesa::class)->make();

        $mesa_form = [
          'nro_mesa' => $mesa->nro_mesa,
          'nro_admin' => $mesa->nro_mesa,
          'nombre' => $mesa->nombre,
          'descripcion' => $mesa->descripcion,
          'id_juego_mesa' => $mesa->id_juego_mesa,
          'id_casino' => $mesa->id_casino,
          'id_moneda' => $mesa->id_moneda,
          'id_sector_mesas' =>$mesa->id_sector_mesas,

        ];

        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'mesas/nuevaMesa/'.$mesa->id_casino, $mesa_form);
                      //   dd($response);
        $response->assertStatus(200);

        $this->assertDatabaseHas('mesa_de_panio', $mesa_form);
    }

//
//     public function crear_una_mesa_datos_fails_required()
//     {
//         $user = factory(User::class)->create();
//         $user->assignRole('ADMINISTRADOR');
//         $user->casinos()->sync([1,2,3]);
//
//         $mesa = factory(Mesa::class)->make();
//
//         $mesa_form = [
//           'nro_mesa' => '',
//           'nombre' => $mesa->nombre,
//           'descripcion' => $mesa->descripcion,
//           'id_juego_mesa' => $mesa->id_juego_mesa,
//           'id_casino' => $mesa->id_casino,
//           'id_moneda' => $mesa->id_moneda,
//           'id_sector_mesas' =>$mesa->id_sector_mesas,
//         ];
//
//
//
//         $response = $this->actingAs($user)
//                          ->withSession(['user' => $user])
//                          ->json('POST', 'mesas/crear/'.$mesa->id_casino, $mesa_form);
//
//        $response->assertStatus(422);
//        $response->assertJson(['errors' => ['nro_mesa'=>["El campo Número de Mesa es obligatorio."],
//                                           ]]);
//
//     }
//
//
//
//     public function crear_exitosamente_una_mesa_segunda_fails_nro()
//     {
//         $user = factory(User::class)->create();
//         $user->assignRole('ADMINISTRADOR');
//         $user->casinos()->sync([1,2,3]);
//
//         $mesa = factory(Mesa::class)->create();
//
//         $mesa_form = [
//           'nro_mesa' => $mesa->nro_mesa,
//           'nombre' => $mesa->nombre,
//           'descripcion' => $mesa->descripcion,
//           'id_juego_mesa' => $mesa->id_juego_mesa,
//           'id_casino' => $mesa->id_casino,
//           'id_moneda' => $mesa->id_moneda,
//           'id_sector_mesas' =>$mesa->id_sector_mesas,
//         ];
//
//         $this->assertDatabaseHas('mesa_de_panio', $mesa_form);
//
//         //segunda mesa
//         $mesaa = factory(Mesa::class)->create();
//
//         $mesaa_form = [
//           'nro_mesa' => $mesa->nro_mesa,
//           'nombre' => $mesaa->nombre,
//           'descripcion' => $mesaa->descripcion,
//           'id_juego_mesa' => $mesa->id_juego_mesa,
//           'id_casino' => $mesa->id_casino,
//           'id_moneda' => $mesaa->id_moneda,
//           'id_sector_mesas' =>$mesaa->id_sector_mesas,
//         ];
//
//
//
//         $response2 = $this->actingAs($user)
//                          ->withSession(['user' => $user])
//                          ->json('POST', 'mesas/crear/'.$mesaa->id_casino, $mesaa_form);
//
//        $response2->assertStatus(422);
//
//        $response2->assertJson(['errors' => ['nro_mesa'=>["El campo Número de Mesa es obligatorio."],
//                                           ]]);
//
//     }
//
//
//     /** @test */
//     public function modificar_exitosamente_una_mesa()
//     {
//         $user = factory(User::class)->create();
//         $user->assignRole('ADMINISTRADOR');
//         $user->casinos()->sync([1,2,3]);
//
//         $mesa = factory(Mesa::class)->create();
//
//         $mesa_form = [
//           'id_mesa_de_panio' =>$mesa->id_mesa_de_panio,
//           'nro_mesa' => '1115',
//           'nombre' => $mesa->nombre,
//           'descripcion' => 'pepe',
//           'id_juego_mesa' => $mesa->id_juego_mesa,
//           'id_casino' => $mesa->id_casino,
//           'id_moneda' => $mesa->id_moneda,
//           'id_sector_mesas' =>$mesa->id_sector_mesas,
//         ];
//
//
//
//         $response = $this->actingAs($user)
//                          ->withSession(['user' => $user])
//                          ->json('POST', 'mesas/modificar/'.$mesa->id_casino, $mesa_form);
//
//         $response->assertStatus(200);
//
//         $this->assertDatabaseHas('mesa_de_panio', $mesa_form);
//     }
//
//
// /** @test */
// public function modificar_mesa_fails_nro_continuo()
// {
//     $user = factory(User::class)->create();
//     $user->assignRole('CONTROLADOR');
//     $user->casinos()->sync([1,2,3]);
//
//     $mesa = factory(Mesa::class)->create();
//
//     $mesa_form = [
//       'id_mesa_de_panio' =>$mesa->id_mesa_de_panio,
//       'nro_mesa' => 'xxx',
//       'nombre' => $mesa->nombre,
//       'descripcion' => $mesa->descripcion,
//       'id_juego_mesa' => $mesa->id_juego_mesa,
//       'id_casino' => $mesa->id_casino,
//       'id_moneda' => $mesa->id_moneda,
//       'id_sector_mesas' =>$mesa->id_sector_mesas,
//     ];
//     $response = $this->actingAs($user)
//                      ->withSession(['user' => $user])
//                      ->json('POST', 'mesas/modificar/'.$mesa->id_casino, $mesa_form);
//
//     $response->assertStatus(422);
//     $response->assertJson(['errors' => ['nro_mesa'=>["El campo Número de Mesa debe ser un número entero."],
//                                        ]]);
//
//   }
//
//   /** @test */
//   public function eliminar_mesa()
//   {
//       $user = factory(User::class)->create();
//       $user->assignRole('ADMINISTRADOR');
//       $user->casinos()->sync([1,2,3]);
//
//       $mesa = factory(Mesa::class)->create();
//
//       $mesa_form = [
//         'id_mesa_de_panio' =>$mesa->id_mesa_de_panio,
//         'nro_mesa' => $mesa->nro_mesa,
//         'nombre' => $mesa->nombre,
//         'descripcion' => $mesa->descripcion,
//         'id_juego_mesa' => $mesa->id_juego_mesa,
//         'id_casino' => $mesa->id_casino,
//         'id_moneda' => $mesa->id_moneda,
//         'id_sector_mesas' =>$mesa->id_sector_mesas,
//       ];
//
//       $this->assertDatabaseHas('mesa_de_panio', $mesa_form);
//
//       $response = $this->actingAs($user)
//                        ->withSession(['user' => $user])
//                        ->json('POST','mesas/eliminar/'.$mesa->id_casino.'/'.$mesa->id_mesa_de_panio);
//                       // dd($response);
//       $response->assertStatus(200);
//
//
//     }

}
