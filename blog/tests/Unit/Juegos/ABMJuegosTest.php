<?php

namespace Tests\Unit\Juegos;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Spatie\Permission\PermissionRegistrar;

use database\factories\UserFactory;
use App\User;
use database\factories\JuegoMesaFactory;
use App\Mesas\JuegoMesa;

class ABMJuegosTest extends TestCase
{
  use DatabaseTransactions;

    public function setUp(){
      // first include all the normal setUp operations
      parent::setUp();

      // now re-register all the roles and permissions
      $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function crear_exitosamente_un_juego()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $user->casinos()->sync([1,2,3]);

        $juego = factory(JuegoMesa::class)->make();

        $juego_form = [
          'nombre_juego' => $juego->nombre_juego,
          'siglas' => $juego->siglas,
          'id_tipo_mesa' => $juego->id_tipo_mesa,
          'posiciones'=>$juego->posiciones,
          'id_casino' => $juego->id_casino,

        ];

        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'juegos/nuevoJuego', $juego_form);
                         //dd($response, $juego_form);
        $response->assertStatus(200);

        $this->assertDatabaseHas('juego_mesa', $juego_form);
    }


    public function crear_un_juego_datos_fails_required()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $user->casinos()->sync([1,2,3]);

        $juego = factory(JuegoMesa::class)->make();

        $juego_form = [
          'nombre_juego' => '',
          'siglas' => $juego->siglas,
          'id_tipo_mesa' => $juego->id_tipo_mesa,
          'posiciones'=>$juego->posiciones,
          'id_casino' => $juego->id_casino,
        ];

        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'juegos/nuevoJuego', $juego_form);

       $response->assertStatus(422);
       $response->assertJson(['errors' => ['nombre_juego'=>["El campo nombre_juego es obligatorio."],
                                          ]]);

    }


    public function crear_exitosamente_dos_juegos_segundo_fails_nombre()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $user->casinos()->sync([1,2,3]);

        $juego = factory(JuegoMesa::class)->create();

        $juego_form = [
          'nombre_juego' => $juego->nombre_juego,
          'siglas' => $juego->siglas,
          'id_tipo_mesa' => $juego->id_tipo_mesa,
          'posiciones'=>$juego->posiciones,
          'id_casino' => $juego->id_casino,

        ];

        $this->assertDatabaseHas('juego_mesa', $juego_form);

        //segunda mesa
        $juego2 = factory(JuegoMesa::class)->create();

        $juego2_form = [
          'nombre_juego' => $juego->nombre_juego,
          'siglas' => $juego2->siglas,
          'id_tipo_mesa' => $juego2->id_tipo_mesa,
          'posiciones'=>$juego2->posiciones,
          'id_casino' => $juego2->id_casino,
        ];



        $response2 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'juegos/nuevoJuego', $juego2_form);

       $response2->assertStatus(422);
       dd($response2);
       $response2->assertJson(['errors' => ['nombre_juego'=>["Ya existe un Juego con ese nombre."],
                                          ]]);

    }


    /** @test */
    public function modificar_exitosamente_un_juego()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $user->casinos()->sync([1,2,3]);

        $juego = factory(JuegoMesa::class)->create();

                $juego_form = [
                  'nombre_juego' => $juego->nombre_juego,
                  'siglas' => $juego->siglas,
                  'id_tipo_mesa' => $juego->id_tipo_mesa,
                  'posiciones'=>$juego->posiciones,
                  'id_casino' => $juego->id_casino,

                ];
        $this->assertDatabaseHas('juego_mesa', $juego_form);

        $juego_form = [
          'id_juego_mesa' => $juego->id_juego_mesa,
          'nombre_juego' => $juego->nombre_juego,
          'siglas' => 'MMM',
          'id_tipo_mesa' => $juego->id_tipo_mesa,
          'posiciones'=>$juego->posiciones,
          'id_casino' => $juego->id_casino,

        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'juegos/modificarJuego', $juego_form);

        $response->assertStatus(200);

        $this->assertDatabaseHas('juego_mesa', $juego_form);
    }


/** @test */
public function modificar_juego_fails_sin_nombre()
{
    $user = factory(User::class)->create();
    $user->assignRole('SUPERUSUARIO');
    $user->casinos()->sync([1,2,3]);

    $juego = factory(JuegoMesa::class)->create();

            $juego_form = [
              'nombre_juego' => $juego->nombre_juego,
              'siglas' => $juego->siglas,
              'id_tipo_mesa' => $juego->id_tipo_mesa,
              'posiciones'=>$juego->posiciones,
              'id_casino' => $juego->id_casino,

            ];
            echo $juego;
    $this->assertDatabaseHas('juego_mesa', $juego_form);

    $juego_form = [
      'siglas' => $juego->siglas,
      'id_tipo_mesa' => $juego->id_tipo_mesa,
      'posiciones'=>$juego->posiciones,
      'id_casino' => $juego->id_casino,

    ];
    $response = $this->actingAs($user)
                     ->withSession(['user' => $user])
                     ->json('POST', 'juegos/modificarJuego', $juego_form);
                     dd($response);
    $response->assertStatus(422);
    $response->assertJson(['errors' => ['nombre_juego'=>["El campo Número de Mesa debe ser un número entero."],
                                       ]]);

  }
//
//   /** @test */
//
// public function eliminar_juego()
//   {
//       $user = factory(User::class)->create();
//       $user->assignRole('SUPERUSUARIO');
//       $user->casinos()->sync([1,2,3]);
//
//       $juego = factory(JuegoMesa::class)->create();
//
//       $juego_form = [
//         'nombre_juego' => $juego->nombre_juego,
//         'siglas' => $juego->siglas,
//         'id_tipo_mesa' => $juego->id_tipo_mesa,
//         'posiciones'=>$juego->posiciones,
//         'id_casino' => $juego->id_casino,
//
//       ];
//       $this->assertDatabaseHas('juego_mesa', $juego_form);
//
//       $response = $this->actingAs($user)
//                        ->withSession(['user' => $user])
//                        ->json('GET','juegos/bajaJuego/'.$juego->id_casino);
//                       // dd($response);
//       $response->assertStatus(200);
//
//
//     }

}
