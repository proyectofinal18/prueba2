<?php

namespace Tests\Unit\SeccionCasino;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Spatie\Permission\PermissionRegistrar;
use database\factories\UserFactory;
use database\factories\CasinoFactory;
use App\User;
use App\Casino;



class GestionCasinoTest extends TestCase
{
  use DatabaseTransactions;

    public function setUp(){
      // first include all the normal setUp operations
      parent::setUp();

      // now re-register all the roles and permissions
      $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function crear_exitosamente_un_casino()
    {  $user = factory(User::class)->create();
      $user->assignRole('SUPERUSUARIO');

        $casino = factory(Casino::class)->create();

        $task2 = [
          'nombre' =>'New Casino',
          'codigo' => 'NC',

        ];

        $response2 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'casinos/guardarCasino', $task2);
        //me aseguro de que se creó
        $response2->assertStatus(200);

    }

    // /** @test */
    public function crear_un_casino_sin_completar_campo_codigo()
    {  $user = factory(User::class)->create();
      $user->assignRole('SUPERUSUARIO');

        $casino = factory(Casino::class)->create();

        $task2 = [
          'nombre' =>$casino->nombre,
          'codigo' => '' ,

        ];

        $response2 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'casinos/guardarCasino', $task2);

        //me aseguro de que se creó
        $response2->assertStatus(422);


        $response2->assertJson(['errors' => ['codigo'=>["El campo Código de Casino es obligatorio."],
                                                   ]
                              ]);

    }

    /** @test */
    public function crear_casino_con_nombre_existente()
    {  $user = factory(User::class)->create();
      $user->assignRole('SUPERUSUARIO');

        $casino = factory(Casino::class)->create();

        $task2 = [
          'nombre' =>'Santa Fe',
          'codigo' => $casino->codigo ,

        ];

        $response2 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'casinos/guardarCasino', $task2);

        $response2->assertStatus(422);
        $response2->assertJson(['errors' => ['nombre'=>["El valor del campo Nombre del Casino ya está en uso."],
                                                   ]
                              ]);

    }

    // /** @test */
    public function crear_un_casino_con_codigo_existente()
    {  $user = factory(User::class)->create();
      $user->assignRole('SUPERUSUARIO');

        $casino = factory(Casino::class)->create();

        $task2 = [
          'nombre' =>$casino->nombre,
          'codigo' => 'MEL' ,

        ];

        $response2 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'casinos/guardarCasino', $task2);

        //me aseguro de que se creó
        $response2->assertStatus(422);

        $response2->assertJson(['errors' => ['codigo'=>["El campo Código de Casino ya está en uso."],
                                                   ]
                              ]);

    }

    // /** @test */
    public function modificar_codigo_de_casino_correctamente()
    {  $user = factory(User::class)->create();
      $user->assignRole('SUPERUSUARIO');

        $casino = factory(Casino::class)->create();

        $task2 = [
          'nombre' =>'New Casino2',
          'codigo' => 'NC2' ,

        ];

        $response2 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'casinos/guardarCasino', $task2);

        //me aseguro de que se creó
        $response2->assertStatus(200);

        $cas = Casino::where('nombre','=','New Casino2')->first();
        $datos = [
          'id_casino' => $cas->id_casino ,
          'codigo' => 'UNO' ,

        ];

        $response = $this->actingAs($user)
                          ->withSession(['user' => $user])
                          ->json('POST', 'usuarios/modificarCasino', $datos);


        $response->assertJson(200);

    }

    // /** @test */
    public function modificar_codigo_de_casino_invalido()
    {  $user = factory(User::class)->create();
      $user->assignRole('SUPERUSUARIO');

        $casino = factory(Casino::class)->create();

        $task2 = [
          'nombre' =>'New Casino2',
          'codigo' => 'NC2' ,

        ];

        $response2 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'casinos/guardarCasino', $task2);

        //me aseguro de que se creó
        $response2->assertStatus(200);

        $cas = Casino::where('nombre','=','New Casino2')->first();
        $datos = [
          'id_casino' => $cas->id_casino ,
          'codigo' => 'UNOYT' ,

        ];

        $response = $this->actingAs($user)
                          ->withSession(['user' => $user])
                          ->json('POST', 'usuarios/modificarCasino', $datos);


        $response->assertJson(422);
        $response2->assertJson(['errors' => ['codigo'=>["El campo Código de Casino no debe contener más de 3 caracteres."],
                                                   ]
                              ]);

    }
}
