<?php

namespace Tests\Unit\Sector;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Spatie\Permission\PermissionRegistrar;

use database\factories\UserFactory;
use App\User;
use database\factories\SectorMesaFactory;
use App\Mesas\SectorMesa;

class ABMSectoresTest extends TestCase
{
  use DatabaseTransactions;

    public function setUp(){
      // first include all the normal setUp operations
      parent::setUp();

      // now re-register all the roles and permissions
      $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function creacion_exitosa()
    {
        $user = factory(User::class)->create();
        $user->assignRole('ADMINISTRADOR');
        $user->casinos()->sync([1,2,3]);

        $sector = factory(SectorMesa::class)->make();

        $sector_form = [
          'descripcion' => $sector->descripcion,
          'id_casino' => $sector->id_casino,

        ];

        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'sectores-mesas/nuevoSector', $sector_form);

        $response->assertStatus(200);

        $this->assertDatabaseHas('sector_mesa', $sector_form);
    }


    public function crear_sin_descripcion_error()
    {
        $user = factory(User::class)->create();
        $user->assignRole('ADMINISTRADOR');
        $user->casinos()->sync([1,2,3]);

        $sector = factory(SectorMesa::class)->make();

        $sector_form = [
          'descripcion' => '',
          'id_casino' => $sector->id_casino,

        ];

        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'sectores-mesas/nuevoSector', $sector_form);

       $response->assertStatus(422);
       $response->assertJson(['errors' => ['descripcion'=>["El campo Descripcion de Sector es obligatorio."],
                                          ]]);
    }
    public function crear_sin_casino_error()
    {
        $user = factory(User::class)->create();
        $user->assignRole('ADMINISTRADOR');
        $user->casinos()->sync([1,2,3]);

        $sector = factory(SectorMesa::class)->make();

        $sector_form = [
          'descripcion' => $sector->descripcion,
          'id_casino' => '',

        ];

        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'sectores-mesas/nuevoSector', $sector_form);

       $response->assertStatus(422);
       $response->assertJson(['errors' => ['casino'=>["Debe seleccionar un Casino para este nuevo Sector."],
                                          ]]);
    }


    public function creacion_sectore_descripcion_existente()
    {
        $user = factory(User::class)->create();
        $user->assignRole('ADMINISTRADOR');
        $user->casinos()->sync([1,2,3]);

        $sector = factory(SectorMesa::class)->make();

        $sector_form = [
          'descripcion' => $sector->descripcion,
          'id_casino' => $sector->id_casino,

        ];


        $this->assertDatabaseHas('sector_mesa', $sector_form);

        //segunda mesa
        $sector2 = factory(SectorMesa::class)->create();

        $sector2_form = [
          'descripcion' => $sector2->descripcion,
          'id_casino' => $sector2->id_casino,

        ];



        $response2 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'sectores-mesas/nuevoSector', $sector2_form);

       $response2->assertStatus(422);

       $response2->assertJson(['errors' => ['sector_mesa'=>["Ya existe un Sector con esta descripcion."],
                                          ]]);

    }


    /** @test */
    public function modificacion_exitosa()
    {
        $user = factory(User::class)->create();
        $user->assignRole('ADMINISTRADOR');
        $user->casinos()->sync([1,2,3]);

        $sector = factory(SectorMesa::class)->make();

        $sector_form = [
          'descripcion' =>'Vip sss',
          'id_casino' => $sector->id_casino,

        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'sectores-mesas/modificarSector', $sector_form);

        $response->assertStatus(200);

        $this->assertDatabaseHas('sector_mesa', $sector_form);
    }


/** @test */
public function modificacion_sin_descripcion_error()
{
    $user = factory(User::class)->create();
    $user->assignRole('CONTROLADOR');
    $user->casinos()->sync([1,2,3]);

    $sector = factory(SectorMesa::class)->make();

    $sector_form = [
      'descripcion' =>'',
      'id_casino' => $sector->id_casino,

    ];


    $response = $this->actingAs($user)
                     ->withSession(['user' => $user])
                     ->json('POST', 'sectores-mesas/modificarSector/'.$sector->id_sector, $sector_form);

    $response->assertStatus(422);
    $response->assertJson(['errors' => ['descripcion'=>["El campo Descripcion es obligatorio."],
                                       ]]);

  }

  /** @test */

public function eliminar_sector()
  {
      $user = factory(User::class)->create();
      $user->assignRole('ADMINISTRADOR');
      $user->casinos()->sync([1,2,3]);

      $sector = factory(SectorMesa::class)->make();

      $sector_form = [
        'descripcion' =>$sector->descripcion,
        'id_casino' => $sector->id_casino,

      ];
      $this->assertDatabaseHas('sector_mesa', $sector_form);

      $response = $this->actingAs($user)
                       ->withSession(['user' => $user])
                       ->json('GET','sector-mesas/eliminarSector/'.$sector->id_casino);
                      // dd($response);
      $response->assertStatus(200);


    }

//FALTA ERROR CON ELIMINAR SECTOR
}
