<?php

namespace Tests\Unit\SeccionUsuarios;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Spatie\Permission\PermissionRegistrar;
use database\factories\UserFactory;
use App\User;
class ConsultaUserTest extends TestCase
{
  use DatabaseTransactions;

    public function setUp(){
      // first include all the normal setUp operations
      parent::setUp();

      // now re-register all the roles and permissions
      $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function crear_exitosamente_un_usuario_buscarlo()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'pepe' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme pepon',
          'imagen' => '',
          'dni' => 39576024,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);

        $filtros = [
          'usuario' => 'pepe' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme pepon',
        ];
        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/buscar', $filtros);

        $this->assertEquals($response->getData()->usuarios[0]->user_name, "pepe");

    }

    /** @test */
    public function buscar_usuario_inexist_por_user_name()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');

        $filtros = [
          'usuario' => '555' ,
        ];
        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/buscar', $filtros);

        $this->assertEquals($response->getData()->usuarios, []);

    }


}
