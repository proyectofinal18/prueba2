<?php

namespace Tests\Unit\SeccionUsuarios;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Spatie\Permission\PermissionRegistrar;
use database\factories\UserFactory;
use App\User;
class AltaUserTest extends TestCase
{
  use DatabaseTransactions;

    public function setUp(){
      // first include all the normal setUp operations
      parent::setUp();

      // now re-register all the roles and permissions
      $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function crear_exitosamente_un_usuario()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'meme1' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme',
          'imagen' => '',
          'dni' => 39576024,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);

        $response->assertStatus(200);
        //$this->assertDatabaseHas('users', $task);
    }

    /** @test */
    public function crear_un_usuario_erroneo()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'meme1' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme',
          'imagen' => '',
          'dni' => 39576024,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);

        $response->assertStatus(422);
        $response->assertJson(['errors' => ['casinos'=>["El campo Casinos es obligatorio."],
                                           'roles'=>["El campo Roles es obligatorio."]]]);
        //$this->assertDatabaseHas('users', $task);
    }

    /** @test */
    public function crear_un_user_name_existente()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'camix' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme',
          'imagen' => '',
          'dni' => 39576024,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);

        $response->assertStatus(422);
        $response->assertJson(['errors' => ['usuario'=>["El valor del campo Nombre de Usuario ya está en uso."],
                                           ]
                              ]);
        //$this->assertDatabaseHas('users', $task);
    }

    /** @test */
    public function crear_un_usuario_email_invalido()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'memex' ,
          'email' => 'meme.com',
          'nombre' =>'meme',
          'imagen' => '',
          'dni' => 30006020,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);

        $response->assertStatus(422);
        $response->assertJson(['errors' => ['email'=>["Formato de email inválido."],
                                           ]
                              ]);
        //$this->assertDatabaseHas('users', $task);
    }

    /** @test */
    public function crear_un_usuario_nombre_invalido()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'memex' ,
          'email' => 'meme@meme.com',
          'nombre' =>'m25)eme',
          'imagen' => '',
          'dni' => 30006020,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);

        $response->assertStatus(422);
        $response->assertJson(['errors' => ['nombre'=>["El formato del campo Nombre y Apellido es inválido."],
                                           ]
                              ]);
    }

    /** @test */
    public function crear_un_usuario_dni_existente()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'memex' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme j',
          'imagen' => '',
          'dni' => 39576025,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);

        $response->assertStatus(422);
        $response->assertJson(['errors' => ['dni'=>["El dni ya existe para otro usuario."],
                                           ]
                              ]);
    }

    /** @test */
    public function crea_un_usuario_y_eliminarlo_exitosamente()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'meme1' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme',
          'imagen' => '',
          'dni' => 39576024,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);

        $response->assertStatus(200);
        $id = User::where('user_name','=','meme1')->first()->id;

        $response = $this->call('DELETE', 'usuarios/eliminarUsuario',['id' => $id]);

        $this->assertEquals(200, $response->getStatusCode());
        //$this->assertDatabaseHas('users', $task);
    }

}
