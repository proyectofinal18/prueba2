<?php

namespace Tests\Unit\SeccionUsuarios;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Spatie\Permission\PermissionRegistrar;
use database\factories\UserFactory;
use App\User;
use Illuminate\Support\Facades\Hash;


class ModifyUserTest extends TestCase
{
  use DatabaseTransactions;

    public function setUp(){
      // first include all the normal setUp operations
      parent::setUp();

      // now re-register all the roles and permissions
      $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function crear_exitosamente_un_usuario_modificarlo()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'pepe' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme pepon',
          'imagen' => '',
          'dni' => 39576024,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);

        $pepe = User::where('user_name','=','pepe')->first();
        $roles[]=2;
        $datos = [
          'id_usuario' => $pepe->id ,
          'user_name' => 'pepa' ,
          'email' => 'pepa@meme.com',
          'nombre' =>'meme pepon',
          'origen' => 1,
          'casinos' => $roles,
          'roles' => $roles,
        ];

        $response2 = $this->actingAs($user)
                          ->withSession(['user' => $user])
                          ->json('POST', 'usuarios/modificarUsuario', $datos);


        $response2->assertStatus(200);

        $this->assertEquals($response2->getData()->usuario->id, $pepe->id);
        $this->assertEquals($response2->getData()->usuario->user_name, "pepa");
        $this->assertEquals($response2->getData()->usuario->email, "pepa@meme.com");

    }

    /** @test */
    public function crear_exitosamente_dos_usuarios_modificar_nombre_fails()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'pepe' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme pepon',
          'imagen' => '',
          'dni' => 39576024,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);

        $task2 = [
          'usuario' => 'pepa' ,
          'email' => 'meme2@meme.com',
          'nombre' =>'memexx pepon',
          'imagen' => '',
          'dni' => 39576023,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response3 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task2);
        //me aseguro de que se creó

        $response3->assertStatus(200);

        $pepe = User::where('user_name','=','pepe')->first();
        $roles[]=2;
        $datos = [
          'id_usuario' => $pepe->id ,
          'user_name' => 'pepa' ,
          'email' => 'pepa@meme.com',
          'nombre' =>'meme pepon',
          'origen' => 1,
          'casinos' => $roles,
          'roles' => $roles,
        ];

        $response2 = $this->actingAs($user)
                          ->withSession(['user' => $user])
                          ->json('POST', 'usuarios/modificarUsuario', $datos);


        $response2->assertStatus(422);

        $response2->assertJson(['errors' => ['user_name'=>["El nombre de usuario ya existe."],
                                           ]
                              ]);
    }

    /** @test */
    public function crear_exitosamente_un_usuarios_modificar_sin_completar()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'pepe' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme pepon',
          'imagen' => '',
          'dni' => 39576024,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);

        $pepe = User::where('user_name','=','pepe')->first();
        $roles[]=2;
        $datos = [
          'id_usuario' => $pepe->id ,
          'nombre' =>'meme pepon',
          'origen' => 1,
          'email' =>'',
          'user_name' =>'',
          'casinos' => $roles,
          'roles' => $roles,
        ];

        $response2 = $this->actingAs($user)
                          ->withSession(['user' => $user])
                          ->json('POST', 'usuarios/modificarUsuario', $datos);

                          //dd($response2);
        $response2->assertStatus(422);

        $response2->assertJson(['errors' => ['user_name'=>["El campo Nombre Usuario es obligatorio."],
                                             'email'=>["El campo Email es obligatorio."],
                                           ]
                              ]);
    }
    /** @test */
    public function crear_exitosamente_un_usuarios_modificar_email_fails()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'pepe' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme pepon',
          'imagen' => '',
          'dni' => 39576024,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);

        $pepe = User::where('user_name','=','pepe')->first();
        $roles[]=2;
        $datos = [
          'id_usuario' => $pepe->id ,
          'nombre' =>'meme pepon',
          'origen' => 1,
          'email' =>'trhhr',
          'user_name' =>'pepe',
          'casinos' => $roles,
          'roles' => $roles,
        ];

        $response2 = $this->actingAs($user)
                          ->withSession(['user' => $user])
                          ->json('POST', 'usuarios/modificarUsuario', $datos);

                          //dd($response2);
        $response2->assertStatus(422);

        $response2->assertJson(['errors' => ['email'=>["El campo Email debe ser una dirección de correo válida."],
                                           ]
                              ]);
    }

    /** @test */
    public function crear_exitosamente_un_usuarios_modificar_name_fails()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'pepe' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme pepon',
          'imagen' => '',
          'dni' => 39576024,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);

        $pepe = User::where('user_name','=','pepe')->first();
        $roles[]=2;
        $datos = [
          'id_usuario' => $pepe->id ,
          'nombre' =>'meme55 *pepon',
          'origen' => 1,
          'email' =>'meme@meme.com',
          'user_name' =>'pepe',
          'casinos' => $roles,
          'roles' => $roles,
        ];

        $response2 = $this->actingAs($user)
                          ->withSession(['user' => $user])
                          ->json('POST', 'usuarios/modificarUsuario', $datos);

                          //dd($response2);
        $response2->assertStatus(422);

        $response2->assertJson(['errors' => ['nombre'=>["El formato del campo Nombre y Apellido es inválido."],
                                           ]
                              ]);
    }

    /** @test */
    public function resetear_exitosamente_password()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'pepe' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme pepon',
          'imagen' => '',
          'dni' => 39576024,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);

        $pepe = User::where('user_name','=','pepe')->first();

        $response2 = $this->actingAs($user)
                          ->withSession(['user' => $user])
                          ->get('usuarios/resetear-ctr/'.$pepe->id);

                          //dd($response2);
        $pepee = User::where('user_name','=','pepe')->first();
        $response2->assertStatus(200);


        $this->assertEquals(true,(Hash::check($pepee->dni,$pepee->password )));
      }

      /** @test */
      public function crear_exitosamente_un_usuario_sin_cas_rol_fails()
      {
          $user = factory(User::class)->create();
          $user->assignRole('SUPERUSUARIO');
          $roles = array();
          $roles[] = 1;

          $task = [
            'usuario' => 'pepe' ,
            'email' => 'meme@meme.com',
            'nombre' =>'meme pepon',
            'imagen' => '',
            'dni' => 39576024,
            'casinos' => $roles,
            'roles' => $roles,
          ];


          $response = $this->actingAs($user)
                           ->withSession(['user' => $user])
                           ->json('POST', 'usuarios/guardarUsuario', $task);
          //me aseguro de que se creó
          $response->assertStatus(200);
          $roles2 = array();
          $pepe = User::where('user_name','=','pepe')->first();
          $datos = [
            'id_usuario' => $pepe->id ,
            'user_name' => 'pepa' ,
            'email' => 'pepa@meme.com',
            'nombre' =>'meme pepon',
            'origen' => 1,
            'casinos' => $roles2,
            'roles' => $roles2,
          ];

          $response2 = $this->actingAs($user)
                            ->withSession(['user' => $user])
                            ->json('POST', 'usuarios/modificarUsuario', $datos);


          $response2->assertStatus(422);

          $response2->assertJson(['errors' => ['casinos'=>["El campo Casinos es obligatorio."],
                                             'roles'=>["El campo Roles es obligatorio."]]]);

      }


}
