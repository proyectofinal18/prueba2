<?php

namespace Tests\Unit\SeccionUsuarios;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Spatie\Permission\PermissionRegistrar;
use database\factories\UserFactory;
use App\User;
use Illuminate\Support\Facades\Hash;


class ConfigUserTest extends TestCase
{
  use DatabaseTransactions;

    public function setUp(){
      // first include all the normal setUp operations
      parent::setUp();

      // now re-register all the roles and permissions
      $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function crear_exitosamente_un_usuario_configurar_password_ok()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'pepe' ,
          'email' => 'meme@meme.com',
          'nombre' =>'meme pepon',
          'imagen' => '',
          'dni' => 39576024,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);

        $pepe = User::where('user_name','=','pepe')->first();
        $datos = [
          'id_usuario' => $pepe->id,
          'password_actual' => '39576024',
          'password_nuevo' => '39544',
          'password_nuevo_confirmation' => '39544',
        ];

        $response2 = $this->actingAs($pepe)
                          ->withSession(['user' => $pepe])
                          ->json('POST', 'configCuenta/modificarPassword', $datos);

        $response2->assertStatus(200);

        $this->assertEquals($response2->getData()->id, $pepe->id);
        $this->assertEquals($response2->getData()->user_name, $pepe->user_name);
        $this->assertEquals($response2->getData()->dni, $pepe->dni);
        $this->assertEquals($response2->getData()->name, $pepe->name);
        $this->assertEquals($response2->getData()->email, $pepe->email);

    }
    /** @test */
    public function crear_exitosamente_un_usuario_configurar_datos_sin_completar_campos()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task2 = [
          'usuario' => 'Paulina' ,
          'email' => 'meme2@meme.com',
          'nombre' =>'Paulina Rubio',
          'imagen' => '',
          'dni' => 39576023,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response3 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task2);
        //me aseguro de que se creó

        $response3->assertStatus(200);

        $paulina = User::where('user_name','=','Paulina')->first();
        $roles[]=2;
        $datos = [
          'id_usuario' => $paulina->id ,
          'nombre' => $paulina->name,
          'user_name' => '' ,
          'email' => '',

        ];

        $response2 = $this->actingAs($paulina)
                          ->withSession(['user' => $paulina])
                          ->json('POST','configCuenta/modificarDatos', $datos);

        $response2->assertStatus(422);

        $response2->assertJson(['errors' => ['email'=>["El campo Email es obligatorio."],
                                            'user_name'=>["El campo Nombre Usuario es obligatorio."]]
                                ]);
    }

    /** @test */
    public function crear_exitosamente_un_usuario_configurar_passw_actual_incorrecta()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'nuevoUser' ,
          'email' => 'new@meme.com',
          'nombre' =>'user nuevo',
          'imagen' => '',
          'dni' => 39576054,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);

        $nuevo = User::where('user_name','=','nuevoUser')->first();
        $datos = [
          'id_usuario' => $nuevo->id,
          'password_actual' => '39576024',
          'password_nuevo' => '39544',
          'password_nuevo_confirmation' => '39544',
        ];

        $response2 = $this->actingAs($nuevo)
                          ->withSession(['user' => $nuevo])
                          ->json('POST', 'configCuenta/modificarPassword', $datos);

        $response2->assertStatus(422);

        $response2->assertJson(['errors' => ['password_incorrecta'=>["La contraseña actual no coincide con la del usuario."],
                                                                  ]
                                                            ]);

    }

    /** @test */
    public function crear_exitosamente_un_usuario_configurar_password_sin_completar_datos()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task2 = [
          'usuario' => 'Paulina' ,
          'email' => 'meme2@meme.com',
          'nombre' =>'Paulina Rubio',
          'imagen' => '',
          'dni' => 39576023,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response3 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task2);
        //me aseguro de que se creó

        $response3->assertStatus(200);

        $paulina = User::where('user_name','=','Paulina')->first();
        $roles[]=2;
        $datos = [
          'id_usuario' => $paulina->id ,
          'password_actual' => '39576024',
          'password_nuevo' => '',
          'password_nuevo_confirmation' => '',

        ];

        $response2 = $this->actingAs($paulina)
                          ->withSession(['user' => $paulina])
                          ->json('POST','configCuenta/modificarPassword', $datos);

        $response2->assertStatus(422);

        $response2->assertJson(['errors' => ['password_nuevo'=>["El campo Nueva Contraseña es obligatorio."],
                                            'password_nuevo_confirmation'=>["El campo Repita Nueva Contraseña es obligatorio."]]
                                ]);
    }

    /** @test */
    public function crear_exitosamente_un_usuario_configurar_passw_nueva_y_confirmacion_diferentes()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'nuevoUser' ,
          'email' => 'new@meme.com',
          'nombre' =>'user nuevo',
          'imagen' => '',
          'dni' => 39576054,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);

        $nuevo = User::where('user_name','=','nuevoUser')->first();
        $datos = [
          'id_usuario' => $nuevo->id,
          'password_actual' => '39576054',
          'password_nuevo' => '395',
          'password_nuevo_confirmation' => '39544',
        ];

        $response2 = $this->actingAs($nuevo)
                          ->withSession(['user' => $nuevo])
                          ->json('POST', 'configCuenta/modificarPassword', $datos);

        $response2->assertStatus(422);

        $response2->assertJson(['errors' => ['password_nuevo'=>["El campo confirmación de Nueva Contraseña no coincide."],
                                             'password_nuevo_confirmation'=>["Los campos Repita Nueva Contraseña y Nueva Contraseña deben coincidir."]
                                                                  ]
                                                            ]);
    }



    /** @test */
    public function crear_exitosamente_dos_usuarios_configurar_user_name_existente()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'Ricardo' ,
          'email' => 'meme@meme.com',
          'nombre' =>'Ricardo Arjona',
          'imagen' => '',
          'dni' => 22566998,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);

        $task2 = [
          'usuario' => 'Paulina' ,
          'email' => 'meme2@meme.com',
          'nombre' =>'Paulina Rubio',
          'imagen' => '',
          'dni' => 39576023,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response3 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task2);
        //me aseguro de que se creó

        $response3->assertStatus(200);

        $ricardo = User::where('user_name','=','Ricardo')->first();
        $roles[]=2;
        $datos = [
          'id_usuario' => $ricardo->id ,
          'nombre' =>'Ricardo Arjona',
          'user_name' => 'Paulina' ,
          'email' => 'meme@meme.com',

        ];

        $response2 = $this->actingAs($ricardo)
                          ->withSession(['user' => $ricardo])
                          ->json('POST','configCuenta/modificarDatos', $datos);


        $response2->assertStatus(422);

        $response2->assertJson(['errors' => ['user_name'=>["El valor del campo Nombre Usuario ya está en uso."],
                                           ]
                              ]);
    }

    /** @test */
    public function crear_exitosamente_un_usuario_configurar_email_incorrecto()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task = [
          'usuario' => 'Ricardo' ,
          'email' => 'meme@meme.com',
          'nombre' =>'Ricardo Arjona',
          'imagen' => '',
          'dni' => 22566998,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task);
        //me aseguro de que se creó
        $response->assertStatus(200);


        $ricardo = User::where('user_name','=','Ricardo')->first();
        $roles[]=2;
        $datos = [
          'id_usuario' => $ricardo->id ,
          'nombre' =>'Ricardo Arjona',
          'user_name' => 'Ricardo' ,
          'email' => 'meme.com',

        ];

        $response2 = $this->actingAs($ricardo)
                          ->withSession(['user' => $ricardo])
                          ->json('POST','configCuenta/modificarDatos', $datos);


        $response2->assertStatus(422);

        $response2->assertJson(['errors' => ['email'=>["El campo Email debe ser una dirección de correo válida."],
                                           ]
                              ]);
    }


    // /** @test */
    public function crear_exitosamente_un_usuario_configurar_email_correctamente()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task2 = [
          'usuario' => 'Paulina' ,
          'email' => 'meme2@meme.com',
          'nombre' =>'Paulina Rubio',
          'imagen' => '',
          'dni' => 39576023,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response3 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task2);
        //me aseguro de que se creó

        $response3->assertStatus(200);

        $paulina = User::where('user_name','=','Paulina')->first();
        $roles[]=2;
        $datos = [
          'id_usuario' => $paulina->id ,
          'nombre' =>'Paulina Rubio',
          'user_name' => 'Paulina' ,
          'email' => 'paulina3@outlook.com',

        ];

        $response2 = $this->actingAs($paulina)
                          ->withSession(['user' => $paulina])
                          ->json('POST','configCuenta/modificarDatos', $datos);

        $response2->assertStatus(200);

        $this->assertEquals($response2->getData()->id, $paulina->id);
        $this->assertEquals($response2->getData()->user_name, $paulina->user_name);
        $this->assertEquals($response2->getData()->dni, $paulina->dni);
        $this->assertEquals($response2->getData()->name, $paulina->name);
        $this->assertEquals($response2->getData()->email, $paulina->email);
    }

    // /** @test */
    public function crear_exitosamente_un_usuario_configurar_user_name_correctamente()
    {
        $user = factory(User::class)->create();
        $user->assignRole('SUPERUSUARIO');
        $roles = array();
        $roles[] = 1;

        $task2 = [
          'usuario' => 'Paulina' ,
          'email' => 'meme2@meme.com',
          'nombre' =>'Paulina Rubio',
          'imagen' => '',
          'dni' => 39576023,
          'casinos' => $roles,
          'roles' => $roles,
        ];


        $response3 = $this->actingAs($user)
                         ->withSession(['user' => $user])
                         ->json('POST', 'usuarios/guardarUsuario', $task2);
        //me aseguro de que se creó

        $response3->assertStatus(200);

        $paulina = User::where('user_name','=','Paulina')->first();
        $roles[]=2;
        $datos = [
          'id_usuario' => $paulina->id ,
          'nombre' =>'Paulina Rubio',
          'user_name' => 'Pau' ,
          'email' => 'meme2@meme.com',

        ];

        $response2 = $this->actingAs($paulina)
                          ->withSession(['user' => $paulina])
                          ->json('POST','configCuenta/modificarDatos', $datos);

        $response2->assertStatus(200);

        $this->assertEquals($response2->getData()->id, $paulina->id);
        $this->assertEquals($response2->getData()->user_name, $paulina->user_name);
        $this->assertEquals($response2->getData()->dni, $paulina->dni);
        $this->assertEquals($response2->getData()->name, $paulina->name);
        $this->assertEquals($response2->getData()->email, $paulina->email);
    }
  
}
