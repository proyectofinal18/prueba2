@extends('layouts.dashboard')
@section('estilos')
  <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
  <link rel="stylesheet" href="css/paginacion.css">
@endsection
@section('headerLogo')
<span class="etiquetaLogoUsuarios">@svg('usuario','iconoUsuarios')</span>
@endsection
@section('contenidoVista')

        <div id="page-wrapper">
          <div class="container-fluid">
                <div class="row"> <!-- Tarjeta de FILTROS -->
                  <div class="col-md-12">

                      <div class="panel panel-default">
                        <div class="panel-heading" data-toggle="collapse" href="#collapseFiltros" style="cursor: pointer">
                          <h4>Filtros de búsqueda <i class="fa fa-fw fa-angle-down"></i></h4>
                        </div>
                        <div id="collapseFiltros" class="panel-collapse collapse">
                          <div class="panel-body">

                            <div class="row">
                              <div class="col-xs-4">
                                <h5>Usuario</h5>
                                <input id="B_usuario" type="text" class="form-control" placeholder="Usuario">
                              </div>
                              <div class="col-xs-4">
                                <h5>Tabla</h5>
                                <input id="B_tabla" type="text" class="form-control" placeholder="Tabla">
                              </div>
                              <div class="col-xs-4">
                                <h5>Acción</h5>
                                <input id="B_accion" type="text" class="form-control" placeholder="Acción">
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-4">
                                <h5>Fecha</h5>
                                <div class='input-group date' id='dtpFecha'>
                                  <input type='text' class="form-control" id="B_fecha" placeholder="aaaa-mm-dd"/>
                                  <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                                </div>
                              </div>
                              <div class="col-xs-4">
                                <h5>Casino</h5>
                                <select class="form-control" name="" id="buscadorCasino" >
                                  <option value="0" selected>- Todos los Casinos -</option>
                                  @foreach ($casinos as $cas)
                                  <option value="{{$cas->id_casino}}">{{$cas->nombre}}</option>
                                  @endforeach
                                </select>
                              </div>
                            <div class="col-xs-4">
                              <br>
                              <br>
                              <center><button id="btn-buscar" class="btn btn-infoBuscar" type="button" name="button"><i class="fa fa-fw fa-search"></i> BUSCAR</button></center>
                            </div>
                            </div>
                            <br>

                          </div>
                        </div>

                      </div>

                  </div>
                </div> <!-- / Tarjeta FILTROS -->

                <div class="row"> <!-- Tarjeta TABLA log Actividades -->
                  <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4>REGISTROS DE ACTIVIDADES DEL SISTEMA</h4>
                      </div>
                      <div class="panel-body">
                        <table id="tablaResultados" class="table table-fixed tablesorter">
                          <thead>
                            <tr>
                              <th class="col-xs-2" value="usuario.nombre" estado="" style="text-align:center !important">USUARIO <i class="fas fa-sort"></i></th>
                              <th class="col-xs-2 " value="fecha" estado="desc" style="text-align:center !important">FECHA <i class="fas fa-sort-down"></i></th>
                              <th class="col-xs-2 cabeceraOculta" value="accion" estado="" style="text-align:center !important">ACTIVIDAD <i class="fas fa-sort"></i></th>
                              <th class="col-xs-2" value="tabla" estado="" style="text-align:center !important">TABLA <i class="fas fa-sort"></i></th>
                              <th class="col-xs-2 cabeceraOculta" value="id_casino" estado="" style="text-align:center !important">CASINO <i class="fas fa-sort"></i></th>
                              <th class="col-xs-2">ACCIÓN</th>
                            </tr>
                          </thead>
                          <tbody id="cuerpoTabla" >

                          </tbody>
                        </table>
                        <!--Comienzo indices paginacion-->
                        <div id="herramientasPaginacion" class="row zonaPaginacion"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> <!-- / Tarjeta TABLA -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Modal Log Actividad -->
    <div class="modal fade" id="modalLogActividad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
             <div class="modal-content">
               <div class="modal-header"  style="font-family: Roboto-Black; background-color: #0D47A1; color: #fff">
                 <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                 <button id="btn-minimizar" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsado" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
                 <h3 class="modal-title modalVerMas" id="myModalLabel">DETALLES</h3>
                </div>

                <div class="modal-body" style="font-family: Roboto; color: #aaa;">
                 <div  id="colapsado" class="collapse in">
                  <form id="frmLog" name="frmLog" class="form-horizontal" novalidate="">

                      <div class="row">
                          <div class="col-xs-6">
                              <h6 class="list-group-item"  style="font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">FECHA</h6>
                              <h6 class="list-group-item" style="text-align:center !important; margin-top:0px !important;color:#757575; font-size:14px !important" id="fecha"></h6>
                              <br>
                            </div>
                            <div class="col-xs-6">
                              <h6 class="list-group-item"  style="font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">USUARIO</h6>
                              <h6 class="list-group-item" style="text-align:center !important; margin-top:0px !important;color:#757575; font-size:14px !important" id="usuario"></h6>
                              <br>
                            </div>
                          </div>

                      <div class="row">
                        <div class="col-xs-4">
                          <h6 class="list-group-item"  style="font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">ACCIÓN</h6>
                          <h6 class="list-group-item" style="text-align:center !important; margin-top:0px !important;color:#757575; font-size:14px !important" id="accion"></h6>
                          <br>
                        </div>
                        <div class="col-xs-4">
                          <h6 class="list-group-item"  style="font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">TABLA</h6>
                          <h6 class="list-group-item" style="text-align:center !important; margin-top:0px !important;color:#757575; font-size:14px !important" id="tabla"></h6>
                          <br>
                        </div>
                        <div class="col-xs-4">
                          <h6 class="list-group-item"  style="font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">CASINO</h6>
                          <h6 class="list-group-item" style="text-align:center !important; margin-top:0px !important;color:#757575; font-size:14px !important" id="id_entidad"></h6>
                          <br>
                        </div>
                      </div>

                      <br>
                      <div class="row"> <!-- Tarjeta TABLA log Actividades -->
                        <div class="col-xs-12">
                              <table id="tablaDetalleLog" class="table table-hover">
                                <thead>
                                  <tr>
                                    <th class="col-xs-4" style="padding-bottom:0px !important"> <h6 style="color:#757575 !important; font-size:16px !important">CAMPO</h6></th>
                                    <th class="col-xs-4" style="padding-bottom:0px !important"><h6 style="color:#757575 !important; font-size:16px !important">VALOR</h6></th>
                                    <th class="col-xs-4" style="padding-bottom:0px !important"><h6 style="color:#757575 !important; font-size:16px !important">VALOR ANTERIOR</h6></th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                              </table>
                        </div>
                      </div> <!-- / Tarjeta TABLA -->

                  </form>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">SALIR</button>
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>

    <meta name="_token" content="{!! csrf_token() !!}" />

    @endsection

    <!-- Comienza modal de ayuda -->
    @section('tituloDeAyuda')
    <h3 class="modal-title" style="color: #fff;">| AYUDA LOG DE ACTIVIDADES</h3>
    @endsection
    @section('contenidoAyuda')
    <div class="col-md-12">
      <h5>Tarjeta de logs</h5>
      <p>
        Informe detallado que muestra las últimas tareas o acciones realizadas por los usuarios dentro del sistema.
        Estan clasificadas de acuerdo a la actividad, fecha y tabla en la que fue producida.
      </p>
    </div>
    @endsection
    <!-- Termina modal de ayuda -->

    @section('scripts')
    <!-- JavaScript paginacion -->
    <script src="js/paginacion.js" charset="utf-8"></script>
    <!-- JavaScript personalizado -->
    <script src="js/LogActividades/seccionLogActividades.js" charset="utf-8"></script>
    <!-- DateTimePicker JavaScript -->
    <script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.es.js" charset="UTF-8"></script>

    @endsection
