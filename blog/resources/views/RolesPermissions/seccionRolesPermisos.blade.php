
@extends('layouts.dashboard')

@section('estilos')
  <link href="/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
  <link href="/themes/explorer/theme.css" media="all" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="/css/lista-datos.css">
@endsection

@section('contenidoVista')
<div class="col-lg-12 tab_content" id="pant_roles" hidden="true">
  <div class="row">
    <div class="col-md-12">
      <a href="" id="btn-add-rol" style="text-decoration: none;">
        <div class="panel panel-default panelBotonNuevo">
          <center><img class="imgNuevo" src="/img/logos/roles_white.png"><center>
            <div class="backgroundNuevo"></div>
            <div class="row">
              <div class="col-xs-12">
                <center>
                  <h5 class="txtLogo">+</h5>
                  <h4 class="txtNuevo" style="font-family:'Arial'; font-size:16px; color:#EF5350;" >NUEVO ROL</h4>
                </center>
              </div>
            </div>
          </div>
        </a>
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" href="#collapseFiltrosRoles" style="cursor: pointer">
              <h4 style="font-size:16px;">Filtros de Búsqueda  <i class="fa fa-fw fa-angle-down"></i></h4>
            </div>
            <div id="collapseFiltrosRoles" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                    <h5>Rol</h5>
                    <input id="buscadorRol" class="form-control" placeholder="Rol">
                  </div>
                  <div class="col-md-6">
                    <button id="buscarRol" class="btn btn-infoBuscar" type="button" style="margin-top:30px !important" name="button"><i class="fa fa-fw fa-search"></i> BUSCAR</button>

                  </div>
                </div>
                <br>
              </div>
            </div>
          </div>
          <div class="row"> <!-- fila de TABLA ROLES -->
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 id="tituloBusquedaRoles" style="font-size:16px;">Roles</h4>
                </div>
                <div class="panel-body">
                  <table id="tablaRoles" class="table table-fixed tablesorter">
                    <thead>
                      <tr>
                        <th class="col-xs-7">NOMBRE  <i class="fa fa-sort"></i></th>
                        <th class="col-xs-5">ACCIONES</th>
                      </tr>
                    </thead>
                    <tbody id="cuerpoTablaRoles" style="height: 220px;">
                      @foreach($roles as $rol)
                      <tr id="{{$rol->id}}">
                        <td class="col-xs-7"> {{$rol->name}}</td>
                        <td class="col-xs-5"><!-- ENLACE DE BOTON MODAL background: #ff9d2d;-->
                          <button class="btn btn-info detalleRol" value="{{$rol->id}}"><i class="fa fa-fw fa-search-plus"></i></button>
                          <button type="button" class="btn btn-warning modificarRol" value="{{$rol->id}}"><i class="fas fa-fw fa-pencil-alt"></i></button>
                          <button type="button" class="btn btn-danger openEliminarRol"  value="{{$rol->id}}"><i class="fa fa-fw fa-trash"></i></button>
                        </td>
                      </tr>
                      @endforeach

                    </tbody>
                  </table>
                  <div class="table-responsive" id="mostrarFRol"style="display:none">
                  <table  class="table">
                      <tr id="moldeRolFila" class="filaClone" style="display:none">
                        <td class="col-xs-7 nombreRolTabla" ></td>
                        <td class="col-xs-5" >
                          <button type="button" class="btn btn-infoBuscar detalleRol" value=""><i class="fa fa-fw fa-search-plus"></i></button>
                          <button type="button" class="btn btn-warningModificar modificarRol" value=""><i class="fas fa-fw fa-pencil-alt"></i></button>
                          <button type="button" class="btn btn-dangerEliminar openEliminarRol" value=""><i class="fa fa-fw fa-trash"></i></button>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

</div>


<div class="col-md-12 tab_content" id="pant_permisos" hidden="true">
  <div class="row">
      <div class="col-md-12">
       <a href="" id="btn-add-permiso" style="text-decoration: none;">
        <div class="panel panel-default panelBotonNuevo">
            <center><img class="imgNuevo" src="/img/logos/permisos_white.png"><center>
            <div class="backgroundNuevo"></div>
            <div class="row">
                <div class="col-xs-12">
                  <center>
                      <h5 class="txtLogo">+</h5>
                      <h4 class="txtNuevo" style="font-family:'Arial'; font-size:16px; color:#EF5350;">NUEVO PERMISO</h4>
                  </center>
                </div>
            </div>
        </div>
       </a>
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" href="#collapseFiltrosPermisos" style="cursor: pointer">
          <h4 style="font-size:16px;"id="tituloBusquedaPermisos">Filtros de Búsqueda  <i class="fa fa-fw fa-angle-down"></i></h4>
        </div>
        <div id="collapseFiltrosPermisos" class="panel-collapse collapse">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-6">
                <h5>Permiso</h5>
                <input id="buscadorPermiso" class="form-control" placeholder="Permiso">
              </div>
              <div class="col-md-6">
                <h5 style="font-family:'Arial';  background-color:#FFFFFF;color:#FFFFFF; font-size:16px;">boton buscar</h5>
                <button id="buscarPermiso" class="btn btn-infoBuscar" type="button" name="button"><i class="fa fa-fw fa-search"></i> BUSCAR</button>
              </div>
            </div>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 style="font-size:16px;">Permisos</h4>
        </div>
        <div class="panel-body">
          <table id="tablaPermisos" class="table table-fixed tablesorter">
            <thead>
              <tr>
                <th class="col-xs-5">PERMISO</th>
                <th class="col-xs-4">SECCIÓN <i class="fa fa-sort"></i></th>
                <th class="col-xs-3">ACCIONES</th>
              </tr>
            </thead>
            <tbody id="cuerpoTablaPermisos" style="max-height: 220px;">
              @foreach($permisos as $permiso)
              <tr id="{{$permiso->id}}">
                <td class="col-xs-5">{{$permiso->name}}</td>
                <td class="col-xs-4">{{$permiso->nombre_seccion}}</td>
                <td class="col-xs-3">
                  <button class="btn btn-info detallePermiso" value="{{$permiso->id}}"><i class="fa fa-fw fa-search-plus"></i></button>
                  <button class="btn btn-danger openEliminar"  value="{{$permiso->id}}"><i class="fa fa-fw fa-trash"></i></button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          <div class="table-responsive" id="mostrarFPermiso"style="display:none">
          <table  class="table">
              <tr id="moldePermisos" class="filaClone" style="display:none">
                <td class="col-xs-5 perm_name" ></td>
                <td class="col-xs-4 perm_seccion" ></td>

                <td class="col-xs-3" style="text-align:center !important;">
                  <button type="button" class="btn btn-infoBuscar detallePermiso" value=""><i class="fa fa-fw fa-search-plus"></i></button>
                  <button type="button" class="btn btn-dangerEliminar openEliminar" value=""><i class="fa fa-fw fa-trash"></i></button>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- MODAL CREAR ROL -->
 <div class="modal fade" id="myModalRol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
       <div class="modal-dialog modal-lg" style="width:60% !important;">
          <div class="modal-content">
             <div class="modal-header" style="font-family: Roboto-Black; background-color: #6dc7be; color: #fff">
               <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
               <button id="btn-minimizar" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsado" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
               <h3 class="modal-title">| NUEVO ROL</h3>
             </div>
     <form id="frmRol" name="frmRol">
       <div id="colapsado" class="collapse in">
             <div class="modal-body" >
               <div class="row" id="desplegableCarga">
                 <div class="row" >
                     <div class="col-md-6">
                         <h5 style="font-family:'Arial'; font-size:16px; color:#EF5350;" >Nombre del Rol</h5>
                         <input type="text" class="form-control" id="comment" placeholder="Nombre Rol">
                    </div>
                 </div><br>

                 <div class="row" id="aeliminar">

                 </div>
               </div>

             </div>
             <div id="mensajeErrorRol" hidden>
               <br>
               <span style="font-family:'Roboto-Black'; font-size:16px; color:#EF5350;">ERROR</span>
               <br>
               <span style="font-family:'Roboto-Regular'; font-size:16px; color:#555;">Debe asignarle al menos un permiso a este rol.</span>
             </div> <!-- mensaje -->
             <div class="modal-footer">
               <button type="button" class="btn btn-successAceptar" id="btn-save-rol" value="add">ACEPTAR</button>
               <button id='boton-cancelar' type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
               <!-- <button id='boton-salir' type="button" class="btn btn-default" data-dismiss="modal" style="display: none;">SALIR</button> -->
               <input type="hidden" id="id_rol" name="id_rol" value="0">
             </div>
           </div>
         </form>
        </div>
       </div>
     </div>


<div class="row" >

       <div id="contenedorSecciones"  class="panel panel-default col-xs-8" style="display:none !important;">
          <div class="panel-heading" id="headingP" data-toggle="collapse"  style="cursor: pointer; background-color:#ccc !important;">
            <h4 id="nombreSeccion2"><i class="fa fa-fw fa-angle-down"></i></h4>
          </div>

          <div class="panel-collapse">
            <div class="panel-body">
              <table class="table-responsive">
                <tbody  id="bodyNew">
                </tbody>
              </table>
            </div>
          </div>
        </div>
  </div>

<!-- modificar -->
<div class="modal fade" id="modalModificarRol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" style="width:60% !important;">
    <div class="modal-content">
      <div class="modal-header" style="font-family: Roboto-Black; background-color: #FFB74D; color: #fff">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <button id="btn-minimizar" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsado" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
        <h3 class="modal-title">| MODIFICAR ROL</h3>
      </div>
      <div id="colapsadoModif" class="collapse in">
        <div class="modal-body">
          <div class="row iconoCargando" >

          </div>
          <div class="row" id="desplegableRolMod">
            <div class="row">
              <div class="col-md-6">
                <h5 style="font-family:'Arial'; font-size:16px; color:#EF5350;" >Nombre del Rol</h5>
                <input type="text" class="form-control" id="nombreRolModif" placeholder="Nombre Rol">
              </div>
            </div>
            <br>
            <div class="row" id="ggg">

           </div>
          </div>
        </div>
        <div id="mensajeErrorRol" hidden>
          <span style="font-family:'Roboto-Black'; font-size:16px; color:#EF5350;">ERROR</span>
          <br>
          <span style="font-family:'Roboto-Regular'; font-size:16px; color:#555;">Debe asignarle al menos un permiso a este rol.</span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warningModificar" id="btn-modificar-rol"  >GUARDAR</button>
          <button id='boton-cancelar-mod' type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
          <input type="hidden" id="id_rol_modif" name="id_rol_modif" value="0">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div id="contenedorSeccionesModif"  class="panel panel-default col-xs-8 moldeContenedor" style="display:none !important;">
     <div class="panel-heading" id="headingPMod" data-toggle="collapse"  style="cursor: pointer; background-color:#ccc !important;">
       <h4 id="nombreSeccionmodif"><i class="fa fa-fw fa-angle-down"></i></h4>
     </div>

     <div class="panel-collapse">
       <div class="panel-body">
         <table class="table-responsive">
           <tbody  id="bodyModificar">
           </tbody>
         </table>
       </div>
     </div>
   </div>
</div>


<!-- DETALLES DE ROL -->
<div class="modal fade" id="modalDetalleRol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg" style="width:45%">
         <div class="modal-content">
            <div class="modal-header" style="font-family: Roboto-Black; background-color: #0D47A1; color: #fff">
              <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
              <button id="btn-minimizar" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsado" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
              <h3 class="modal-title">| DETALLE ROL</h3>
            </div>
          <div id="colapsado" class="collapse in">
            <div class="modal-body">
                  <div class="row">
                    <div class="col-xs-12">
                      <h6 class="list-group-item"  style="font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">NOMBRE DE ROL</h6>
                      <h6 class="list-group-item nombreRol" style="text-align:center !important; margin-top:0px !important; font-size:14px !important" readonly="true" ></h6>
                      <br>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-xs-6">
                      <h6 class="list-group-item"  style="font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">USUARIOS</h6>
                      <h6 class="list-group-item " style=" margin-top:0px !important; font-size:14px !important" readonly="true" id="users"></h6>
                      <br>
                    </div>
                    <div class="col-xs-6">
                      <h6 class="list-group-item"  style="font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">PERMISOS ASOCIADOS</h6>
                      <h6 class="list-group-item " style=" margin-top:0px !important; font-size:14px !important" readonly="true" id="listaPermisosAsoc"></h6>
                      <br>
                    </div>
                  </div>
            </div>
            <div class="modal-footer">
              <button id='boton-cancelar' type="button" class="btn btn-default" data-dismiss="modal">SALIR</button>
              <!-- <button id='boton-salir' type="button" class="btn btn-default" data-dismiss="modal" style="display: none;">SALIR</button> -->
              <input type="hidden" id="id_rol" name="id_rol" value="0">
            </div>
          </div>
        </form>
       </div>
      </div>
    </div>

<!-- NUEVO PERMISO -->
<div class="modal fade" id="myModalPermisos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
     <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="font-family: Roboto-Black; background-color: #6dc7be; color: #fff">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <h3 class="modal-title">| ALTA PERMISO</h3>
         </div>
        <form id="frmPermisos" name="frmPermisos">
         <div id="colapsadoP" class="collapse in">
          <div class="modal-body">
            <div class="row">
              <div class="col-xs-6">
                <h5>Nombre del Permiso</h5>
                <input type="text" class="form-control" id="commentPermiso" placeholder="Nombre del Permiso">
              </div>
              <div class="col-xs-6">
                <h5>Sección</h5>
                <select class="" id="selectSecciones" name="">
                </select>
              </div>
            </div>
            <div class="row">
              <div id="conteinerRoles" class="col-xs-6">
                <br><h5>Roles Asociados</h5>
                <table class="table-responsive rolesAsoc">
                  <tbody style="padding:0px !important">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </form>
      <br>
      <div id="mensajeErrorPermiso" hidden>
        <br>
         <span style="font-family:'Roboto-Black'; font-size:16px; color:#EF5350;">ERROR</span>
         <br>
         <span style="font-family:'Roboto-Regular'; font-size:16px; color:#555;">-- --</span>
      </div> <!-- mensaje -->

      <div class="modal-footer">
        <button type="button" class="btn btn-successAceptar" id="btn-save-permiso" value="add">CREAR</button>
        <input type="hidden" id="id_permiso" name="id_permiso" value="0">
        <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
      </div>
   </div>
 </div>
</div>


 <!-- DETALLES DE PERMISOS -->

 <div class="modal fade" id="modalDetallePermiso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" style="font-style:normal;">
     <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header modalNuevo" style="font-family: Roboto-Black; background-color: #0D47A1; color: #fff">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <button id="btn-minimizarPermisos" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsadoP" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
            <h3 class="modal-title">| DETALLE PERMISO</h3>
         </div>
        <form id="frmPermisos" name="frmPermisos">
         <div id="colapsadoP" class="collapse in">
          <div class="modal-body">
                 <div class="row">
                   <div class="col-xs-6">
                     <h6 class="list-group-item"  style="font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">NOMBRE</h6>
                     <h6 class="list-group-item" style="text-align:center !important; margin-top:0px !important; font-size:14px !important" id="nombrePermiso"></h6>
                     <br>
                   </div>
                   <div class="col-xs-6">
                     <h6 class="list-group-item"  style="font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">SECCIÓN</h6>
                     <h6 class="list-group-item" style="text-align:center !important; margin-top:0px !important; font-size:14px !important" id="nombreSeccion"></h6>
                     <br>
                   </div>
                  </div>
                 <div class="row" style="text-align:center !important">
                   <div class="col-xs-12" >
                     <h6 class="list-group-item"  style=" font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">ROLES ASOCIADOS</h6>
                     <div class="list-group" style="font-size:14px !important; text-align:center !important" id="contenedorCasinoVer">
                       @foreach($roles as $rol)
                       <div class="checkbox" style="text-align:left !important; margin-left:200px !important"> <label><input id="perm{{$rol->id}}" type="checkbox" value="{{$rol->id}}" style="text-align:left !important">{{$rol->name}}</label></div>
                       @endforeach
                     </div>
                   </div>


                 </div>

                   </div>
                 </div>
               </form>
                 <br>
         <div class="modal-footer">
           <input type="hidden" id="id_permiso" name="id_permiso" value="0">
           <button id='cancelar_permiso' type="button" class="btn btn-default" data-dismiss="modal">SALIR</button>
         </div>
     </div>
   </div>
 </div>

<!-- modificar permiso -->
<!-- <div class="modal fade" id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" style="width:60%" >
    <div class="modal-content">
      <div class="modal-header" style="font-family: Roboto-Black; background-color: #FFB74D; color: #fff">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <button id="btn-minimizarPermisos" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsadoP" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
        <h3 class="modal-title">| MODIFICAR PERMISO</h3>
      </div>
      <div class="collapse in">
        <div class="modal-body">
          <div class="row">
            <div class="col-xs-6">
              <h5>NOMBRE</h5>
              <input id="mod_permiso" class="form-control"></input>
            </div>
            <div class="col-xs-6">
              <h5>Sección</h5>
              <select class="form-control" id="modif_secc_perm" name="seccionPerm">
              </select>
            </div>
          </div>
          <div class="row">
            <div id="roles_modif" class="col-md-6">
              <br><h5 readonly="true" style="font-family:'Arial'; font-size:16px; color:#EF5350;" >ROLES ASOCIADOS</h5>
              @foreach($roles as $rol)
              <div class="checkbox"> <label><input id="pmod{{$rol->id}}" type="checkbox" value="{{$rol->id}}" >{{$rol->name}}</label></div>
              @endforeach
            </div>
          </div>
        </div>
        <br>
        <div class="modal-footer">
           <button type="button" class="btn btn-warningModificar" id="btn-modificar-permiso" >MODIFICAR</button>
           <button  type="button" class="btn btn-default" data-dismiss="modal">SALIR</button>
           <input type="hidden" id="id_permiso_modif" name="id_permiso" value="0">
         </div>
         <div id="mensajeErrorPermisoModif" hidden>
             <br>
            <span style="font-family:'Roboto-Black'; font-size:16px; color:#EF5350;">ERROR</span>
            <br>
            <span style="font-family:'Roboto-Regular'; font-size:16px; color:#555;" id="msjmodif"></span>
        </div>
     </div>
   </div>
 </div>
</div> -->


 <!-- MODAL ELIMINAR -->
 <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
     <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header" style="font-family: Roboto-Black; background-color: #EF5350;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h3 class="modal-titleEliminar" id="myModalLabel" style="color:white;">| ALERTA</h3>
         </div>

         <div class="modal-body" style="color:#fff; background-color:#FFFFF;">
             <!-- <form id="frmEliminar" name="frmEliminar" class="form-horizontal" novalidate=""> -->

                 <!-- Si no anda falta el <fieldset> -->

               <h6  style="color:#000000">¿Está seguro que desea eliminar este Permiso?</h6>
                 <div class="row">
                   <div class="col-md-8 col-md-offset-2">

                     <div class="row">
                         <div class="col-md-12">
                             <h6  style="color:#000000">Roles que lo tienen asociado:</h6>
                             <ul id="lista2" style="color:#000000">

                             </ul>
                         </div>
                     </div>
                   </div>
                 </div>
                 <br>

           <!-- </form> -->
         </div>

         <div class="modal-footer">
           <button type="button" class="btn btn-dangerEliminar" id="btn-eliminar-permiso" value="">ELIMINAR</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
         </div>
     </div>
   </div>
 </div>

 <!-- MODAL ELIMINAR -->
 <div class="modal fade" id="modalEliminarRol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
     <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header" style="font-family: Roboto-Black; background-color: #EF5350;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h3 class="modal-titleEliminar" id="myModalLabel" style="color:white;">| ALERTA</h3>
         </div>

         <div class="modal-body" style="color:#fff; background-color:#FFFFF;">
           <div class="modal-body" style="color:#fff; background-color:#FFFFF;">

                 <h6 style="color:#000000 !important; font-size:17px !important;">¿ESTA SEGURO QUE DESEA ELIMINAR ESTE ROL?</h6>
                 <br>
                 <h6 id="msjeliminarRol" style="color:black !important;font-size:16px;"></h6>

           </div>

                 <br>

           <!-- </form> -->
         </div>

         <div class="modal-footer">
           <button type="button" class="btn btn-dangerEliminar" id="btn-eliminar" value="">ELIMINAR</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
         </div>
     </div>
   </div>
 </div>



 </body>

@endsection

<!-- Comienza modal de ayuda -->
@section('tituloDeAyuda')
<h3 class="modal-title" style="color: #fff;">| AYUDA ROLES Y PERMISOS</h3>
@endsection
@section('contenidoAyuda')
<div class="col-md-12">
  <h5>Tarjetas de roles</h5>
  <p>
    Administra roles dentro del sistema que permitirán asociar a cada usuario, con el que determinarán sus tareas correspondientes.
    Están relacionadas a permisos con los que se definen dichos roles.
  </p>
</div>
<div class="col-md-12">
  <h5>Tarjetas de permisos</h5>
  <p>
    Cada permiso asociado a un rol es implementado para que cada vista sea filtrada para accesos a las tareas de usuarios asignados.
  </p>
</div>

@endsection
<!-- Termina modal de ayuda -->

@section('scripts')

 <script src="/js/fileinput.min.js" type="text/javascript"></script>
 <script src="/js/locales/es.js" type="text/javascript"></script>
 <script src="/themes/explorer/theme.js" type="text/javascript"></script>

 <script src="js/inputSpinner.js" type="text/javascript"></script>
 <script src="/js/lista-datos.js" type="text/javascript"></script>

<script src="js/RolesPermissions/rolesPermisos.js"></script>
<script src="js/RolesPermissions/permisos.js"></script>

<script type="text/javascript">

  $('.pop').popover({
    html:true
  });

</script>
@endsection
