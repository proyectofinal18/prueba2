 <?php
use App\Http\Controllers\Usuarios\BuscarUsuariosController;
use Illuminate\Http\Request;

 $usuario = Auth::user();
 $id_usuario = $usuario->id;
 $cas = $usuario->casinos;
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!! csrf_token() !!}"/>

    <!-- favicon, logo de la pág web -->
    <link rel="icon" type="image/png" sizes="32x32" href="/img/logos/favicon.png">
    <title>CAS - Lotería de Santa Fe</title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap.min.css.map" rel="stylesheet">
    <link href="/css/bootstrap-col-xl.css" rel="stylesheet">

    <link href="/css/estilosBotones.css" rel="stylesheet">
    <link href="/css/indexCss.css" rel="stylesheet">
    <link href="/css/estilosModal.css" rel="stylesheet">
    <link href="/css/estilosFileInput.css" rel="stylesheet">
    <link href="/css/estilosPopUp.css" rel="stylesheet">
    <link href="/css/table-fixed.css" rel="stylesheet">
    <link href="/css/importacionFuentes.css" rel="stylesheet">
    <link href="/css/tarjetasMenues.css" rel="stylesheet">
    <link href="/css/flaticon.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/css/style.css">

    <link rel="stylesheet" type="text/css" href="/css/component.css" />

    <!-- Custom Fonts -->
    <!-- <link rel="stylesheet" type="text/css" href="/font-awesome/css/font-awesome.min.css" > -->

    <!-- Animaciones de los LINKS en MENU -->
    <link rel="stylesheet" href="/css/animacionesMenu.css">

    <!-- Animaciones de alta -->
    <link rel="stylesheet" href="/css/animacionesAlta.css">

    <!-- Animación de carga de datos -->
    <link rel="stylesheet" href="/css/loadingAnimation.css">

    <!-- Mesaje de notificación -->
    <link rel="stylesheet" href="/css/mensajeExito.css">
    <link rel="stylesheet" href="/css/mensajeError.css">

    <!-- Estilos de imagenes en SVG -->
    <link rel="stylesheet" href="/css/estilosSVG.css">
    <link rel="stylesheet" href="/css/estiloDashboard.css">
    <link rel="stylesheet" href="/css/estiloDashboard_xs.css">

    <!-- Custom Fonts -->
    <link rel="stylesheet" href="/web-fonts-with-css/css/fontawesome-all.css">

    <link rel="stylesheet" href="/css/perfect-scrollbar.css">


    <link href="/css/tab_style.css" media="all" rel="stylesheet" type="text/css"/>
    @section('estilos')
    @show

  </head>
  <body>

    <!-- Contenedor de toda la página -->
    <div class="contenedor">

        <!-- Barra superior  -->
        <header>
            <nav id="id_nav">@section('headerLogo')
                 @show

              <h2 class="tituloSeccionPantalla"></h2>
              <ul class="nav nav-tabs nav-justified juegosSec" id="juegosSec" style=" width:80%;" hidden="true">
                <li id="b_juego" ><a href="#pant_juegos"  style="font-family:Roboto-condensed;font-size:20px; ">Juegos</a></li>
                <li id="b_sector"><a href="#pant_sectores"  style="font-family:Roboto-condensed;font-size:20px;">Sectores</a></li>
             </ul>

             <ul class="nav nav-tabs nav-justified cierreApertura" id="cierreApertura" style=" width:80%;" hidden="true">
               <li id="b_apertura" ><a href="#pant_aperturas"  style="font-family:Roboto-condensed;font-size:20px; ">Aperturas</a></li>
               <li id="b_cierre"><a href="#pant_cierres"  style="font-family:Roboto-condensed;font-size:20px;">Cierres</a></li>
            </ul>

            <ul class="nav nav-tabs nav-justified pestCanon" id="pestCanon" style=" width:80%;" hidden="true">
              <li id="canon1" ><a href="#pant_canon_pagos"  style="font-family:Roboto-condensed;font-size:20px; ">Detalles Canon y Pagos</a></li>
              <li id="canon2"><a href="#pant_canon_valores"  style="font-family:Roboto-condensed;font-size:20px;">Actualización Valores</a></li>
            </ul>

             <ul class="nav nav-tabs nav-justified rolesPermisos" id="rolesPermisos" style=" width:80%;" hidden="true">
               <li id="rolespest" ><a href="#pant_roles"  style="font-family:Roboto-condensed;font-size:20px; ">Gestión de Roles</a></li>
               <li id="permisospest"><a href="#pant_permisos"  style="font-family:Roboto-condensed;font-size:20px;">Gestión de Permisos</a></li>
            </ul>

            <ul class="nav nav-tabs nav-justified informesMes" id="informesMes" style=" width:80%;" hidden="true">
              <li id="gestInformes" ><a href="#gestionInfoMes"  style="font-family:Roboto-condensed;font-size:20px; ">Informes Mensuales</a></li>
              <li id="graficos"><a href="#graficosMes"  style="font-family:Roboto-condensed;font-size:20px;">Gráficos Mensuales</a></li>
           </ul>

           <ul class="nav nav-tabs nav-justified pestImportaciones" id="pestImportaciones" style=" width:80%;" hidden="true">
             <li id="imp_diaria" ><a href="#pest_diaria"  style="font-family:Roboto-condensed;font-size:20px; ">Importaciones Diarias</a></li>
             <li id="imp_mensual"><a href="#pest_mensual"  style="font-family:Roboto-condensed;font-size:20px;">Importaciones Mensuales</a></li>
          </ul>

            <a href="#" id="btn-ayuda" hidden="true">
                <i class="iconoAyuda glyphicon glyphicon-question-sign"
                   style="padding-top: 12px; padding-left: 10px; !important">
                </i>
              </a >
              <ul class="opcionesBarraSuperior">

                <?php
                  $usuario = Auth::user();
                ?>

                  <!-- aca iba todo lo de usuario y notificaciones -->
                  <li>
                    <a href="#" class="etiquetaLogoSalida"><img src="/img/logos/salida_negrita.png" style="margin-top:4px; margin-right: 32px; width: 17px;"></a>
                  </li>
              </ul>
              <div class="tab pestania" style="border-style:none !important;" hidden>

              </div>
            </nav>
        </header>

        <!-- Menú lateral -->
        <aside style="position:fixed !important;top:0px !important; height:100% !important;">
            <div class="contenedorLogo">
                <a onclick="window.location = window.location.protocol + '//' + window.location.host + '/home'"  href="#">
                  <img src="/img/logos/logo_brand_blanco.png" alt="" width="48%">
                </a>
            </div>
            <!-- <div class="scrollMenu"> -->

              <div class="contenedorMenu">
                <div class="contenedorUsuario">
                  <?php
                    $cas = $usuario->casinos[0]->id_casino;
                    if($cas == 1){
                      echo '<div class="fondoMEL"></div>';
                    }
                    else if($cas == 2){
                      echo '<div class="fondoSFE"></div>';
                    }
                    else if($cas == 3){
                      echo '<div class="fondoROS"></div>';
                    }
                  ?>
                  @can('Ver Sección Configuración de Cuenta')
                    <div class="infoUsuario">
                      <a onclick="window.location = window.location.protocol + '//' + window.location.host + '/configCuenta'" href="#">
                        <?php
                          $tieneImagen = BuscarUsuariosController::getInstancia()->tieneImagen();
                          if($tieneImagen) {
                            echo '<img id="img_perfilBarra" src="/usuarios/imagen" class="img-circle">';
                          }
                          else {
                            echo '<img id="img_perfilBarra" src="/img/img_user.jpg" class="img-circle">';
                          }
                        ?>
                        <i id="iconConfig" class="fa fa-cog"></i>
                      </a>
                        <h3>{{$usuario->user_name}}</h3>
                        <div class="nombreUsuario"><h4>{{'@'.$usuario->user_name}}</h4></div>
                    </div>
                    @endcan
                </div>

                <div class="opcionesMenu">

                    <!-- PRIMER NIVEL -->
                    <ul>
                        <li>
                            <div id="opcInicio" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/home'" style="cursor: pointer;">
                                <span class="icono" style="padding-bottom: 50px;">
                                  @svg('home','iconoHome')
                                </span>
                                <span>Inicio</span>
                            </div>
                        </li>
                        @can('Ver Sección Juegos y Sectores')
                      <div class="separadoresMenu" style="font-size:11px !important">GESTIÓN ADMINISTRATIVA</div>
                      @endcan
                      @can('Ver Sección Casinos')
                        <li>
                            <div id="opcCasino" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/casinos'" href="#" style="cursor: pointer;">
                                <span class="icono" style="padding-bottom: 56px;">
                                  @svg('casinos','iconoCasinos')
                                </span>
                                <span>Casinos</span>
                            </div>
                        </li>
                        @endcan
                        @can('Ver Sección Usuarios')
                        <li>
                            <div id="barraUsuarios" class="opcionesHover" data-target="#usuarios" data-toggle="collapse">
                                <span class="flechita">
                                  <i class="fa fa-angle-right"></i>
                                </span>
                                <span class="icono" style="padding-bottom: 50px;">
                                  @svg('usuario','iconoUsuarios')
                                </span>
                                <span>Usuarios</span>
                            </div>

                            <!-- SEGUNDO NIVEL -->
                            <ul class="subMenu1 collapse" id="usuarios">
                              <li>
                                <div id="opcGestionarUsuarios" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/usuarios'" href="#" style="cursor: pointer;">
                                  <span>Gestionar usuarios</span>
                                </div>
                              </li>

                              <li>
                                <div id="opcRolesPermisos" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/roles'" href="#" style="cursor: pointer;">
                                  <span>Roles y permisos</span>
                                </div>
                              </li>
                              @role('SUPERUSUARIO')
                              <li>
                                <div id="opcLogActividades" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/logActividades'" href="#" style="cursor: pointer;">
                                  <span>Log de actividades</span>
                                </div>
                              </li>
                              @endrole
                            </ul>
                        </li>
                        @endcan
                        @can('Ver Sección Juegos y Sectores')
                        <li>
                            <div id="barraJuegosSectores" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/juegos'" href="#" style="cursor: pointer;">

                                <span class="icono" style="padding-bottom: 56px;">
                                  @svg('svgs_mesas.ruleta_grid','iconoJuego')
                                </span>
                                <span >Juegos y Sectores</span>

                            </div>
                        </li>
                        @endcan
                        @can('Ver Sección Mesas')
                        <li>
                            <div id="barraGestionMesas" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/mesas-de-panio'" href="#" style="cursor: pointer;">

                                <span class="icono" style="padding-bottom: 56px;">
                                  @svg('mesa','iconoMesa')
                                </span>
                                <span>Mesas de Paño</span>

                            </div>
                        </li>
                        @endcan
                        @can('Ver Sección de Cierres y Aperturas')
                        <div class="separadoresMenu" style="font-size:11px !important">GESTIÓN DE RELEVAMIENTOS</div>
                        @endcan
                        @can('Ver Sección de Cierres y Aperturas')
                          <li>
                              <div id="barraCierresAperturas" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/aperturas'" href="#" style="cursor: pointer;">

                                  <span class="icono" style="padding-bottom: 56px;">
                                    @svg('svgs_mesas.rel_gris','iconoCierre')
                                  </span>
                                  <span>Cierres y Aperturas</span>

                              </div>
                          </li>
                          @endcan
                          @can('Ver Sección Apuestas Mínimas')
                          <li>
                              <div id="barraApuestas" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/apuestas'" href="#" style="cursor: pointer;">

                                  <span class="icono" style="padding-bottom: 56px;">
                                    @svg('svgs_mesas.fichas_gris','iconoMesa')
                                  </span>
                                  <span>Apuestas Mínimas</span>

                              </div>
                          </li>
                          @endcan
                          @can('Ver Sección Informes a Fiscalizadores')
                          <li>
                              <div id="barraInfoFisca" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/informeDiarioBasico'" href="#" style="cursor: pointer;">

                                <span class="icono" style="padding-bottom: 54px;">
                                  @svg('informes','iconoInformes')
                                </span>
                                  <span>Informes Diarios </span>

                              </div>
                          </li>
                          @endcan
                        @can('Ver Sección Informes Contables')
                        <div class="separadoresMenu" style="font-size:11px !important">GESTIÓN CONTABLE</div>
                        @endcan
                        @can('Ver Sección Importaciones')
                          <li>
                              <div id="barraImportaciones" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/importacionDiaria'" href="#" style="cursor: pointer;">

                                  <span class="icono" style="padding-bottom: 56px;">
                                    @svg('csv_bordes','iconoImportaciones')
                                  </span>
                                  <span>Importaciones</span>

                              </div>
                          </li>
                        @endcan
                        @can('Ver Sección Informes Contables')
                          <li>
                              <div id="barraInformes" class="opcionesHover" data-target="#informes" data-toggle="collapse" href="#">
                                <span class="flechita">
                                    <i class="fa fa-angle-right"></i>
                                  </span>
                                  <span class="icono" style="padding-bottom: 54px;">
                                    @svg('informes','iconoInformes')
                                  </span>
                                  <span>Informes</span>
                              </div>

                              <!-- SEGUNDO NIVEL -->
                              <ul class="subMenu1 collapse" id="informes">
                                <li>
                                  <div id="opcInfoDiario" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/informeDiario'" href="#" style="cursor: pointer;">
                                    <span>Diario</span>
                                  </div>
                                </li>
                                <li>
                                  <div id="opcInfoMensual" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/informeMensual'" href="#" style="cursor: pointer;">
                                    <span>Mensual</span>
                                  </div>
                                </li>

                                <li>
                                  <div id="opcInfoInteranuales" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/informeAnual'" href="#" style="cursor: pointer;">
                                    <span>Anuales</span>
                                  </div>
                                </li>
                              </ul>
                          </li>
                          @endcan
                          @can('Ver Sección Canon')
                          <li>
                            <div id="barraCanon" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/canon'" href="#" style="cursor: pointer;">

                                <span class="icono" style="padding-bottom: 56px;">
                                  @svg('bolsa_pesos','iconoCanon')
                                </span>
                                <span>Canon</span>

                            </div>
                          </li>
                          @endcan
                          @can('Ver Sección Imágenes Búnker')
                          <li>
                            <div id="barraImagenes" class="opcionesHover" onclick="window.location = window.location.protocol + '//' + window.location.host + '/solicitudImagenes'" href="#" style="cursor: pointer; padding-left:15px; ">
                              <span class="icono" >
                                @svg('camara','iconoCamara')
                              </span>
                              <span>Imágenes Bunker</span>
                            </div>
                          </li>
                          @endcan

                </div>
                <div class="bottomMenu"></div>
              </div> <!-- contenedorMenu -->
            <!-- </div> <! scrollMenu -->
        </aside>

        <!-- Vista de secciones -->
        <main class="contenedorVistaPrincipal">

          <section>
              <div class="container-fluid">
                @section('contenidoVista')
                @show

              </div>

          </section>
        </main>
              <!-- DESDE ACA -->

        <!-- NOTIFICACIÓN DE ÉXITO -->
          <!--  (*) Para que la animación solo MUESTRE (fije) el mensaje, se agrega la clase 'fijarMensaje' a #mensajeExito-->
          <!--  (*) Para que la animación MUESTRE Y OCULTE el mensaje, se quita la clase 'fijarMensaje' a #mensajeExito-->
          <!-- (**) si se quiere mostrar los botones de ACEPTAR o SALIR, se agrega la clase 'mostrarBotones' a #mensajeExito -->
          <!-- (**) para no mostrarlos, se quita la clase 'mostrarBotones' a #mensajeExito -->

        <div id="mensajeExito" class="" hidden>
            <div class="cabeceraMensaje"></div>
            <div class="iconoMensaje">
              <img src="/img/logos/check.png" alt="imagen_check" >
            </div>
            <div class="textoMensaje" >
                <h3>ÉXITO</h3>
                <p>El CASINO fue creado con éxito.</p>
            </div>
            <div class="botonesMensaje">
                <button class="btn btn-success confirmar" type="button" name="button">ACEPTAR</button>
                <button class="btn btn-default salir" type="button" name="button">SALIR</button>
            </div>
        </div>


        <!-- Modal Error -->
        <div id="mensajeError" hidden>
            <div class="cabeceraMensaje"></div>
            <div class="iconoMensaje">
              <img src="/img/logos/error.png" alt="imagen_error" >
            </div>
            <div class="textoMensaje" >
                <h3>ERROR</h3>
                <p>No es posible realizar la acción</p>
            </div>

        </div>

        <!-- Modal ayuda -->
        <div class="modal fade" id="modalAyuda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                 <div class="modal-content">
                   <div class="modal-header modalNuevo" style="background-color: #1976D2;">
                     <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> -->
                     <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                     <button id="btn-minimizar" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsado" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
                     @section('tituloDeAyuda')
                     @show
                    </div>
                    <div  id="colapsado" class="collapse in">
                    <div class="modal-body modalCuerpo">
                              <div class="row">
                                @section('contenidoAyuda')
                                @show
                              </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">SALIR</button>
                    </div>
                  </div>
                </div>
              </div>
        </div>

    </div>


    <!-- jQuery -->
    <script src="/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/js/bootstrap.js"></script>

    <!-- JavaScript ajaxError -->
    <script src="/js/ajaxError.js"></script>

    <!-- JavaScript personalizado -->
    <script src="/js/barraNavegacion.js"></script>

    <!-- JavaScript de tarjetas animadas -->
    <script src="/js/anime.min.js"></script>
    <script src="/js/main.js"></script>

    <!-- TableSorter -->
    <script type="text/javascript" src="/js/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="/js/iconosTableSorter.js"></script>

    <!-- Collapse JS | Controla el menú -->
    <script type="text/javascript" src="/js/collapse.js"></script>

    <!-- librerias de animate -->
    <script src="/js/createjs-2015.11.26.min.js"></script>
    <script src="/js/Animacion_logo2.js?1517927954849"></script>

    <script src="/js/perfect-scrollbar.js" charset="utf-8"></script>

    <script type="text/javascript">

        $(document).on('show.bs.collapse','.subMenu1',function(){
            $('.subMenu1').not($(this)).collapse('hide');
        });
        $(document).on('show.bs.collapse','.subMenu2',function(){
            $('.subMenu2').not($(this)).collapse('hide');
        });
        $(document).on('show.bs.collapse','.subMenu3',function(){
            $('.subMenu3').not($(this)).collapse('hide');
        });
        //aplica la libreria del scroll al contenedor del menu
        var ps = new PerfectScrollbar('.opcionesMenu');

    </script>

    @section('scripts')
    @show

  </body>
</html>
