@extends('layouts.dashboard')
@section('headerLogo')
<span class="etiquetaLogoInicio">@svg('home','iconoHome')</span>
@endsection
@section('contenidoVista')

@section('estilos')

<!-- Compiled and minified Bootstrap CSS -->

@endsection

@section('contenidoVista')

    <div class="row">
      <div class="col-md-7">

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="item active">
              <img class="d-block w-100" src="img/img_principal1.jpg" alt="First slide" style=" align:center; padding-right:0px; padding-left:14px; width:1200px !important; height:550px !important;" >
            </div>
            <div class="item">
              <img class="d-block w-100" src="img/img_principal2.jpg" alt="Second slide" style=" align:center; padding-right:0px; padding-left:14px; width:1200px !important; height:550px !important;">
            </div>
            <div class="item" >
              <img class="d-block w-100" src="img/img_principal3.jpg" alt="Third slide" style=" align:center; padding-right:0px; padding-left:14px; width:1200px !important; height:550px !important;" >
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-5" >
        <div class="jumbotron" style="padding-top:10px !important; padding-bottom:10px !important; text-align:center; background-color:#000000;">
          <h6 style="font-size:25px; color:#FAFAFA; font-family:Roboto-Black;">BIENVENIDO {{ Auth::user()->name }}</h6>
        </div>
        <div class="panel panel-default" style="margin-top:-20px !important">
          <div class="panel-heading">
            <h4><?php
              $week_days = array ("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");
              $months = array ("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
              $year_now = date ("Y");
              $month_now = date ("n");
              $day_now = date ("j");
              $week_day_now = date ("w");
              $date = $week_days[$week_day_now] . ", " . $day_now . " de " . $months[$month_now] . " de " . $year_now;
              echo $date;
            ?></h4>
          </div>

          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">
                <span style="margin-top:6px;"><div id="cont_1b69ec28a0054c43833bebaf9168811f"><script type="text/javascript" async src="https://www.meteored.com.ar/wid_loader/1b69ec28a0054c43833bebaf9168811f"></script></div></span>
              </div>
            </div>

          </div> <!-- panel-body -->

        </div> <!-- panel -->
        <div class="row">
          <div class="col-md-12">
            <a onclick="window.location = window.location.protocol + '//' + window.location.host + '/configCuenta'" href="#" id="btn-configuracion" style="text-decoration: none;">
              <div class="panel panel-default panelBotonNuevo">
                <center><img class="imgNuevo" src="/img/logos/procedimientos.png"><center>
                  <div class="backgroundEnlace"></div>
                  <div class="row">
                    <div class="col-xs-12">
                      <center>
                        <br>
                        <h5 class="txtLogo" style="font-size:80px !important;margin-top:60px !important"> <i class="fas fa-fw fa-user-cog"></i> </h5>
                        <br>
                        <br>
                        <h4 class="txtNuevo" style="font-family:'Arial'; font-size:18px; color:#EF5350;" >CONFIGURACIÓN DE CUENTA</h4>
                      </center>
                    </div>
                  </div>
                </div>
              </a>
            </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12" style="">
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #ccc !important">
            <h4>ÚLTIMAS SECCIONES VISITADAS</h4>
          </div>

          <div class="panel-body" style="background-color: #ccc !important">
            @foreach (Auth::user()->secciones_recientes as $seccion)

            <div class="col-xs-6 col-md-3">
              <div class="thumbnail" width="100%" style="height: 160px;">
                <a href="/{{$seccion->url_seccion}}" class="btn btn-default " role="button" style="width:100%; height: 150px;" >
                  <div class="row" style="height:100px;">
                    <img class="card-img-bottom" src="{{$seccion->url_imagen}}" data-holder-rendered="true" alt="falta la imagen" style="vertical-align:middle;">
                  </div>
                  <div class="row" style="vertical-align:bottom">
                    <div class="caption">
                      <h6 style="font-family: Roboto-Condensed;font-size: 16px; text-align:center;">{{$seccion->nombre_seccion}}</h6>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
    <!-- token -->
    <meta name="_token" content="{!! csrf_token() !!}" />
      <!--     <script src='/js/moment.min.js'></script> -->
  <!--  <script src='/js/fullcalendar.min.js'></script>-->





@endsection
