@extends('layouts.dashboard')
@section('headerLogo')
<span class="etiquetaLogoCasinos">@svg('usuario','iconoUsuarios')</span>
@endsection
@section('estilos')
<link rel="Stylesheet" type="text/css" href="css/croppie.css" />

<style media="screen">

.btn-fileInput {
  position: relative;
  overflow: hidden;
}
.btn-fileInput input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  opacity: 0;
  cursor: inherit;
  display: block;
}

</style>
@endsection

@section('contenidoVista')

    <!-- <div class="container-fluid"> -->
    <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4>EDITAR TUS DATOS</h4>
              </div>
              <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12">
                          <h5>Nombre y Apellido</h5>
                          <input id="nombre" class="form-control" type="text" value="{{$usuario->name}}" readonly="true">
                          <br>
                          <h5>Username</h5>
                          <input id="user_name" class="form-control" type="text" value="{{$usuario->user_name}}">
                          <br>
                          <h5>Email</h5>
                          <input id="email" class="form-control" type="text" value="{{$usuario->email}}">
                          <br>
                      </div>
                  </div>
                  <br>
                  <div class="row">
                      <div class="col-md-12">
                        <button id="btn-guardarDatos" class="btn btn-primary" type="button" value="{{$usuario->id}}">EDITAR DATOS</button>
                      </div>
                  </div>
              </div> <!-- /.panel-body -->
            </div> <!-- /.panel -->
          </div> <!-- /.col-lg-3 -->
    </div>
  <div class="row">

          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4>CAMBIAR CONTRASEÑA</h4>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12">
                      <h5>Contraseña actual</h5>
                      <input id="password_actual"
                      data-content='Contraseña incorrecta' data-trigger="manual" data-placement="top"
                      placeholder="" class="form-control" type="password" value="">
                      <br>
                      <h5>Nueva contraseña</h5>
                      <input id="password_nuevo" placeholder="" class="form-control" type="password" value="">
                      <br>
                      <h5>Repita nueva contraseña</h5>
                      <input id="password_nuevo_confirmation" data-content='Los datos no coinciden' data-trigger="manual" data-toggle="popover" data-placement="top"
                      placeholder="" class="form-control" type="password" value="">
                      <br>
                  </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                      <button id="btn-cambiarPass" class="btn btn-primary" type="button" value="{{$usuario->id}}">CAMBIAR CONTRASEÑA</button>
                    </div>
                </div>
              </div> <!-- /.panel-body -->
            </div> <!-- /.panel -->
          </div> <!-- /.col-lg-6 -->
  </div>
  <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4>EDITAR IMAGEN DE PERFIL</h4>
              </div>
              <div class="panel-body">
                  <div class="row">
                      <div class="col-md-12">
                          <div id="imagenPerfil"></div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                        <span class="btn btn-default btn-fileInput">
                          SUBIR IMAGEN <input type="file" name="upload" id="upload">
                        </span>
                        <button id="btn-guardarImagen" class="btn btn-primary" type="button" value="{{$usuario->id}}">GUARDAR CAMBIOS</button>
                      </div>
                  </div>
                  <br>
              </div>
            </div>
          </div> <!-- /.col-lg-6 -->

    </div> <!-- /.row -->



@endsection

@section('scripts')
<!-- JavaScript personalizado -->
<script src="js/Usuarios/seccionConfigCuenta.js" charset="utf-8"></script>

<!-- Croppie -->
<script src="js/croppie.js" charset="utf-8"></script>

<script type="text/javascript">


</script>

@endsection
