@extends('layouts.dashboard')


@section('estilos')
<link href="/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>

<link href="/themes/explorer/theme.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/css/lista-datos.css">
<link rel="stylesheet" href="/css/paginacion.css">

@endsection

@section('contenidoVista')


<div class="row">
  <div class="col-xl-12">
        <a href="" id="btn-nuevo" dusk="btn-nuevo" style="text-decoration: none;">
          <div class="panel panel-default panelBotonNuevo">
            <center><img class="imgNuevo" src="/img/logos/gestion_usuarios_white.png"><center>
            <div class="backgroundNuevo"></div>
            <div class="row">
              <div class="col-xs-12">
                <center>
                  <h5 class="txtLogo">+</h5>
                  <h4 class="txtNuevo">NUEVO USUARIO</h4>
                </center>
              </div>
            </div>
          </div>
        </a>
  </div>
</div>

  <div class="row">
    <div class="col-xl-12">
      <div id="contenedorFiltros" class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" href="#collapseFiltros" style="cursor: pointer">
          <h4>Filtros de Búsqueda  <i class="fa fa-fw fa-angle-down"></i></h4>
        </div>
        <div id="collapseFiltros" class="panel-collapse collapse">
          <div class="panel-body">
            <div class="row">
              <div class="col-xs-6">
                <h5>Usuario</h5>
                <input id="buscadorUsuario" class="form-control" placeholder="Usuario">
              </div>
              <div class="col-xs-6">
                <h5>Nombre</h5>
                <input id="buscadorNombre" class="form-control" placeholder="Nombre">
              </div>
            </div>
            <div class="row">
              <div class="col-xs-4">
                <h5>Email</h5>
                <input id="buscadorEmail" class="form-control" placeholder="Email">
              </div>
              <div class="col-xs-4">
                <h5>Casino</h5>
                <select class="form-control" name="" id="buscadorCasino" >
                  <option value="0" selected>- Todos los Casinos -</option>
                  @foreach ($casinos as $cas)
                  <option value="{{$cas->id_casino}}">{{$cas->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-xs-4">
                <button id="buscar-usuarios" class="btn btn-infoBuscar" type="button" style="margin-top:30px !important"><i class="fa fa-fw fa-search"></i> BUSCAR</button>
              </div>
            </div>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 id="tituloBusqueda">Usuarios cargados en el Sistema</h4>
        </div>
        <div class="panel-body">
            <table id="tablaUsuarios" class="table table-responsive tablesorter">
              <thead>
                <tr>
                  <th class="col-xs-3 activa" estado="asc" value="users.user_name">USUARIO <i class="fa fa-sort"></i></th>
                  <th class="col-xs-3" estado="asc" value="users.name">NOMBRE <i class="fa fa-sort"></i></th>
                  <th class="col-xs-3">EMAIL</th>
                  <th class="col-xs-3">ACCIONES</th>
                </tr>
              </thead>
              <tbody id="cuerpoTabla" >

            </tbody>
            </table>

          <div class="table-responsive" id="dd" style="display:none">
            <table  class="table table-responsive tablesorter">
              <tbody>
                <tr id="moldeFila" class="filaClone" style="display:none">
                  <td class="col-xs-3 NUsuario"></td>
                  <td class="col-xs-3 nombre"></td>
                  <td class="col-xs-3 emailUs"></td>
                  <td class="col-xs-3">
                    <button type="button" class="btn btn-info info" value="">
                            <i class="fa fa-fw fa-search-plus"></i>
                    </button>
                    <button type="button" class="btn btn-warning modificar" value="">
                            <i class="fas fa-fw fa-pencil-alt"></i>
                    </button>
                    <button type="button" class="btn btn-danger openEliminar" value="">
                            <i class="fa fa-fw fa-trash"></i>
                    </button>
                    <button type="button" class="btn btn-danger resetPassword" value="">
                            <i class="fa fa-fw fa-cog"></i>

                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <legend></legend>
          <div id="herramientasPaginacion" class="row zonaPaginacion"></div>


        </div>
      </div>
    </div>

</div>

<!-- modal preg elim -->
<div class="modal fade" id="modalAlerta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-70%">
    <div class="modal-content">
      <div class="modal-header" style="font-family: Roboto-Black; background-color:#D50000">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <button id="btn-minimizar" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsado" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
        <h3 class="modal-title">| ALERTA</h3>
      </div>
      <div id="colapsado" class="collapse in">
        <div class="modal-body">
          <h6 style="color:#000000; font-size: 18px !important">¿Esta seguro que desea resetear la contraseña de este Usuario?</h6>
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <div class="row">
                <div class="col-md-12">
                  <h6  style="color:#000000; font-size:16px !important"><i> La nueva contraseña será el DNI de este Usuario</i></h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dangerEliminar" id="btn-reset" value="">RESETEAR</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>

      </div>
    </div>
  </div>
</div>

<!-- modal CREAR -->
<div class="modal fade" id="modalCrear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="font-family: Roboto-Black; background-color: #6dc7be; color: #fff">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <button id="btn-minimizar" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsado" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
        <h3 class="modal-title">|  NUEVO USUARIO</h3>
      </div>

      <div id="colapsado" class="collapse in">
        <div class="modal-body">
            <div class="row" style="border-bottom:2px solid #ccc;">
              <div class="row">
                <div class="col-xs-6">
                  <h5 class="list-group-item"  style="font-size:15px !important; text-align:center !important;">Nombre de Usuario</h5>
                  <input type="text" class="form-control" id="usuario" name="user_name"   style="font-family:Roboto-Condensed; text-align:center !important; margin-top:0px !important; font-size:16px !important" >
                  <div class="alertaSpan" hidden role="alert" id="alertaUsuario"><span></span></div>
                  <br>
                </div>
                <div class="col-xs-6" style="border-left:2px solid #ccc; border-right:2px solid #ccc">
                  <h5 class="list-group-item"  style=" font-size:15px !important; text-align:center !important;">Nombre Completo</h5>
                  <input type="text" class="form-control" id="nombre" name="name"
                       value=""
                        style="font-family:Roboto-Condensed; margin-top:0px !important; text-align:center !important; font-size:16px !important">
                  <div class="alertaSpan" hidden role="alert" id="alertaNombre"><span></span></div>
                  <br>
                </div>
              </div>
            </div>
            <div class="row" style="border-bottom:2px solid #ccc;">
              <div class="row">
                <div class="col-xs-6">
                  <h5 class="list-group-item"  style=" font-size:15px !important; text-align:center !important;">Email</h5>
                  <input type="text" class="form-control" id="email" name="email"  value="" style="font-family:Roboto-Condensed; margin-top:0px !important; text-align:center !important; font-size:16px !important" >
                  <div class="alertaSpan" hidden role="alert" id="alertaEmail"><span></span></div>
                  <br>
                </div>
                <div class="col-xs-6">
                  <h5 class="list-group-item"  style=" font-size:15px !important; text-align:center !important;">DNI</h5>
                  <input type="text" class="form-control"  id="dni" name="dni"  value="" style="font-family:Roboto-Condensed; margin-top:0px !important; text-align:center !important; font-size:16px !important" >
                  <div class="alertaSpan" hidden role="alert" id="alertaDni"><span></span></div>
                  <br>
                </div>
              </div>
            </div>
            <div class="row" style="border-bottom:2px solid #ccc;">
              <div class="row">
                <div class="col-xs-6" id="contenedorCasino" style="border-right:2px solid #ccc;">
                  <h5 class="list-group-item"  style=" font-size:15px !important; text-align:center !important;">Casinos</h5>
                    @foreach($casinos as $casino)
                     <div class="col-lg-6">
                       <div class="input-group" style="font-size:14px !important; text-align:center !important" id="contenedorCasinoModificar">
                          <span class="input-group-addon" style="text-align:left; background-color:#eee;">
                            <input type="checkbox" id="casino{{$casino->id_casino}}" name="casino{{$casino->codigo}}" value="{{$casino->id_casino}}" aria-label="">
                            <h6 style="color:#000 !important; font-size:14px !important; display:inline;">{{$casino->nombre}}</h6>
                          </span>
                        </div>
                      </div>
                    @endforeach
                </div>
                <div class="col-xs-6" id="contenedorRoles" >
                  <h5 class="list-group-item"  style=" font-size:15px !important; text-align:center !important; " >Roles</h5>
                  @foreach($roles as $rol)
                   <div class="col-lg-6">
                     <div class="input-group" style="font-size:14px !important; text-align:center !important" >
                        <span class="input-group-addon" style="text-align:left; background-color:#eee;">
                          <input type="checkbox" id="rol{{$rol->id}}" name="rol{{$rol->name}}" value="{{$rol->id}}" aria-label="">
                          <h6 style="color:#000 !important; font-size:14px !important; display:inline;">{{$rol->name}}</h6>
                        </span>
                      </div>
                    </div>
                  @endforeach
                  <div class="list-group"  style="font-size:14px !important; text-align:center !important" id="contenedorRoles"></div>
                </div>
              </div>
            </div>
            <div class="row" style="border-bottom:2px solid #ccc;">
              <div class="row">
                <div id="collapsePermisos" class="list-group col-xs-12"  data-toggle="collapse" data-target="#permisosuser" style="cursor: pointer">
                  <h6 class="list-group-item"
                      style=" font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">
                      Permisos<i class="fa fa-fw fa-angle-down"></i>
                  </h6>
                </div>
                <div id="permisosuser" class="col-xs-12 collapse" style="border-left:2px solid #ccc;">
                  <div id="contenedorPermisos" class="list-group" style="display: inline; font-size:14px !important; text-align:center !important" >

                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-successAceptar" id="btn-guardar" value="nuevo">GUARDAR</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
              <button type="button" class="btn btn-default" data-dismiss="modal" style="display: none;">SALIR</button>
              <!-- <input type="hidden" id="id_juego" name="id_juego" value="0"> -->
            </div>
            <div id="mensajeErrorUser" hidden>
              <br>
              <span style="font-family:'Roboto-Black'; font-size:16px; color:#EF5350;">ERROR</span>
              <br>
              <span style="font-family:'Roboto-Regular'; font-size:16px; color:#555;" class="msjTextCarga">Deben completarse todos los datos solicitados.</span>
            </div> <!-- mensaje -->
        </div>
      </div>
    </div>
  </div>
</div>

<!-- modal ver detalles -->
<div class="modal fade" id="modalVer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="font-family: Roboto-Black; background-color: #0D47A1; color: #fff">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <button id="btn-minimizar" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsado" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
        <h3 class="modal-title">|  DETALLES</h3>
      </div>

      <div id="colapsado" class="collapse in">
        <div class="modal-body">
            <div class="row" style="border-bottom:2px solid #ccc;">
              <div class="row">
                <div class="col-xs-4">
                  <h6 class="list-group-item"  style="font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">Nombre de Usuario</h6>
                  <h6 class="list-group-item" style="text-align:center !important; margin-top:0px !important; font-size:14px !important" id="infoUsuario"></h6>
                  <br>
                </div>
                <div class="col-xs-4" style="border-left:2px solid #ccc; border-right:2px solid #ccc">
                  <h6 class="list-group-item"  style=" font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">Nombre Completo</h6>
                  <h6 class="list-group-item" style="margin-top:0px !important; text-align:center !important; font-size:14px !important" id="infoNombre"></h6>
                  <br>
                </div>
                <div class="col-xs-4">
                  <h6 class="list-group-item"  style=" font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">Email</h6>
                  <h6 class="list-group-item" style="margin-top:0px !important; text-align:center !important; font-size:14px !important" id="infoEmail"></h6>
                  <br>
                </div>
              </div>
            </div>
            <div class="row" style="border-bottom:2px solid #ccc;">
              <div class="row">
                <div class="col-xs-6" >
                  <h6 class="list-group-item"  style=" font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">Casinos</h6>
                  <div class="list-group" style="font-size:14px !important; text-align:center !important" id="contenedorCasinoVer"></div>
                </div>
                <div class="col-xs-6" style="border-left:2px solid #ccc;">
                  <h6 class="list-group-item"  style=" font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;" >Roles</h6>
                  <div class="list-group"  style="font-size:14px !important; text-align:center !important" id="contenedorRoles"></div>
                </div>
              </div>
            </div>
            <div class="row" style="border-bottom:2px solid #ccc;">
              <div class="row">
                <div id="collapsePermisos" class="list-group col-xs-12"  data-toggle="collapse" data-target="#permisosuserr" style="cursor: pointer">
                  <h6 class="list-group-item"
                      style=" font-size:16px !important; text-align:center !important; background-color:#aaa; color:white;">
                      Permisos<i class="fa fa-fw fa-angle-down"></i>
                  </h6>
                </div>
                <div id="permisosuserr" class="col-xs-12 collapse" style="border-left:2px solid #ccc;">
                  <div id="contenedorPermisosVer" class="list-group" style="display: inline; font-size:14px !important; text-align:center !important" >

                  </div>
                </div>
              </div>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">SALIR</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- modal ver MODIFICAR -->
<div class="modal fade" id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header"  style="font-family: Roboto-Black; background-color: #FFB74D; color: #fff">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <button id="btn-minimizar" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsado" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
        <h3 class="modal-title">|  MODIFICAR</h3>
      </div>

      <div id="colapsado" class="collapse in">
        <div class="modal-body">
            <div class="row" style="border-bottom:2px solid #ccc;">
              <div class="row">
                <div class="col-xs-4">
                  <h5 class="list-group-item"  style="font-size:15px !important; text-align:center !important;">Nombre de Usuario</h5>
                  <input type="text" class="form-control" id="modUsuario" name="user_name" readonly="true" style="font-family:Roboto-Condensed; text-align:center !important; margin-top:0px !important; font-size:16px !important" >
                  <br>
                </div>
                <div class="col-xs-4" style="border-left:2px solid #ccc; border-right:2px solid #ccc">
                  <h5 class="list-group-item"  style=" font-size:15px !important; text-align:center !important;">Nombre Completo</h5>
                  <input type="text" class="form-control" id="modNombre" name="name"
                         value=""
                        style="font-family:Roboto-Condensed; margin-top:0px !important; text-align:center !important; font-size:16px !important">
                  <br>
                </div>
                <div class="col-xs-4">
                  <h5 class="list-group-item"  style=" font-size:15px !important; text-align:center !important;">Email</h5>
                  <input type="text" class="form-control" id="modEmail" name="email"
                   value="" style="font-family:Roboto-Condensed; margin-top:0px !important; text-align:center !important; font-size:16px !important" >
                  <br>
                </div>
              </div>
            </div>
            <div class="row" style="border-bottom:2px solid #ccc;">
              <div class="row">
                <div class="col-xs-6" id="contenedorCasino">
                  <h5 class="list-group-item"  style=" font-size:15px !important; text-align:center !important;">Casinos</h5>
                    @foreach($casinos as $casino)
                     <div class="col-lg-6">
                       <div class="input-group" style="font-size:14px !important; text-align:center !important" id="contenedorCasinoModificar">
                          <span class="input-group-addon" style="text-align:left; background-color:#eee;">
                            <input type="checkbox" id="casino{{$casino->id_casino}}" name="casino{{$casino->codigo}}" value="{{$casino->id_casino}}" aria-label="">
                            <h6 style="color:#000 !important; font-size:14px !important; display:inline;">{{$casino->nombre}}</h6>
                          </span>

                        </div>
                      </div>
                    @endforeach
                </div>
                <div class="col-xs-6" id="contenedorRoles" style="border-left:2px solid #ccc;">
                  <h5 class="list-group-item"  style=" font-size:15px !important; text-align:center !important; " >Roles</h5>
                  @foreach($roles as $rol)
                   <div class="col-lg-6">
                     <div class="input-group" style="font-size:14px !important; text-align:center !important" >
                        <span class="input-group-addon" style="text-align:left; background-color:#eee;">
                          <input type="checkbox" id="rol{{$rol->id}}" name="rol{{$rol->name}}" value="{{$rol->id}}" aria-label="">
                          <h6 style="color:#000 !important; font-size:14px !important; display:inline;">{{$rol->name}}</h6>
                        </span>
                      </div>
                    </div>
                  @endforeach
                  <div class="list-group"  style="font-size:14px !important; text-align:center !important" id="contenedorRoles"></div>
                </div>
              </div>
            </div>
            <div class="row" style="border-bottom:2px solid #ccc;">
              <div class="row">
                <div id="collapsePermisos" class="list-group col-xs-12"  data-toggle="collapse" data-target="#permisosuserrrr" style="cursor: pointer">
                  <h5 class="list-group-item"
                      style=" font-size:16px !important; text-align:center !important;">
                      Permisos<i class="fa fa-fw fa-angle-down"></i>
                  </h6>
                </div>
                <div id="permisosuserrrr" class="col-xs-12 collapse" style="border-left:2px solid #ccc;">
                  <div id="contenedorPermisos" class="list-group" style="display: inline; font-size:14px !important; text-align:center !important" >

                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-warningModificar" id="btn-modificar" value="nuevo">GUARDAR</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
            </div>
            <div id="mensajeErrorUserModif" hidden>
              <br>
              <span style="font-family:'Roboto-Black'; font-size:16px; color:#EF5350;">ERROR</span>
              <br>
              <span style="font-family:'Roboto-Regular'; font-size:16px; color:#555;" class="msjText">Deben completarse todos los datos solicitados.</span>
            </div> <!-- mensaje -->
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Final Modal Ver Modificar -->

<!-- modal eliminar -->
<div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">
             <div class="modal-content">
                <div class="modal-header" style="font-family: Roboto-Black; background-color: #ef3e42">
                  <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                  <button id="btn-minimizar" type="button" class="close" data-toggle="collapse" data-minimizar="true" data-target="#colapsado" style="position:relative; right:20px; top:5px"><i class="fa fa-minus"></i></button>
                  <h3 class="modal-titleEliminar" id="myModalLabel" style="color:#fff">| ALERTA</h3>
                </div>

               <div  id="colapsado" class="collapse in">
                <div class="modal-body">
                  <form id="frmEliminar" name="frmJuego" class="form-horizontal" novalidate="">
                      <div class="form-group error ">
                          <div id="mensajeEliminar" class="col-xs-12">
                            <h6 style="color:#000000; font-size: 18px !important">¿Está seguro que desea eliminar este usuario y sus relaciones ?</h6>
                          </div>
                      </div>
                  </form>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-dangerEliminar" id="btn-eliminar" value="0">ELIMINAR</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                </div>
              </div>
            </div>
          </div>
    </div>

@endsection

<!-- Comienza modal de ayuda -->
@section('tituloDeAyuda')
<h3 class="modal-title" style="color: #fff;">| AYUDA GESTIONAR USUARIOS</h3>
@endsection
@section('contenidoAyuda')
<div class="col-md-12">
  <h5>Tarjetas de gestionar usuarios</h5>
  <p>
    Gestiona el ingreso o baja de usuarios existentes en el sistema. Permite asociarlos a diferentes casinos y roles que derivan en sus tareas asignadas.
  </p>
</div>

@endsection
<!-- Termina modal de ayuda -->

@section('scripts')

<script src="js/inputSpinner.js" type="text/javascript"></script>
<script src="/js/lista-datos.js" type="text/javascript"></script>
<script src="/js/fileinput.min.js" type="text/javascript"></script>
<script src="/js/locales/es.js" type="text/javascript"></script>
<script src="/themes/explorer/theme.js" type="text/javascript"></script>

<script src="/js/paginacion.js" charset="utf-8"></script>

<script src="js/Usuarios/usuarios.js" type="text/javascript" charset="utf-8"></script>

@endsection
