<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('listaDatos',function(){
  return view('listaDatos');
});

Route::group(['middleware' => ['role:SUPERUSUARIO']], function () {

  Route::get('logActividades','LogController@buscarTodo');
  Route::post('logActividades/buscarLogActividades','LogController@buscarLogActividades');
  Route::get('logActividades/obtenerLogActividad/{id}','LogController@obtenerLogActividad');
});

Auth::routes(); //rutas del login


Route::group(['middleware' => ['permission:Alta y Modificación de Aperturas']], function () {
  Route::post('aperturas/guardarApertura', 'Aperturas\ABMAperturaController@guardar');
  Route::post('aperturas/modificarApertura','Aperturas\ABMAperturaController@modificarApertura');


});

Route::group(['middleware' => ['permission:ABMC Relevamientos de Aperturas']], function () {
  Route::post('aperturas/generarRelevamiento', 'Aperturas\ABMCRelevamientosAperturaController@generarRelevamiento');

  Route::get('sorteo-aperturas/descargarZip/{nombre}', 'Aperturas\ABMCRelevamientosAperturaController@descargarZip');

});

Route::group(['middleware' => ['permission:Ver Sección de Cierres y Aperturas','seccion:Cierres y Aperturas']], function () {
  Route::get('/aperturas', 'Aperturas\BCAperturaController@buscarTodo');
});

Route::group(['middleware' => ['permission:Consulta de Aperturas','seccion:Cierres y Aperturas']], function () {
  Route::post('aperturas/filtrosAperturas', 'Aperturas\BCAperturaController@filtros');
  Route::get('aperturas/obtenerAperturas/{id_apertura}', 'Aperturas\BCAperturaController@getApertura');
  Route::get('aperturas/obtenerApValidar/{id_apertura}', 'Aperturas\BCAperturaController@obtenerApParaValidar');
  Route::get('aperturas/bajaApertura/{id_apertura}', 'Aperturas\BCAperturaController@eliminarApertura')->middleware('permission:Eliminar Cierres y Aperturas');
  Route::get('compararCierre/{id_apertura}/{id_cierre}/{id_moneda}','Aperturas\BCAperturaController@obtenerDetallesApCierre');

});

Route::group(['middleware' => ['permission:Validar Aperturas']], function () {
  Route::post('aperturas/validarApertura','Aperturas\VAperturaController@validarApertura');
  Route::get('aperturas/desvincularApertura/{id_apertura}', 'Cierres\ABMCCierreAperturaController@desvincularApertura');

});

//APUESTAS
Route::group(['middleware' => ['permission:ABM Relevamientos de Apuestas']], function () {
  Route::post('apuestas/cargarRelevamiento','Apuestas\ABMApuestasController@cargarRelevamiento');

});

Route::group(['middleware' => ['permission:ABMC Valor de Apuesta Mínima']], function () {
  Route::get('apuestas/obtenerRequerimientos/{id_casino}/{id_moneda}','Apuestas\ABMCApuestaMinimaController@obtenerApuestaMinima');
  Route::post( 'apuestas/modificarRequerimiento','Apuestas\ABMCApuestaMinimaController@modificar');
  Route::get('apuestas/consultarMinimo','Apuestas\ABMCApuestaMinimaController@consultarMinimo');
});

Route::group(['middleware' => ['permission:Consulta de Relevamientos de Apuestas','seccion:Apuestas Mínimas']], function () {
  //Route::get('apuestas/planilla/1','Apuestas\BCApuestasController@obtenerRelevamientoCarga');

  Route::get('/apuestas', 'Apuestas\BCApuestasController@buscarTodo');
  Route::post('/apuestas/buscarRelevamientosApuestas', 'Apuestas\BCApuestasController@filtros');
  Route::post('apuestas/generarRelevamientoApuestas', 'Apuestas\BCApuestasController@obtenerNombreZip');
  Route::get('apuestas/descargarZipApuestas/{nombre}', 'Apuestas\BCApuestasController@descargarZip');
  Route::get('apuestas/obtenerDatos/{id_relevamiento}', 'Apuestas\BCApuestasController@obtenerRelevamientoCarga');
  Route::get('apuestas/relevamientoCargado/{id_relevamiento}', 'Apuestas\BCApuestasController@obtenerRelevamientoApuesta');
  Route::post('apuestas/obtenerRelevamientoBackUp', 'Apuestas\BCApuestasController@buscarRelevamientosBackUp');
  Route::get('apuestas/imprimir/{id}','Apuestas\BCApuestasController@imprimirPlanilla');

});
Route::group(['middleware' => ['permission:Validar Relevamientos de Apuestas']], function () {
  Route::get('apuestas/baja/{id_relevamiento}', 'Apuestas\BVApuestasController@eliminar');
  Route::post('apuestas/validar', 'Apuestas\BVApuestasController@validar');

});


Route::group(['middleware' => ['permission:ABMC Imágenes Búnker','seccion:Imágenes Bunker']], function () {
  Route::post('solicitudImagenes/buscar','Bunker\ABMCImgBunkerController@filtros');
  Route::get('solicitudImagenes/obtenerMesas/{id}', 'Bunker\ABMCImgBunkerController@obtenerBunker');
  Route::get('solicitudImagenes/hayCoincidencia/{drop}/{id_detalle}', 'Bunker\ABMCImgBunkerController@consultarDiferencias');
  Route::post('solicitudImagenes/sorteoFechasMesas', 'Bunker\ABMCImgBunkerController@altaImgsBunker');
  Route::post('solicitudImagenes/guardar','Bunker\ABMCImgBunkerController@cargar');
  Route::get('/solicitudImagenes','Bunker\ABMCImgBunkerController@index');
});

Route::group(['middleware' => ['permission:AMC Canon','seccion:Canon']], function () {
  Route::get('/canon','Canon\IndexController@index');
  Route::post('canon/modificar','Canon\ABMCCanonController@modificar');
  Route::get('canon/obtenerCanon/{id_cas}','Canon\ABMCCanonController@obtenerCanon');

});

Route::group(['middleware' => ['permission:Actualizar Canon']], function () {
  Route::get('canon/generarTablaActualizacion1/{id}/{anio}','Canon\ActualizarValoresController@forzarActualizacion');
});

Route::group(['middleware' => ['permission:Alta y Modificación de Pagos']], function () {
  Route::post('canon/guardarPago','Canon\APagosController@crear');
  Route::post('canon/modificarPago','Canon\APagosController@modificar');

});

Route::group(['middleware' => ['permission:Consulta de Pagos']], function () {
  Route::post('canon/buscarPagos','Canon\BPagosController@filtros');
  Route::get('canon/obtenerPago/{id_detalle}','Canon\BPagosController@obtenerPago');
  Route::get('canon/obtenerAnios/{id_casino}','Canon\BPagosController@obtenerAnios');
  Route::post('canon/verInforme','Canon\BPagosController@verInformeFinalMesas');

});
Route::group(['middleware' => ['permission:ABMC Casinos','seccion:Casinos']], function () {
  Route::get('casinos','Casino\CasinoController@buscarTodo');
  Route::post('casinos/guardarCasino', 'Casino\CasinoController@guardarCasino');
  Route::post('casinos/modificarCasino', 'Casino\CasinoController@modificarCasino');
  Route::get('casinos/obtenerCasino/{id_casino}', 'Casino\CasinoController@obtenerCasino');
  Route::get('casinos/get', 'Casino\CasinoController@getAll');
  Route::get('casinos/getCasinos', 'Casino\CasinoController@getParaUsuario');
  Route::get('casinos/getMeses/{id_casino}', 'Casino\CasinoController@meses');
  Route::get('casinos/getFichas','Casino\CasinoController@getFichas');

});

Route::group(['middleware' => ['permission:Alta y Modificación de Cierres']], function () {
  Route::post('cierres/guardar', 'Cierres\ABMCierreController@guardar');
  Route::post('cierres/modificarCierre','Cierres\ABMCierreController@modificarCierre');

});

Route::group(['middleware' => ['permission:Consulta de Cierres','seccion:Cierres y Aperturas']], function () {
  Route::get('/cierres', 'Cierres\BCCierreController@buscarTodo');
  Route::post('cierres/filtrosCierres','Cierres\BCCierreController@filtros');
  Route::get('cierres/obtenerCierres/{id_cierre}', 'Cierres\BCCierreController@getCierre');
  Route::get('cierres/bajaCierre/{id_cierre}', 'Cierres\BCCierreController@eliminarCierre')->middleware('permission:Eliminar Cierres y Aperturas');

});
Route::group(['middleware' => ['permission:Validar Cierres']], function () {
  Route::post('cierres/validar', 'Cierres\VCierreController@validarCierre');

});

Route::group(['middleware' => ['permission:Alta y Consulta de Importaciones']], function () {
  Route::group(['middleware' => ['seccion:Importaciones Diarias']], function () {
    Route::get('/importacionDiaria','Importaciones\Mesas\ImportadorController@buscarTodo');
    Route::post('importacionDiaria/importar','Importaciones\Mesas\ImportadorController@importarDiario');
    Route::post('importacionDiaria/filtros','Importaciones\Mesas\ImportadorController@filtros');
    Route::get('importacionDiaria/verImportacion/{id_imp}/{t_mesa}','Importaciones\Mesas\ImportadorController@buscarPorTipoMesa');
    Route::post('importacionDiaria/guardar','Importaciones\Mesas\ImportadorController@guardarObservacion');
    Route::get('importacionDiaria/eliminarImportacion/{id_imp}','Importaciones\Mesas\ImportadorController@eliminar');
  });
  //Route::get('/importacionMensual','Importaciones\Mesas\MensualController@buscarTodo')->middleware([' :Importaciones Mensuales']);
  Route::group(['middleware' => ['seccion:Importaciones Mensuales']], function () {
    Route::post('importacionMensual/importar','Importaciones\Mesas\MensualController@importarMensual');
    Route::post('importacionMensual/filtros','Importaciones\Mesas\MensualController@filtros');
    Route::get('importacionMensual/verImportacion/{id_imp}','Importaciones\Mesas\MensualController@buscar');
    Route::post('importacionMensual/guardar','Importaciones\Mesas\MensualController@guardarObservacion');
    Route::get('importacionMensual/eliminarImportacion/{id_imp}','Importaciones\Mesas\MensualController@eliminar');
  });

});

Route::group(['middleware' => ['permission:Consulta de Informes a Fiscalizadores','seccion:Informes Diarios Fiscalizaciones']], function () {
  Route::get('/informeDiarioBasico','InformeFiscalizadores\BCInformesController@index');
  Route::post('informeDiarioBasico/buscar', 'InformeFiscalizadores\BCInformesController@filtros');
  Route::post('/informeDiarioBasico/buscarInformes','InformeFiscalizadores\BCInformesController@filtros');
  Route::get('informeDiarioBasico/imprimir/{id_informe_fiscalizacion}','InformeFiscalizadores\BCInformesController@imprimirPlanilla');


});

Route::group(['middleware' => ['permission:Gestión de Informes Anuales']], function () {
  Route::get('/informeAnual',function(){
    return view('Informes.seccionInformesAnuales',['casinos'=>Auth::user()->casinos]);});
  Route::post('/informeAnual/obtenerDatos','InformesMesas\BCAnualesController@buscarPorAnioCasinoMoneda');

});

Route::group(['middleware' => ['permission:Gestión de Informes Diarios y Mensuales']], function () {
  Route::get('/informeDiario','InformesMesas\IndexController@indexDiarios')->middleware(['seccion:Informes Contables']);
  Route::get('informeDiario/imprimir/{id_imp}','InformesMesas\BCInformesController@imprimirDiario');
  Route::post('informeDiario/buscar','InformesMesas\InformesController@filtrarDiarios');
  Route::get('informeDiario/getDatos/{id}','InformesMesas\ModificarInformeDiarioController@obtenerDatosAModificar');
  Route::get('informeDiario/getDatosImportacion/{id}','InformesMesas\ModificarInformeDiarioController@obtenerDatosDetalle');
  Route::post('informeDiario/almacenarDatos','InformesMesas\ModificarInformeDiarioController@almacenarDatos');

  Route::get('/informeMensual','InformesMesas\IndexController@indexMensuales')->middleware(['seccion:Informes Contables']);
  Route::post('informeMensual/buscar','InformesMesas\InformesController@filtrarMensuales');
  Route::post('informeMensual/obtenerDatos','InformesMesas\BCInformesController@obtenerDatosGraficos');
  Route::get('informeMensual/imprimir/{fecha}/{id_casino}','InformesMesas\BCInformesController@imprimirMensual');



});//incluido el informes controller
Route::group(['middleware' => ['permission:ABM Juegos']], function () {
  Route::post('juegos/nuevoJuego', 'Juegos\ABMJuegoController@guardar');
  Route::post('juegos/modificarJuego', 'Juegos\ABMJuegoController@modificarJuego');
  Route::get('juegos/obtenerJuego/{id_juego}', 'Juegos\ABMJuegoController@obtenerJuego');
  Route::get('juegos/bajaJuego/{id}', 'Juegos\ABMJuegoController@eliminarJuego');

});

Route::group(['middleware' => ['permission:Consulta Juegos','seccion:Juegos y Sectores']], function () {
  Route::get('/juegos', 'Juegos\BuscarJuegoController@buscarTodo');
  Route::post('juegos/buscarJuegos', 'Juegos\BuscarJuegoController@buscarJuegos');
  Route::get('juegos/obtenerJuegoPorCasino/{id_cas}/{nombreJuego}', 'Juegos\BuscarJuegoController@buscarJuegoPorCasinoYNombre');

});
Route::group(['middleware' => ['permission:ABM Mesas']], function () {
  Route::post('mesas/nuevaMesa/{id_casino}','Mesas\ABMMesaController@guardar');
  Route::post('mesas/modificarMesa/{id_casino}','Mesas\ABMMesaController@modificar');
  Route::get('mesas/eliminarMesa/{id_casino}/{id_mesa_de_panio}','Mesas\ABMMesaController@eliminar');

});
Route::group(['middleware' => ['permission:Consultar Mesas','seccion:Mesas']], function () {
  Route::get('/mesas-de-panio','Mesas\BuscarMesasController@getMesas');
  Route::post('mesas/buscarMesas','Mesas\BuscarMesasController@buscarMesas');
  Route::get('mesas/cargarDatos','Mesas\BuscarMesasController@getDatos');
  Route::get('mesas/detalleMesa/{id_mesa}','Mesas\BuscarMesasController@getMesa');
  Route::get('mesas/obtenerMesasApertura/{id_cas}/{nro_mesa}', 'Mesas\BuscarMesasController@buscarMesaPorNroCasino');
  Route::get('mesas/obtenerDatos/{id_cas}', 'Mesas\BuscarMesasController@datosSegunCasino');
  Route::get('mesas/obtenerMesasCierre/{id_cas}/{nro_mesa}', 'Mesas\BuscarMesasController@buscarMesaPorNroCasino');

});
Route::group(['middleware' => ['permission:ABMC Sectores','seccion:Juegos y Sectores']], function () {
  Route::post('sectores-mesas/nuevoSector','Sectores\ABMCSectoresController@guardar');
  Route::get('sectores-mesas/obtenerSector/{id_sector}','Sectores\ABMCSectoresController@obtenerSector');
  Route::post('sectores-mesas/modificarSector/{id_sector}','Sectores\ABMCSectoresController@modificarSector');
  Route::get('sectores-mesas/eliminarSector/{id_sector}','Sectores\ABMCSectoresController@eliminarSector');
  Route::post('sectores-mesas/buscarSectores','Sectores\ABMCSectoresController@filtrarSectores');
  Route::post('sectores-mesas/guardar','Sectores\ABMCSectoresController@guardar');

});
Route::group(['middleware' => ['auth']], function () {
  Route::get('turnos/buscarTurnos/{nro}','Turnos\TurnosController@buscarTurnos');
});
  Route::get('/home', 'HomeController@index')->name('home');



Route::group(['middleware' => ['permission:ABM Roles y Permisos','seccion:Roles y Permisos']], function (){

  Route::get('permisos/getSecciones','RolesPermissions\PermissionController@getSecciones');
  Route::post('roles/buscar','RolesPermissions\RoleFinderController@buscarRoles');
  Route::post('permisos/buscar','RolesPermissions\PermissionController@buscarPermisos');
  Route::get('roles','RolesPermissions\RoleController@buscarTodo');
  Route::post('permiso/guardar','RolesPermissions\PermissionController@store');
  Route::post('rol/guardar','RolesPermissions\RoleController@store');
  Route::post('rol/modificar/{id}','RolesPermissions\RoleController@update');
  Route::post('permiso/modificar/{id}','RolesPermissions\PermissionController@update');
  Route::get('permiso/getAll','RolesPermissions\PermissionController@getAll');
  Route::get('rol/getAll','RolesPermissions\RoleFinderController@getAll');

  Route::get('permiso/getAllSecciones','RolesPermissions\PermissionController@obtenerPermisosPorSeccion');
  /*
  * Borrar permiso
  */
  Route::delete('permiso/{id}','RolesPermissions\PermissionController@destroy');
  Route::delete('rol/{id}','RolesPermissions\RoleController@destroy');
  Route::get('rol/{id}','RolesPermissions\RoleFinderController@getRol');
  Route::get('permiso/{id}','RolesPermissions\PermissionController@getPermiso');

 });

 Route::group(['middleware' => ['auth','permission:ABMC Usuarios','seccion:Gestionar Usuarios']], function () {
  Route::post('usuarios/guardarUsuario','Usuarios\AMUsuariosController@guardarUsuario');
  Route::post('usuarios/modificarUsuario', 'Usuarios\AMUsuariosController@modificarUsuario');
  Route::delete('usuarios/eliminarUsuario','Usuarios\AMUsuariosController@eliminarUsuario');
  Route::post('permiso/buscarPermisosPorRoles','RolesPermissions\PermissionController@buscarPermisosPorRoles');
 });

Route::group(['middleware' => ['auth']], function () {
  Route::get('usuarios/buscarUsuariosPorNombre/{nombre}', 'Usuarios\BuscarUsuariosController@buscarUsuariosPorNombre');
  Route::post('usuarios/buscar','Usuarios\BuscarUsuariosController@buscarUsuarios');
  Route::get('usuarios','Usuarios\BuscarUsuariosController@buscarTodo')->middleware(['permission:Ver Sección Usuarios']);
  Route::get('usuarios/buscar/{id_usuario}', 'Usuarios\BuscarUsuariosController@buscarUsuario');
  Route::get('usuarios/quienSoy' ,'Usuarios\BuscarUsuariosController@quienSoy');
  Route::get('usuarios/buscarFiscalizadores/{id_cas}/{nombre}', 'Usuarios\BuscarUsuariosController@buscarFiscaNombreCasino');

});

//con solo haber iniciado sesion puede modificar algunos datos de su cuenta

Route::group(['middleware' => ['auth','seccion:Configuración de Cuenta']],function(){
  Route::post('configCuenta/modificarImagen','Usuarios\ConfiguracionCuentaController@modificarImagen');
  Route::post('configCuenta/modificarPassword','Usuarios\ConfiguracionCuentaController@modificarPassword');
  Route::get('configCuenta','Usuarios\ConfiguracionCuentaController@configUsuario');
  Route::post('configCuenta/modificarDatos','Usuarios\ConfiguracionCuentaController@modificarDatos');
  Route::get('usuarios/imagen','Usuarios\ConfiguracionCuentaController@leerImagenUsuario');
  Route::get('usuarios/resetear-ctr/{id}','Usuarios\ConfiguracionCuentaController@restablecerPassword');

  Route::get('logout', 'Auth\LoginController@logout')->name('logout');

});
Route::post('reset_password', 'Auth\ResetPasswordController@resetPassword')->name('reset_password');


/*
INSERT INTO `permissions`(`name`, `guard_name`, `seccion`) VALUES ('abm_usuarios', 'web','Gestionar Usuarios'), ('buscar_usuarios', 'web','Gestionar Usuarios')
UPDATE `secciones` SET `url_seccion` = 'importacionDiaria' WHERE `secciones`.`id_seccion` = 7;
*/
